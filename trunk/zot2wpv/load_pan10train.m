%% Load annotations.
fid=fopen('annotations.csv');
annotations=textscan(fid, '%u %u %q "%u" %q', 'delimiter', ',', 'headerlines', 1);
% annotations=[editid, annotatorid, regular, vandalism, dunno, decisiontime]
annotations=[cell2mat(annotations(:,1:2)), ...
    cell2mat(cellfun(@(c) strcmp(c, 'regular'), annotations(:,3), 'UniformOutput', false)), ...
    cell2mat(cellfun(@(c) strcmp(c, 'vandalism'), annotations(:,3), 'UniformOutput', false)), ...
    cell2mat(cellfun(@(c) strcmp(c, 'dunno'), annotations(:,3), 'UniformOutput', false)), ...
    cell2mat(annotations(:,4))];
fclose(fid);
%%
annotators=dataset('File', 'annotators.csv', 'Delimiter', ',', 'VarNames', {'annotatorid','age','sex','reading','editing','vandalizing','noticing'}); 
summary(annotators)
%%
fid=fopen('gold-annotations.csv');
gold=textscan(fid, '%u %q %u %u', 'delimiter', ',', 'headerlines', 1);
% gold=[editid,regular,vandalism,yesvotes,totalvotes]
gold=[cell2mat(gold(:,1)), ...
    cell2mat(cellfun(@(c) strcmp(c, 'regular'), gold(:,2), 'UniformOutput', false)), ...
    cell2mat(cellfun(@(c) strcmp(c, 'vandalism'), gold(:,2), 'UniformOutput', false)), ...
    cell2mat(gold(:,3:4))];
fclose(fid);
%%
N=size(annotations,1);

annotator_max=max(annotations(:,2));
decider_time=zeros(annotator_max,2);

edit_max=max(annotations(:,1));
edit_time=zeros(edit_max,2);

for i=1:N
    decider_time(annotations(i,2),1) = decider_time(annotations(i,2),1) + annotations(i,6);
    decider_time(annotations(i,2),2) = decider_time(annotations(i,2),2) + 1;

    edit_time(annotations(i,1),1) = edit_time(annotations(i,1),1) + annotations(i,6);
    edit_time(annotations(i,1),2) = edit_time(annotations(i,1),2) + 1;
end

% Average time to make a decision for each annotator.
decider_meantime = decider_time(:,1) ./ decider_time(:,2);
edit_meantime =  edit_time(:,1) ./ edit_time(:,2);
%%
fid=fopen('edits.csv');
edits=textscan(fid, '%u %q %u %u %q %q %q %q', 'delimiter', ',', 'headerlines', 1);

editors=sort(unique(edits{:,2}));
editor_anon=cellfun(@(s) ~isempty(regexp(s, '^\d+\.\d+\.\d+\.\d+$', 'once')), editors);
editor_bot=cellfun(@(s) ~isempty(regexpi(s, 'bot$', 'once')), editors);

comment_length=cellfun(@(s) length(char(s)), edits{:,7});
comment_length(strmatch('null', edits{:,7}, 'exact'))=0;
comment_revert=cellfun(@(s) ~isempty(regexpi(s, 'revert', 'once')), edits{:,7});

editsx=zeros(edit_max,8);
editsx(cell2mat(edits(:,1)),1:4) = [
    editor_anon(cellfun(@(e) strmatch(e, editors, 'exact'), edits{:,2})), ...
    editor_bot(cellfun(@(e) strmatch(e, editors, 'exact'), edits{:,2})), ...
    comment_length, ...
    comment_revert];

editsx(gold(:,1),5:8)=gold(:,2:5);

fclose(fid);
%%

% annotationsx=[e_id, a_id, a_regular,a_vandalism,a_dunno,a_decisiontime,a_fastfactor,e_anon,e_bot,c_length,c_revert,g_regular,g_vandalism,g_yesvotes,g_totalvotes]
%  1 : edit_id
%  2 : annotator_id
%  3 : annotator_regular
%  4 : annotator_vandalism
%  5 : annotator_dunno
%  6 : annotator_decisiontime
%  7 : annotator_fastfactor
%  8 : editorid_anon
%  9 : editorid_bot
% 10 : comment_length
% 11 : comment_revert
% 12 : gold_regular
% 13 : gold_vandalism
% 14 : gold_yesvotes
% 15 : gold_totalvotes

annotationsx=[double(annotations(:,:)), ...
    arrayfun(@(i) exp(1-(double(annotations(i,6))/decider_meantime(annotations(i,2)))), 1:N)', ...
    editsx(annotations(:,1),:)];
%%
t = edit_meantime;
t(t > 100000)=100000;
hist(t,100);
%%
clear gold annotations annotators edits editsx;
clear decider_time edit_time N editors annotator_max  comment_length comment_revert edit_max editor_anon editor_bot fid i t;
%%
corrcoef(annotationsx(:,3), annotationsx(:,7))
std(annotationsx(:,7))
mean(annotationsx(annotationsx(:,3)==1,7))
mean(annotationsx(annotationsx(:,3)==0,7))
mean(annotationsx(:,7))
mean(annotationsx(annotationsx(:,3)==1,6))
mean(annotationsx(annotationsx(:,3)==0,6))
mean(annotationsx(:,6))
std(annotationsx(:,6))
