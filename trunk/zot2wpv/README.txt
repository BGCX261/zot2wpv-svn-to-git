Processing Steps

Currently the paths for most files/directories are hardcoded in the various scripts, so this note will refer to them that way here.  Naturally a good revision to the code would be to parameterize the paths.

zot2wpv/
    cache/
    code/
    data/
    html/
    west_cache/
    workspace/

/files/
    pan10-vandalism-test-collection-2010-05-16/
    article-revisions/
 
1) Ant script zot2wpv/code/build.xml
   In the code subdirectory run ExtendEdits.groovy with 'ant genxedits' at command line.
   That will delete the data/corpus and data/visible directories and take some hours to run.
   Output:
   	zot2wpv/data/corpus/regular
   	zot2wpv/data/corpus/vandalism
   	zot2wpv/data/visible/regular
   	zot2wpv/data/visible/vandalism
        data/xedits.csv
        
2) RapidMiner script make_wordfreq_dataset.xml
   TextInput (TFIDF, bigrams, Snowball stemmer) from:
   	zot2wpv/data/corpus/regular
   	zot2wpv/data/corpus/vandalism
   Output:
      wordlist.txt		- word list
      tfidf_corpus.dat          - word vector features (about 21K of them).   
      tfidf_vandalism_weights.wgt	- CorpusBasedWeighting for class vandalism
      tfidf_regular_weights.wgt  	- CorpusBasedWeighting for class regular

3) RapidMiner strip_low_weights_and_merge
   Take tfidf_corpus.aml and retain top 300 features tfidf_vandalism_weights.wgt
   Take tfidf_corpus.aml and retain top 300 features tfidf_regular_weights.wgt
   Join those two sets and write it to stripped_tfidf.aml
   Load the extended edit features (../data/xedits.csv)
   Strip text features (_editcomment, _editsection, _editor, _edittime, _articletitle)
   Join with the tfidf features and write to stripped_edits_tfidf.aml


Preparing the Test Set

   After the test set RAR files are unpacked, generate the revision-index.txt file.
   In the directory /files/pan10-vandalism-test-collection-2010-05-16/article-revisions,
   at the command line:
       find * -type f >../revision-index.txt
   
Apply Model To Test Set

1) Ant script zot2wpv/code/build.xml
   In the code subdirectory need to run ProcessTestEdits.groovy.
   First remove any old version of the processed text diffs:
        ant cleantest
   That will delete:
   	/files/pan10-vandalism-test-collection-2010-05-16/corpus
   	/files/pan10-vandalism-test-collection-2010-05-16/visible
   Then:
        ant processtest0
        ant processtest1
   Those should be run in parallel which can most easily be done in separate shells.
   That will read from:
   	/files/pan10-vandalism-test-collection-2010-05-16/article-revisions
   	/files/pan10-vandalism-test-collection-2010-05-16/edits.csv
   
   Output:
   	/files/pan10-vandalism-test-collection-2010-05-16/corpus/regular
   	/files/pan10-vandalism-test-collection-2010-05-16/corpus/vandalism
   	/files/pan10-vandalism-test-collection-2010-05-16/visible/regular
   	/files/pan10-vandalism-test-collection-2010-05-16/visible/vandalism
        /files/pan10-vandalism-test-collection-2010-05-16/xedits_0.csv
        /files/pan10-vandalism-test-collection-2010-05-16/xedits_1.csv

   Combine two parts of xedits, be careful of the header row.
   In the directory /files/pan10-vandalism-test-collection-2010-05-16, 
   at the command line:
        tail -n +2 xedits_1.csv | cat xedits_0.csv - >xedits.csv
        cp xedits.csv ~/Projects/zot2wpv/data/test-xedits.csv 
        
2) Extract word vectors using wordlist from training.
   RapidMiner make_test_tfidf.xml
   Input    /files/pan10-vandalism-test-collection-2010-05-16/corpus
            wordlist.txt
   Output   tfidf_test_raw.aml

3) Split test set into 10 parts
   RapidMiner test_split_raw_data.xml
   Input	tfidf_test_raw.aml
   Output	subsets/raw/tfidf_test_%{a}.aml
   
4) Make feature stripped and xedits merged subsets.
   RapidMiner  test_strip_and_merge.xml
   Input	subsets/raw/tfidf_test_%{a}.aml
                data/test-xedits.csv
   Output	subsets/stripped/tfidf_test_%{a}.aml
   		subsets/merged/tfidf_test_%{a}.aml

5) Apply trained model to subsets.   
   RapidMiner test_apply_model_to_subsets.xml
   Input	train_bagged_j48graft_model_2010_06_22-AM_01_49_27.mod
   		subsets/merged/tfidf_test_%{a}.aml
   Output	subsets/results/tfidf_test_%{a}.aml
   		subsets/results/tfidf_test_%{a}.txt
   The .txt files have the results in PAN CLEF format.
   Concatenate them for the final results file:
   
   cat subsets/results/*.txt >final-results.txt
   
  
  