package sunx.text;//package wikiparse;

import java.util.Map;
import java.util.HashMap;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class EntityConverter {

    static Map<String, String> entities = new HashMap<String, String>();

    static Pattern entityPattern = Pattern.compile("&([a-zA-Z0-9]{1,10});");

    static public String replaceEntities(String s) {
        Matcher m = entityPattern.matcher(s);

        StringBuffer sb = new StringBuffer(s.length());

        while (m.find()) {
            String r = entities.get(m.group(1));

            m.appendReplacement(sb, r == null ? "" : r);
        }
        m.appendTail(sb);

        return sb.toString();
    }

    static {
// <!-- Portions (C) International Organization for Standardization 1986
//      Permission to copy in any form is granted for use with
//      conforming SGML systems and applications as defined in
//      ISO 8879, provided this notice is included in all copies.
// -->
// <!-- Character entity set. Typical invocation:
//     <!ENTITY % HTMLlat1 PUBLIC
//        "-//W3C//ENTITIES Latin 1 for XHTML//EN"
//        "http://www.w3.org/TR/xhtml1/DTD/xhtml-lat1.ent">
//     %HTMLlat1;
// -->
// 
        entities.put("nbsp", "\u00A0");    // <!ENTITY nbsp   "&#160;"> <!-- no-break space = non-breaking space, U+00A0 ISOnum -->
        entities.put("iexcl", "\u00A1");    // <!ENTITY iexcl  "&#161;"> <!-- inverted exclamation mark, U+00A1 ISOnum -->
        entities.put("cent", "\u00A2");    // <!ENTITY cent   "&#162;"> <!-- cent sign, U+00A2 ISOnum -->
        entities.put("pound", "\u00A3");    // <!ENTITY pound  "&#163;"> <!-- pound sign, U+00A3 ISOnum -->
        entities.put("curren", "\u00A4");    // <!ENTITY curren "&#164;"> <!-- currency sign, U+00A4 ISOnum -->
        entities.put("yen", "\u00A5");    // <!ENTITY yen    "&#165;"> <!-- yen sign = yuan sign, U+00A5 ISOnum -->
        entities.put("brvbar", "\u00A6");    // <!ENTITY brvbar "&#166;"> <!-- broken bar = broken vertical bar, U+00A6 ISOnum -->
        entities.put("sect", "\u00A7");    // <!ENTITY sect   "&#167;"> <!-- section sign, U+00A7 ISOnum -->
        entities.put("uml", "\u00A8");    // <!ENTITY uml    "&#168;"> <!-- diaeresis = spacing diaeresis, U+00A8 ISOdia -->
        entities.put("copy", "\u00A9");    // <!ENTITY copy   "&#169;"> <!-- copyright sign, U+00A9 ISOnum -->
        entities.put("ordf", "\u00AA");    // <!ENTITY ordf   "&#170;"> <!-- feminine ordinal indicator, U+00AA ISOnum -->
        entities.put("laquo", "\u00AB");    // <!ENTITY laquo  "&#171;"> <!-- left-pointing double angle quotation mark = left pointing guillemet, U+00AB ISOnum -->
        entities.put("not", "\u00AC");    // <!ENTITY not    "&#172;"> <!-- not sign = angled dash, U+00AC ISOnum -->
        entities.put("shy", "\u00AD");    // <!ENTITY shy    "&#173;"> <!-- soft hyphen = discretionary hyphen, U+00AD ISOnum -->
        entities.put("reg", "\u00AE");    // <!ENTITY reg    "&#174;"> <!-- registered sign = registered trade mark sign, U+00AE ISOnum -->
        entities.put("macr", "\u00AF");    // <!ENTITY macr   "&#175;"> <!-- macron = spacing macron = overline = APL overbar, U+00AF ISOdia -->
        entities.put("deg", "\u00B0");    // <!ENTITY deg    "&#176;"> <!-- degree sign, U+00B0 ISOnum -->
        entities.put("plusmn", "\u00B1");    // <!ENTITY plusmn "&#177;"> <!-- plus-minus sign = plus-or-minus sign, U+00B1 ISOnum -->
        entities.put("sup2", "\u00B2");    // <!ENTITY sup2   "&#178;"> <!-- superscript two = superscript digit two = squared, U+00B2 ISOnum -->
        entities.put("sup3", "\u00B3");    // <!ENTITY sup3   "&#179;"> <!-- superscript three = superscript digit three = cubed, U+00B3 ISOnum -->
        entities.put("acute", "\u00B4");    // <!ENTITY acute  "&#180;"> <!-- acute accent = spacing acute, U+00B4 ISOdia -->
        entities.put("micro", "\u00B5");    // <!ENTITY micro  "&#181;"> <!-- micro sign, U+00B5 ISOnum -->
        entities.put("para", "\u00B6");    // <!ENTITY para   "&#182;"> <!-- pilcrow sign = paragraph sign, U+00B6 ISOnum -->
        entities.put("middot", "\u00B7");    // <!ENTITY middot "&#183;"> <!-- middle dot = Georgian comma = Greek middle dot, U+00B7 ISOnum -->
        entities.put("cedil", "\u00B8");    // <!ENTITY cedil  "&#184;"> <!-- cedilla = spacing cedilla, U+00B8 ISOdia -->
        entities.put("sup1", "\u00B9");    // <!ENTITY sup1   "&#185;"> <!-- superscript one = superscript digit one, U+00B9 ISOnum -->
        entities.put("ordm", "\u00BA");    // <!ENTITY ordm   "&#186;"> <!-- masculine ordinal indicator, U+00BA ISOnum -->
        entities.put("raquo", "\u00BB");    // <!ENTITY raquo  "&#187;"> <!-- right-pointing double angle quotation mark = right pointing guillemet, U+00BB ISOnum -->
        entities.put("frac14", "\u00BC");    // <!ENTITY frac14 "&#188;"> <!-- vulgar fraction one quarter = fraction one quarter, U+00BC ISOnum -->
        entities.put("frac12", "\u00BD");    // <!ENTITY frac12 "&#189;"> <!-- vulgar fraction one half = fraction one half, U+00BD ISOnum -->
        entities.put("frac34", "\u00BE");    // <!ENTITY frac34 "&#190;"> <!-- vulgar fraction three quarters = fraction three quarters, U+00BE ISOnum -->
        entities.put("iquest", "\u00BF");    // <!ENTITY iquest "&#191;"> <!-- inverted question mark = turned question mark, U+00BF ISOnum -->
        entities.put("Agrave", "\u00C0");    // <!ENTITY Agrave "&#192;"> <!-- latin capital letter A with grave = latin capital letter A grave, U+00C0 ISOlat1 -->
        entities.put("Aacute", "\u00C1");    // <!ENTITY Aacute "&#193;"> <!-- latin capital letter A with acute, U+00C1 ISOlat1 -->
        entities.put("Acirc", "\u00C2");    // <!ENTITY Acirc  "&#194;"> <!-- latin capital letter A with circumflex, U+00C2 ISOlat1 -->
        entities.put("Atilde", "\u00C3");    // <!ENTITY Atilde "&#195;"> <!-- latin capital letter A with tilde, U+00C3 ISOlat1 -->
        entities.put("Auml", "\u00C4");    // <!ENTITY Auml   "&#196;"> <!-- latin capital letter A with diaeresis, U+00C4 ISOlat1 -->
        entities.put("Aring", "\u00C5");    // <!ENTITY Aring  "&#197;"> <!-- latin capital letter A with ring above = latin capital letter A ring, U+00C5 ISOlat1 -->
        entities.put("AElig", "\u00C6");    // <!ENTITY AElig  "&#198;"> <!-- latin capital letter AE = latin capital ligature AE, U+00C6 ISOlat1 -->
        entities.put("Ccedil", "\u00C7");    // <!ENTITY Ccedil "&#199;"> <!-- latin capital letter C with cedilla, U+00C7 ISOlat1 -->
        entities.put("Egrave", "\u00C8");    // <!ENTITY Egrave "&#200;"> <!-- latin capital letter E with grave, U+00C8 ISOlat1 -->
        entities.put("Eacute", "\u00C9");    // <!ENTITY Eacute "&#201;"> <!-- latin capital letter E with acute, U+00C9 ISOlat1 -->
        entities.put("Ecirc", "\u00CA");    // <!ENTITY Ecirc  "&#202;"> <!-- latin capital letter E with circumflex, U+00CA ISOlat1 -->
        entities.put("Euml", "\u00CB");    // <!ENTITY Euml   "&#203;"> <!-- latin capital letter E with diaeresis, U+00CB ISOlat1 -->
        entities.put("Igrave", "\u00CC");    // <!ENTITY Igrave "&#204;"> <!-- latin capital letter I with grave, U+00CC ISOlat1 -->
        entities.put("Iacute", "\u00CD");    // <!ENTITY Iacute "&#205;"> <!-- latin capital letter I with acute, U+00CD ISOlat1 -->
        entities.put("Icirc", "\u00CE");    // <!ENTITY Icirc  "&#206;"> <!-- latin capital letter I with circumflex, U+00CE ISOlat1 -->
        entities.put("Iuml", "\u00CF");    // <!ENTITY Iuml   "&#207;"> <!-- latin capital letter I with diaeresis, U+00CF ISOlat1 -->
        entities.put("ETH", "\u00D0");    // <!ENTITY ETH    "&#208;"> <!-- latin capital letter ETH, U+00D0 ISOlat1 -->
        entities.put("Ntilde", "\u00D1");    // <!ENTITY Ntilde "&#209;"> <!-- latin capital letter N with tilde, U+00D1 ISOlat1 -->
        entities.put("Ograve", "\u00D2");    // <!ENTITY Ograve "&#210;"> <!-- latin capital letter O with grave, U+00D2 ISOlat1 -->
        entities.put("Oacute", "\u00D3");    // <!ENTITY Oacute "&#211;"> <!-- latin capital letter O with acute, U+00D3 ISOlat1 -->
        entities.put("Ocirc", "\u00D4");    // <!ENTITY Ocirc  "&#212;"> <!-- latin capital letter O with circumflex, U+00D4 ISOlat1 -->
        entities.put("Otilde", "\u00D5");    // <!ENTITY Otilde "&#213;"> <!-- latin capital letter O with tilde, U+00D5 ISOlat1 -->
        entities.put("Ouml", "\u00D6");    // <!ENTITY Ouml   "&#214;"> <!-- latin capital letter O with diaeresis, U+00D6 ISOlat1 -->
        entities.put("times", "\u00D7");    // <!ENTITY times  "&#215;"> <!-- multiplication sign, U+00D7 ISOnum -->
        entities.put("Oslash", "\u00D8");    // <!ENTITY Oslash "&#216;"> <!-- latin capital letter O with stroke = latin capital letter O slash, U+00D8 ISOlat1 -->
        entities.put("Ugrave", "\u00D9");    // <!ENTITY Ugrave "&#217;"> <!-- latin capital letter U with grave, U+00D9 ISOlat1 -->
        entities.put("Uacute", "\u00DA");    // <!ENTITY Uacute "&#218;"> <!-- latin capital letter U with acute, U+00DA ISOlat1 -->
        entities.put("Ucirc", "\u00DB");    // <!ENTITY Ucirc  "&#219;"> <!-- latin capital letter U with circumflex, U+00DB ISOlat1 -->
        entities.put("Uuml", "\u00DC");    // <!ENTITY Uuml   "&#220;"> <!-- latin capital letter U with diaeresis, U+00DC ISOlat1 -->
        entities.put("Yacute", "\u00DD");    // <!ENTITY Yacute "&#221;"> <!-- latin capital letter Y with acute, U+00DD ISOlat1 -->
        entities.put("THORN", "\u00DE");    // <!ENTITY THORN  "&#222;"> <!-- latin capital letter THORN, U+00DE ISOlat1 -->
        entities.put("szlig", "\u00DF");    // <!ENTITY szlig  "&#223;"> <!-- latin small letter sharp s = ess-zed, U+00DF ISOlat1 -->
        entities.put("agrave", "\u00E0");    // <!ENTITY agrave "&#224;"> <!-- latin small letter a with grave = latin small letter a grave, U+00E0 ISOlat1 -->
        entities.put("aacute", "\u00E1");    // <!ENTITY aacute "&#225;"> <!-- latin small letter a with acute, U+00E1 ISOlat1 -->
        entities.put("acirc", "\u00E2");    // <!ENTITY acirc  "&#226;"> <!-- latin small letter a with circumflex, U+00E2 ISOlat1 -->
        entities.put("atilde", "\u00E3");    // <!ENTITY atilde "&#227;"> <!-- latin small letter a with tilde, U+00E3 ISOlat1 -->
        entities.put("auml", "\u00E4");    // <!ENTITY auml   "&#228;"> <!-- latin small letter a with diaeresis, U+00E4 ISOlat1 -->
        entities.put("aring", "\u00E5");    // <!ENTITY aring  "&#229;"> <!-- latin small letter a with ring above = latin small letter a ring, U+00E5 ISOlat1 -->
        entities.put("aelig", "\u00E6");    // <!ENTITY aelig  "&#230;"> <!-- latin small letter ae = latin small ligature ae, U+00E6 ISOlat1 -->
        entities.put("ccedil", "\u00E7");    // <!ENTITY ccedil "&#231;"> <!-- latin small letter c with cedilla, U+00E7 ISOlat1 -->
        entities.put("egrave", "\u00E8");    // <!ENTITY egrave "&#232;"> <!-- latin small letter e with grave, U+00E8 ISOlat1 -->
        entities.put("eacute", "\u00E9");    // <!ENTITY eacute "&#233;"> <!-- latin small letter e with acute, U+00E9 ISOlat1 -->
        entities.put("ecirc", "\u00EA");    // <!ENTITY ecirc  "&#234;"> <!-- latin small letter e with circumflex, U+00EA ISOlat1 -->
        entities.put("euml", "\u00EB");    // <!ENTITY euml   "&#235;"> <!-- latin small letter e with diaeresis, U+00EB ISOlat1 -->
        entities.put("igrave", "\u00EC");    // <!ENTITY igrave "&#236;"> <!-- latin small letter i with grave, U+00EC ISOlat1 -->
        entities.put("iacute", "\u00ED");    // <!ENTITY iacute "&#237;"> <!-- latin small letter i with acute, U+00ED ISOlat1 -->
        entities.put("icirc", "\u00EE");    // <!ENTITY icirc  "&#238;"> <!-- latin small letter i with circumflex, U+00EE ISOlat1 -->
        entities.put("iuml", "\u00EF");    // <!ENTITY iuml   "&#239;"> <!-- latin small letter i with diaeresis, U+00EF ISOlat1 -->
        entities.put("eth", "\u00F0");    // <!ENTITY eth    "&#240;"> <!-- latin small letter eth, U+00F0 ISOlat1 -->
        entities.put("ntilde", "\u00F1");    // <!ENTITY ntilde "&#241;"> <!-- latin small letter n with tilde, U+00F1 ISOlat1 -->
        entities.put("ograve", "\u00F2");    // <!ENTITY ograve "&#242;"> <!-- latin small letter o with grave, U+00F2 ISOlat1 -->
        entities.put("oacute", "\u00F3");    // <!ENTITY oacute "&#243;"> <!-- latin small letter o with acute, U+00F3 ISOlat1 -->
        entities.put("ocirc", "\u00F4");    // <!ENTITY ocirc  "&#244;"> <!-- latin small letter o with circumflex, U+00F4 ISOlat1 -->
        entities.put("otilde", "\u00F5");    // <!ENTITY otilde "&#245;"> <!-- latin small letter o with tilde, U+00F5 ISOlat1 -->
        entities.put("ouml", "\u00F6");    // <!ENTITY ouml   "&#246;"> <!-- latin small letter o with diaeresis, U+00F6 ISOlat1 -->
        entities.put("divide", "\u00F7");    // <!ENTITY divide "&#247;"> <!-- division sign, U+00F7 ISOnum -->
        entities.put("oslash", "\u00F8");    // <!ENTITY oslash "&#248;"> <!-- latin small letter o with stroke, = latin small letter o slash, U+00F8 ISOlat1 -->
        entities.put("ugrave", "\u00F9");    // <!ENTITY ugrave "&#249;"> <!-- latin small letter u with grave, U+00F9 ISOlat1 -->
        entities.put("uacute", "\u00FA");    // <!ENTITY uacute "&#250;"> <!-- latin small letter u with acute, U+00FA ISOlat1 -->
        entities.put("ucirc", "\u00FB");    // <!ENTITY ucirc  "&#251;"> <!-- latin small letter u with circumflex, U+00FB ISOlat1 -->
        entities.put("uuml", "\u00FC");    // <!ENTITY uuml   "&#252;"> <!-- latin small letter u with diaeresis, U+00FC ISOlat1 -->
        entities.put("yacute", "\u00FD");    // <!ENTITY yacute "&#253;"> <!-- latin small letter y with acute, U+00FD ISOlat1 -->
        entities.put("thorn", "\u00FE");    // <!ENTITY thorn  "&#254;"> <!-- latin small letter thorn, U+00FE ISOlat1 -->
        entities.put("yuml", "\u00FF");    // <!ENTITY yuml   "&#255;"> <!-- latin small letter y with diaeresis, U+00FF ISOlat1 -->
// 
// <!-- Special characters for XHTML -->
// 
// <!-- Character entity set. Typical invocation:
//      <!ENTITY % HTMLspecial PUBLIC
//         "-//W3C//ENTITIES Special for XHTML//EN"
//         "http://www.w3.org/TR/xhtml1/DTD/xhtml-special.ent">
//      %HTMLspecial;
// -->
// 
// <!-- Portions (C) International Organization for Standardization 1986:
//      Permission to copy in any form is granted for use with
//      conforming SGML systems and applications as defined in
//      ISO 8879, provided this notice is included in all copies.
// -->
// 
// <!-- Relevant ISO entity set is given unless names are newly introduced.
//      New names (i.e., not in ISO 8879 list) do not clash with any
//      existing ISO 8879 entity names. ISO 10646 character numbers
//      are given for each character, in hex. values are decimal
//      conversions of the ISO 10646 values and refer to the document
//      character set. Names are Unicode names. 
// -->
// 
//  <!-- C0 Controls and Basic Latin -->
        entities.put("quot", "\"");    // <!ENTITY quot    "&#34;"> <!--  quotation mark, U+0022 ISOnum -->
        entities.put("amp", "\u0026");    // <!ENTITY amp     "&#38;#38;"> <!--  ampersand, U+0026 ISOnum -->
        entities.put("lt", "\u003C");    // <!ENTITY lt      "&#38;#60;"> <!--  less-than sign, U+003C ISOnum -->
        entities.put("gt", "\u003E");    // <!ENTITY gt      "&#62;"> <!--  greater-than sign, U+003E ISOnum -->
        entities.put("apos", "\u0027");    // <!ENTITY apos	 "&#39;"> <!--  apostrophe = APL quote, U+0027 ISOnum -->

// <!-- Latin Extended-A -->
        entities.put("OElig", "\u0152");    // <!ENTITY OElig   "&#338;"> <!--  latin capital ligature OE, U+0152 ISOlat2 -->
        entities.put("oelig", "\u0153");    // <!ENTITY oelig   "&#339;"> <!--  latin small ligature oe, U+0153 ISOlat2 -->
// <!-- ligature is a misnomer, this is a separate character in some languages -->
        entities.put("Scaron", "\u0160");    // <!ENTITY Scaron  "&#352;"> <!--  latin capital letter S with caron, U+0160 ISOlat2 -->
        entities.put("scaron", "\u0161");    // <!ENTITY scaron  "&#353;"> <!--  latin small letter s with caron, U+0161 ISOlat2 -->
        entities.put("Yuml", "\u0178");    // <!ENTITY Yuml    "&#376;"> <!--  latin capital letter Y with diaeresis, U+0178 ISOlat2 -->

// <!-- Spacing Modifier Letters -->
        entities.put("circ", "\u02C6");    // <!ENTITY circ    "&#710;"> <!--  modifier letter circumflex accent, U+02C6 ISOpub -->
        entities.put("tilde", "\u02DC");    // <!ENTITY tilde   "&#732;"> <!--  small tilde, U+02DC ISOdia -->

// <!-- General Punctuation -->
        entities.put("ensp", "\u2002");    // <!ENTITY ensp    "&#8194;"> <!-- en space, U+2002 ISOpub -->
        entities.put("emsp", "\u2003");    // <!ENTITY emsp    "&#8195;"> <!-- em space, U+2003 ISOpub -->
        entities.put("thinsp", "\u2009");    // <!ENTITY thinsp  "&#8201;"> <!-- thin space, U+2009 ISOpub -->
        entities.put("zwnj", "\u200C");    // <!ENTITY zwnj    "&#8204;"> <!-- zero width non-joiner, U+200C NEW RFC 2070 -->
        entities.put("zwj", "\u200D");    // <!ENTITY zwj     "&#8205;"> <!-- zero width joiner, U+200D NEW RFC 2070 -->
        entities.put("lrm", "\u200E");    // <!ENTITY lrm     "&#8206;"> <!-- left-to-right mark, U+200E NEW RFC 2070 -->
        entities.put("rlm", "\u200F");    // <!ENTITY rlm     "&#8207;"> <!-- right-to-left mark, U+200F NEW RFC 2070 -->
        entities.put("ndash", "\u2013");    // <!ENTITY ndash   "&#8211;"> <!-- en dash, U+2013 ISOpub -->
        entities.put("mdash", "\u2014");    // <!ENTITY mdash   "&#8212;"> <!-- em dash, U+2014 ISOpub -->
        entities.put("lsquo", "\u2018");    // <!ENTITY lsquo   "&#8216;"> <!-- left single quotation mark, U+2018 ISOnum -->
        entities.put("rsquo", "\u2019");    // <!ENTITY rsquo   "&#8217;"> <!-- right single quotation mark, U+2019 ISOnum -->
        entities.put("sbquo", "\u201A");    // <!ENTITY sbquo   "&#8218;"> <!-- single low-9 quotation mark, U+201A NEW -->
        entities.put("ldquo", "\u201C");    // <!ENTITY ldquo   "&#8220;"> <!-- left double quotation mark, U+201C ISOnum -->
        entities.put("rdquo", "\u201D");    // <!ENTITY rdquo   "&#8221;"> <!-- right double quotation mark, U+201D ISOnum -->
        entities.put("bdquo", "\u201E");    // <!ENTITY bdquo   "&#8222;"> <!-- double low-9 quotation mark, U+201E NEW -->
        entities.put("dagger", "\u2020");    // <!ENTITY dagger  "&#8224;"> <!-- dagger, U+2020 ISOpub -->
        entities.put("Dagger", "\u2021");    // <!ENTITY Dagger  "&#8225;"> <!-- double dagger, U+2021 ISOpub -->
        entities.put("permil", "\u2030");    // <!ENTITY permil  "&#8240;"> <!-- per mille sign, U+2030 ISOtech -->
        entities.put("lsaquo", "\u2039");    // <!ENTITY lsaquo  "&#8249;"> <!-- single left-pointing angle quotation mark, U+2039 ISO proposed -->
// <!-- lsaquo is proposed but not yet ISO standardized -->
        entities.put("rsaquo", "\u203A");    // <!ENTITY rsaquo  "&#8250;"> <!-- single right-pointing angle quotation mark, U+203A ISO proposed -->
// <!-- rsaquo is proposed but not yet ISO standardized -->

// <!-- Currency Symbols -->
        entities.put("euro", "\u20AC");    // <!ENTITY euro   "&#8364;"> <!--  euro sign, U+20AC NEW -->
// 
// <!-- Mathematical, Greek and Symbolic characters for XHTML -->
// 
// <!-- Character entity set. Typical invocation:
//      <!ENTITY % HTMLsymbol PUBLIC
//         "-//W3C//ENTITIES Symbols for XHTML//EN"
//         "http://www.w3.org/TR/xhtml1/DTD/xhtml-symbol.ent">
//      %HTMLsymbol;
// -->
// 
// <!-- Portions (C) International Organization for Standardization 1986:
//      Permission to copy in any form is granted for use with
//      conforming SGML systems and applications as defined in
//      ISO 8879, provided this notice is included in all copies.
// -->
// 
// <!-- Relevant ISO entity set is given unless names are newly introduced.
//      New names (i.e., not in ISO 8879 list) do not clash with any
//      existing ISO 8879 entity names. ISO 10646 character numbers
//      are given for each character, in hex. values are decimal
//      conversions of the ISO 10646 values and refer to the document
//      character set. Names are Unicode names. 
// -->
// 
// <!-- Latin Extended-B -->
        entities.put("fnof", "\u0192");    // <!ENTITY fnof     "&#402;"> <!-- latin small letter f with hook = function = florin, U+0192 ISOtech -->

// <!-- Greek -->
        entities.put("Alpha", "\u0391");    // <!ENTITY Alpha    "&#913;"> <!-- greek capital letter alpha, U+0391 -->
        entities.put("Beta", "\u0392");    // <!ENTITY Beta     "&#914;"> <!-- greek capital letter beta, U+0392 -->
        entities.put("Gamma", "\u0393");    // <!ENTITY Gamma    "&#915;"> <!-- greek capital letter gamma, U+0393 ISOgrk3 -->
        entities.put("Delta", "\u0394");    // <!ENTITY Delta    "&#916;"> <!-- greek capital letter delta, U+0394 ISOgrk3 -->
        entities.put("Epsilon", "\u0395");    // <!ENTITY Epsilon  "&#917;"> <!-- greek capital letter epsilon, U+0395 -->
        entities.put("Zeta", "\u0396");    // <!ENTITY Zeta     "&#918;"> <!-- greek capital letter zeta, U+0396 -->
        entities.put("Eta", "\u0397");    // <!ENTITY Eta      "&#919;"> <!-- greek capital letter eta, U+0397 -->
        entities.put("Theta", "\u0398");    // <!ENTITY Theta    "&#920;"> <!-- greek capital letter theta, U+0398 ISOgrk3 -->
        entities.put("Iota", "\u0399");    // <!ENTITY Iota     "&#921;"> <!-- greek capital letter iota, U+0399 -->
        entities.put("Kappa", "\u039A");    // <!ENTITY Kappa    "&#922;"> <!-- greek capital letter kappa, U+039A -->
        entities.put("Lambda", "\u039B");    // <!ENTITY Lambda   "&#923;"> <!-- greek capital letter lamda, U+039B ISOgrk3 -->
        entities.put("Mu", "\u039C");    // <!ENTITY Mu       "&#924;"> <!-- greek capital letter mu, U+039C -->
        entities.put("Nu", "\u039D");    // <!ENTITY Nu       "&#925;"> <!-- greek capital letter nu, U+039D -->
        entities.put("Xi", "\u039E");    // <!ENTITY Xi       "&#926;"> <!-- greek capital letter xi, U+039E ISOgrk3 -->
        entities.put("Omicron", "\u039F");    // <!ENTITY Omicron  "&#927;"> <!-- greek capital letter omicron, U+039F -->
        entities.put("Pi", "\u03A0");    // <!ENTITY Pi       "&#928;"> <!-- greek capital letter pi, U+03A0 ISOgrk3 -->
        entities.put("Rho", "\u03A1");    // <!ENTITY Rho      "&#929;"> <!-- greek capital letter rho, U+03A1 -->
// <!-- there is no Sigmaf, and no U+03A2 character either -->
        entities.put("Sigma", "\u03A3");    // <!ENTITY Sigma    "&#931;"> <!-- greek capital letter sigma, U+03A3 ISOgrk3 -->
        entities.put("Tau", "\u03A4");    // <!ENTITY Tau      "&#932;"> <!-- greek capital letter tau, U+03A4 -->
        entities.put("Upsilon", "\u03A5");    // <!ENTITY Upsilon  "&#933;"> <!-- greek capital letter upsilon, U+03A5 ISOgrk3 -->
        entities.put("Phi", "\u03A6");    // <!ENTITY Phi      "&#934;"> <!-- greek capital letter phi, U+03A6 ISOgrk3 -->
        entities.put("Chi", "\u03A7");    // <!ENTITY Chi      "&#935;"> <!-- greek capital letter chi, U+03A7 -->
        entities.put("Psi", "\u03A8");    // <!ENTITY Psi      "&#936;"> <!-- greek capital letter psi, U+03A8 ISOgrk3 -->
        entities.put("Omega", "\u03A9");    // <!ENTITY Omega    "&#937;"> <!-- greek capital letter omega, U+03A9 ISOgrk3 -->
        entities.put("alpha", "\u03B1");    // <!ENTITY alpha    "&#945;"> <!-- greek small letter alpha, U+03B1 ISOgrk3 -->
        entities.put("beta", "\u03B2");    // <!ENTITY beta     "&#946;"> <!-- greek small letter beta, U+03B2 ISOgrk3 -->
        entities.put("gamma", "\u03B3");    // <!ENTITY gamma    "&#947;"> <!-- greek small letter gamma, U+03B3 ISOgrk3 -->
        entities.put("delta", "\u03B4");    // <!ENTITY delta    "&#948;"> <!-- greek small letter delta, U+03B4 ISOgrk3 -->
        entities.put("epsilon", "\u03B5");    // <!ENTITY epsilon  "&#949;"> <!-- greek small letter epsilon, U+03B5 ISOgrk3 -->
        entities.put("zeta", "\u03B6");    // <!ENTITY zeta     "&#950;"> <!-- greek small letter zeta, U+03B6 ISOgrk3 -->
        entities.put("eta", "\u03B7");    // <!ENTITY eta      "&#951;"> <!-- greek small letter eta, U+03B7 ISOgrk3 -->
        entities.put("theta", "\u03B8");    // <!ENTITY theta    "&#952;"> <!-- greek small letter theta, U+03B8 ISOgrk3 -->
        entities.put("iota", "\u03B9");    // <!ENTITY iota     "&#953;"> <!-- greek small letter iota, U+03B9 ISOgrk3 -->
        entities.put("kappa", "\u03BA");    // <!ENTITY kappa    "&#954;"> <!-- greek small letter kappa, U+03BA ISOgrk3 -->
        entities.put("lambda", "\u03BB");    // <!ENTITY lambda   "&#955;"> <!-- greek small letter lamda, U+03BB ISOgrk3 -->
        entities.put("mu", "\u03BC");    // <!ENTITY mu       "&#956;"> <!-- greek small letter mu, U+03BC ISOgrk3 -->
        entities.put("nu", "\u03BD");    // <!ENTITY nu       "&#957;"> <!-- greek small letter nu, U+03BD ISOgrk3 -->
        entities.put("xi", "\u03BE");    // <!ENTITY xi       "&#958;"> <!-- greek small letter xi, U+03BE ISOgrk3 -->
        entities.put("omicron", "\u03BF");    // <!ENTITY omicron  "&#959;"> <!-- greek small letter omicron, U+03BF NEW -->
        entities.put("pi", "\u03C0");    // <!ENTITY pi       "&#960;"> <!-- greek small letter pi, U+03C0 ISOgrk3 -->
        entities.put("rho", "\u03C1");    // <!ENTITY rho      "&#961;"> <!-- greek small letter rho, U+03C1 ISOgrk3 -->
        entities.put("sigmaf", "\u03C2");    // <!ENTITY sigmaf   "&#962;"> <!-- greek small letter final sigma, U+03C2 ISOgrk3 -->
        entities.put("sigma", "\u03C3");    // <!ENTITY sigma    "&#963;"> <!-- greek small letter sigma, U+03C3 ISOgrk3 -->
        entities.put("tau", "\u03C4");    // <!ENTITY tau      "&#964;"> <!-- greek small letter tau, U+03C4 ISOgrk3 -->
        entities.put("upsilon", "\u03C5");    // <!ENTITY upsilon  "&#965;"> <!-- greek small letter upsilon, U+03C5 ISOgrk3 -->
        entities.put("phi", "\u03C6");    // <!ENTITY phi      "&#966;"> <!-- greek small letter phi, U+03C6 ISOgrk3 -->
        entities.put("chi", "\u03C7");    // <!ENTITY chi      "&#967;"> <!-- greek small letter chi, U+03C7 ISOgrk3 -->
        entities.put("psi", "\u03C8");    // <!ENTITY psi      "&#968;"> <!-- greek small letter psi, U+03C8 ISOgrk3 -->
        entities.put("omega", "\u03C9");    // <!ENTITY omega    "&#969;"> <!-- greek small letter omega, U+03C9 ISOgrk3 -->
        entities.put("thetasym", "\u03D1");    // <!ENTITY thetasym "&#977;"> <!-- greek theta symbol, U+03D1 NEW -->
        entities.put("upsih", "\u03D2");    // <!ENTITY upsih    "&#978;"> <!-- greek upsilon with hook symbol, U+03D2 NEW -->
        entities.put("piv", "\u03D6");    // <!ENTITY piv      "&#982;"> <!-- greek pi symbol, U+03D6 ISOgrk3 -->

// <!-- General Punctuation -->
        entities.put("bull", "\u2022");    // <!ENTITY bull     "&#8226;"> <!-- bullet = black small circle, U+2022 ISOpub  -->
// <!-- bullet is NOT the same as bullet operator, U+2219 -->
        entities.put("hellip", "\u2026");    // <!ENTITY hellip   "&#8230;"> <!-- horizontal ellipsis = three dot leader, U+2026 ISOpub  -->
        entities.put("prime", "\u2032");    // <!ENTITY prime    "&#8242;"> <!-- prime = minutes = feet, U+2032 ISOtech -->
        entities.put("Prime", "\u2033");    // <!ENTITY Prime    "&#8243;"> <!-- double prime = seconds = inches, U+2033 ISOtech -->
        entities.put("oline", "\u203E");    // <!ENTITY oline    "&#8254;"> <!-- overline = spacing overscore, U+203E NEW -->
        entities.put("frasl", "\u2044");    // <!ENTITY frasl    "&#8260;"> <!-- fraction slash, U+2044 NEW -->

// <!-- Letterlike Symbols -->
        entities.put("weierp", "\u2118");    // <!ENTITY weierp   "&#8472;"> <!-- script capital P = power set = Weierstrass p, U+2118 ISOamso -->
        entities.put("image", "\u2111");    // <!ENTITY image    "&#8465;"> <!-- black-letter capital I = imaginary part, U+2111 ISOamso -->
        entities.put("real", "\u211C");    // <!ENTITY real     "&#8476;"> <!-- black-letter capital R = real part symbol, U+211C ISOamso -->
        entities.put("trade", "\u2122");    // <!ENTITY trade    "&#8482;"> <!-- trade mark sign, U+2122 ISOnum -->
        entities.put("alefsym", "\u2135");    // <!ENTITY alefsym  "&#8501;"> <!-- alef symbol = first transfinite cardinal, U+2135 NEW -->
// <!-- alef symbol is NOT the same as hebrew letter alef, U+05D0 although the same glyph could be used to depict both characters -->

// <!-- Arrows -->
        entities.put("larr", "\u2190");    // <!ENTITY larr     "&#8592;"> <!-- leftwards arrow, U+2190 ISOnum -->
        entities.put("uarr", "\u2191");    // <!ENTITY uarr     "&#8593;"> <!-- upwards arrow, U+2191 ISOnum-->
        entities.put("rarr", "\u2192");    // <!ENTITY rarr     "&#8594;"> <!-- rightwards arrow, U+2192 ISOnum -->
        entities.put("darr", "\u2193");    // <!ENTITY darr     "&#8595;"> <!-- downwards arrow, U+2193 ISOnum -->
        entities.put("harr", "\u2194");    // <!ENTITY harr     "&#8596;"> <!-- left right arrow, U+2194 ISOamsa -->
        entities.put("crarr", "\u21B5");    // <!ENTITY crarr    "&#8629;"> <!-- downwards arrow with corner leftwards = carriage return, U+21B5 NEW -->
        entities.put("lArr", "\u21D0");    // <!ENTITY lArr     "&#8656;"> <!-- leftwards double arrow, U+21D0 ISOtech -->
// <!-- Unicode does not say that lArr is the same as the 'is implied by' arrow but also does not have any other character for that function. So lArr can be used for 'is implied by' as ISOtech suggests -->
        entities.put("uArr", "\u21D1");    // <!ENTITY uArr     "&#8657;"> <!-- upwards double arrow, U+21D1 ISOamsa -->
        entities.put("rArr", "\u21D2");    // <!ENTITY rArr     "&#8658;"> <!-- rightwards double arrow, U+21D2 ISOtech -->
// <!-- Unicode does not say this is the 'implies' character but does not have  another character with this function so rArr can be used for 'implies' as ISOtech suggests -->
        entities.put("dArr", "\u21D3");    // <!ENTITY dArr     "&#8659;"> <!-- downwards double arrow, U+21D3 ISOamsa -->
        entities.put("hArr", "\u21D4");    // <!ENTITY hArr     "&#8660;"> <!-- left right double arrow, U+21D4 ISOamsa -->

// <!-- Mathematical Operators -->
        entities.put("forall", "\u2200");    // <!ENTITY forall   "&#8704;"> <!-- for all, U+2200 ISOtech -->
        entities.put("part", "\u2202");    // <!ENTITY part     "&#8706;"> <!-- partial differential, U+2202 ISOtech  -->
        entities.put("exist", "\u2203");    // <!ENTITY exist    "&#8707;"> <!-- there exists, U+2203 ISOtech -->
        entities.put("empty", "\u2205");    // <!ENTITY empty    "&#8709;"> <!-- empty set = null set, U+2205 ISOamso -->
        entities.put("nabla", "\u2207");    // <!ENTITY nabla    "&#8711;"> <!-- nabla = backward difference, U+2207 ISOtech -->
        entities.put("isin", "\u2208");    // <!ENTITY isin     "&#8712;"> <!-- element of, U+2208 ISOtech -->
        entities.put("notin", "\u2209");    // <!ENTITY notin    "&#8713;"> <!-- not an element of, U+2209 ISOtech -->
        entities.put("ni", "\u220B");    // <!ENTITY ni       "&#8715;"> <!-- contains as member, U+220B ISOtech -->
        entities.put("prod", "\u220F");    // <!ENTITY prod     "&#8719;"> <!-- n-ary product = product sign, U+220F ISOamsb -->
// <!-- prod is NOT the same character as U+03A0 'greek capital letter pi' though the same glyph might be used for both -->
        entities.put("sum", "\u2211");    // <!ENTITY sum      "&#8721;"> <!-- n-ary summation, U+2211 ISOamsb -->
// <!-- sum is NOT the same character as U+03A3 'greek capital letter sigma' though the same glyph might be used for both -->
        entities.put("minus", "\u2212");    // <!ENTITY minus    "&#8722;"> <!-- minus sign, U+2212 ISOtech -->
        entities.put("lowast", "\u2217");    // <!ENTITY lowast   "&#8727;"> <!-- asterisk operator, U+2217 ISOtech -->
        entities.put("radic", "\u221A");    // <!ENTITY radic    "&#8730;"> <!-- square root = radical sign, U+221A ISOtech -->
        entities.put("prop", "\u221D");    // <!ENTITY prop     "&#8733;"> <!-- proportional to, U+221D ISOtech -->
        entities.put("infin", "\u221E");    // <!ENTITY infin    "&#8734;"> <!-- infinity, U+221E ISOtech -->
        entities.put("ang", "\u2220");    // <!ENTITY ang      "&#8736;"> <!-- angle, U+2220 ISOamso -->
        entities.put("and", "\u2227");    // <!ENTITY and      "&#8743;"> <!-- logical and = wedge, U+2227 ISOtech -->
        entities.put("or", "\u2228");    // <!ENTITY or       "&#8744;"> <!-- logical or = vee, U+2228 ISOtech -->
        entities.put("cap", "\u2229");    // <!ENTITY cap      "&#8745;"> <!-- intersection = cap, U+2229 ISOtech -->
        entities.put("cup", "\u222A");    // <!ENTITY cup      "&#8746;"> <!-- union = cup, U+222A ISOtech -->
        entities.put("int", "\u222B");    // <!ENTITY int      "&#8747;"> <!-- integral, U+222B ISOtech -->
        entities.put("there4", "\u2234");    // <!ENTITY there4   "&#8756;"> <!-- therefore, U+2234 ISOtech -->
        entities.put("sim", "\u223C");    // <!ENTITY sim      "&#8764;"> <!-- tilde operator = varies with = similar to, U+223C ISOtech -->
// <!-- tilde operator is NOT the same character as the tilde, U+007E, although the same glyph might be used to represent both  -->
        entities.put("cong", "\u2245");    // <!ENTITY cong     "&#8773;"> <!-- approximately equal to, U+2245 ISOtech -->
        entities.put("asymp", "\u2248");    // <!ENTITY asymp    "&#8776;"> <!-- almost equal to = asymptotic to, U+2248 ISOamsr -->
        entities.put("ne", "\u2260");    // <!ENTITY ne       "&#8800;"> <!-- not equal to, U+2260 ISOtech -->
        entities.put("equiv", "\u2261");    // <!ENTITY equiv    "&#8801;"> <!-- identical to, U+2261 ISOtech -->
        entities.put("le", "\u2264");    // <!ENTITY le       "&#8804;"> <!-- less-than or equal to, U+2264 ISOtech -->
        entities.put("ge", "\u2265");    // <!ENTITY ge       "&#8805;"> <!-- greater-than or equal to, U+2265 ISOtech -->
        entities.put("sub", "\u2282");    // <!ENTITY sub      "&#8834;"> <!-- subset of, U+2282 ISOtech -->
        entities.put("sup", "\u2283");    // <!ENTITY sup      "&#8835;"> <!-- superset of, U+2283 ISOtech -->
        entities.put("nsub", "\u2284");    // <!ENTITY nsub     "&#8836;"> <!-- not a subset of, U+2284 ISOamsn -->
        entities.put("sube", "\u2286");    // <!ENTITY sube     "&#8838;"> <!-- subset of or equal to, U+2286 ISOtech -->
        entities.put("supe", "\u2287");    // <!ENTITY supe     "&#8839;"> <!-- superset of or equal to, U+2287 ISOtech -->
        entities.put("oplus", "\u2295");    // <!ENTITY oplus    "&#8853;"> <!-- circled plus = direct sum, U+2295 ISOamsb -->
        entities.put("otimes", "\u2297");    // <!ENTITY otimes   "&#8855;"> <!-- circled times = vector product, U+2297 ISOamsb -->
        entities.put("perp", "\u22A5");    // <!ENTITY perp     "&#8869;"> <!-- up tack = orthogonal to = perpendicular, U+22A5 ISOtech -->
        entities.put("sdot", "\u22C5");    // <!ENTITY sdot     "&#8901;"> <!-- dot operator, U+22C5 ISOamsb -->
// <!-- dot operator is NOT the same character as U+00B7 middle dot -->

// <!-- Miscellaneous Technical -->
        entities.put("lceil", "\u2308");    // <!ENTITY lceil    "&#8968;"> <!-- left ceiling = APL upstile, U+2308 ISOamsc  -->
        entities.put("rceil", "\u2309");    // <!ENTITY rceil    "&#8969;"> <!-- right ceiling, U+2309 ISOamsc  -->
        entities.put("lfloor", "\u230A");    // <!ENTITY lfloor   "&#8970;"> <!-- left floor = APL downstile, U+230A ISOamsc  -->
        entities.put("rfloor", "\u230B");    // <!ENTITY rfloor   "&#8971;"> <!-- right floor, U+230B ISOamsc  -->
        entities.put("lang", "\u2329");    // <!ENTITY lang     "&#9001;"> <!-- left-pointing angle bracket = bra, U+2329 ISOtech -->
// <!-- lang is NOT the same character as U+003C 'less than sign'  or U+2039 'single left-pointing angle quotation mark' -->
        entities.put("rang", "\u232A");    // <!ENTITY rang     "&#9002;"> <!-- right-pointing angle bracket = ket, U+232A ISOtech -->
// <!-- rang is NOT the same character as U+003E 'greater than sign'  or U+203A 'single right-pointing angle quotation mark' -->

// <!-- Geometric Shapes -->
        entities.put("loz", "\u25CA");    // <!ENTITY loz      "&#9674;"> <!-- lozenge, U+25CA ISOpub -->

// <!-- Miscellaneous Symbols -->
        entities.put("spades", "\u2660");    // <!ENTITY spades   "&#9824;"> <!-- black spade suit, U+2660 ISOpub -->
// <!-- black here seems to mean filled as opposed to hollow -->
        entities.put("clubs", "\u2663");    // <!ENTITY clubs    "&#9827;"> <!-- black club suit = shamrock, U+2663 ISOpub -->
        entities.put("hearts", "\u2665");    // <!ENTITY hearts   "&#9829;"> <!-- black heart suit = valentine, U+2665 ISOpub -->
        entities.put("diams", "\u2666");    // <!ENTITY diams    "&#9830;"> <!-- black diamond suit, U+2666 ISOpub -->
    }

}
