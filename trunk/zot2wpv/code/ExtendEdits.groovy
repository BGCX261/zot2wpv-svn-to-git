import au.com.bytecode.opencsv.CSVReader
import au.com.bytecode.opencsv.CSVWriter
import java.util.regex.Matcher
import java.security.MessageDigest

import name.fraser.neil.plaintext.diff_match_patch;
import name.fraser.neil.plaintext.diff_match_patch.Diff
import name.fraser.neil.plaintext.diff_match_patch.Operation
import java.util.regex.Pattern

extDir  = new File(".")
dataDir = new File(extDir, 'data')
revisionsDir = new File(dataDir, 'article-revisions')

corpusDir = new File(dataDir, 'corpus')
vandalismDir = new File(corpusDir, 'vandalism')
regularDir = new File(corpusDir, 'regular')

vandalismDir.mkdirs()
regularDir.mkdirs()

visibleDir = new File(dataDir, 'visible')
vandalismVisDir = new File(visibleDir, 'vandalism')
regularVisDir = new File(visibleDir, 'regular')

vandalismVisDir.mkdirs()
regularVisDir.mkdirs()

File findRevisionFile(String revId)
{
   revId = revId + '.txt'
   File revFile = null
   revisionsDir.eachDir { File dir ->
      if (revFile == null) {
        File f = new File(dir, revId)
        if (f.exists()) { revFile = f }
      }
   }

   revFile
}

String mungeFieldName(String name) { name == 'class' ? 'type' : name }

Map readCSVtoMap(String fileName) {
  reader = new CSVReader(new FileReader(new File(dataDir, fileName)))
  String[] fieldNames = reader.readNext()
  List data = reader.readAll()
  Map indexedData = [:]
  data.each { String[] row ->
     Map fields = [:]
     fieldNames.eachWithIndex { name, x -> fields[mungeFieldName(name)] = row[x] }
     indexedData[row[0]] = new Expando(fields)
  }
  indexedData
}

Expando arrayToExpando(String[] row, String[] fieldNames)
{
  Map fields = [:]
  fieldNames.eachWithIndex { name, x -> fields[mungeFieldName(name)] = row[x].replace(/"/, /'/) }
  new Expando(fields)
}

//new File(dataDir, 'edits.csv').withReader {
//   def reader = new CSVReader(it)
//
//   String[] fieldNames = reader.readNext()
//
//   while ((row = reader.readNext()) != null) {
//      def edit = arrayToExpando(row, fieldNames)
//
//      Boolean anon = edit.editor ==~ /^\d+\.\d+\.\d+\.\d+$/
//
//      String comment = edit.editcomment
//       if (comment == "null") {
//          comment = ""
//       }
//       Matcher m = comment =~ /^(\/\*[^*]+\*\/)?\s*(.*)$/
//       assert m.matches()
//      String edit_section = m.group(1)
//      comment = m.group(2).trim()
//
//      def annotation = gold[edit.editid]
//
//      xedits.writeNext (['X' + edit.editid + ".txt", annotation.type
//         , annotation.annotators, annotation.totalannotators
//         , edit.oldrevisionid, edit.newrevisionid
//         , anon ? "1" : "0", edit.editor, edit.edittime
//         , edit_section ?: "", comment, comment.length() as String
//         , edit.articletitle] as String[])
//   }
//}

def printDiffString(String prefix, Diff d) { d.text.split('\n').each { println "$prefix $it" } }

def listDifferences(List<Diff> diff)
{
  diff.each { Diff d -> if (d.operation == Operation.DELETE ) printDiffString ('-', d) }
  diff.each { Diff d -> if (d.operation == Operation.INSERT ) printDiffString ('+', d) }
}

differ = new diff_match_patch()
differ.Diff_Timeout = 3

def listDifferences(String newid, String oldid)
{
  println "newid $newid oldid $oldid\n"

  List<Diff> differences = differ.diff_main(findRevisionFile(oldid).text, findRevisionFile(newid).text)
  differ.diff_cleanupSemantic(differences)
  listDifferences(differences)

  println '\n---\n'
}

//nextCode = initialCode = new Character(0x8000 as char)
//
//wordPattern = ~/[\w\u8000-\uCFFE]+/
//urlPattern = ~'(?x) ^(https?):// ([^/:]+) (?::(\\d+))?'
//
//codePattern = ~/[\u8000-\uCFFE]/
//
//assert ('\uCFFF' > '\uCFFE')
//
//def encodeWords(Map<String, Character> plainToCode, Map<Character, String> codeToPlain, String text)
//{
//  StringBuffer sbuf = new StringBuffer(text.length());
//
//  Matcher m = wordPattern.matcher(text);
//  while (m.find()) {
//      String word = m.group()
//
//      Character code = plainToCode[word]
//
//      if (code == null) {
//         nextCode = nextCode + 1
//         plainToCode[word] = code = nextCode
//         codeToPlain[code] = word
//      }
//
//      m.appendReplacement(sbuf, code as String);
//  }
//  m.appendTail(sbuf);
//
//  sbuf.toString();
//}
//
////def encodeURLs(Map<String, Character> plainToCode, Map<Character, String> codeToPlain, String text)
////{
////  StringBuffer sbuf = new StringBuffer(text.length());
////
////  Matcher m = urlPattern.matcher(text);
////  while (m.find()) {
////      String word = m.group()
////
////      Character code = plainToCode[word]
////
////      if (code == null) {
////         nextCode = nextCode + 1
////         plainToCode[word] = code = nextCode
////         codeToPlain[code] = word
////      }
////
////      m.appendReplacement(sbuf, code as String);
////  }
////  m.appendTail(sbuf);
////
////  sbuf.toString();
////}
////
//
//def decodeWords(Map<Character, String> codeToPlain, String text)
//{
//  StringBuffer sbuf = new StringBuffer(text.length());
//
//  Matcher m = codePattern.matcher(text);
//  while (m.find()) {
//      m.appendReplacement(sbuf, codeToPlain[m.group().charAt(0)]);
//  }
//  m.appendTail(sbuf);
//
//  sbuf.toString();
//}

editorsForHashes = [:]
editCountForEditors = [:]
revertedEditCountForEditors = [:]

md = MessageDigest.getInstance("SHA");

String hashForRevision(String revId)
{
  md.reset()
  md.update(findRevisionFile(revId).text as byte[])

//  def d = md.digest()
//  String s = new String(d, 'ISO-8859-1')
//  assert (s.getBytes('ISO-8859-1') == d)

  // Using ISO-8859-1 to turn 8bit bytes into characters without having to worry about bad code sequences.
  new String(md.digest(), 'ISO-8859-1')
}

String hashForText(String text)
{
  md.reset()
  md.update(text as byte[])

  // Using ISO-8859-1 to turn 8bit bytes into characters without having to worry about bad code sequences.
  new String(md.digest(), 'ISO-8859-1')
}

// Return flags for common replacement patterns.
// ['whole article is blanked',
//  'whole article replaced by new text',
//  'whole section (including header!) replaced by new text',
//  'whole content of section replaced by new text']
def checkForReplacement(List<Diff> differences)
{
  Iterator<Diff> i = differences.iterator()

  // No content at all?  Not much of an article!
  if (!i.hasNext()) return [false, false, false, false]

  Diff d = i.next()

  // Delete starting at beginning of article?
  if (d.operation == Operation.DELETE) {
     // And that's it?  Then this is a blanking edit!
     if (!i.hasNext()) return [true, false, false, false];
     d = i.next()
     if (d.operation == Operation.INSERT) {
        // We've got some replacement.  Is it the whole article?
        // Certainly if there is no more.
        if (!i.hasNext()) return [false, true, false, false];
        d = i.next()
        // Or if what is next is just whitespace.
        if (d.text ==~ /\s*/) return [false, true, false, false];
     }
    // That's all we check for at the beginning of article (for now).
    return [false, false, false, false]
  }

  // A typical section replacement by a vandal looks like:
  // EQUAL  "beginning of article"
  // DELETE "== Header ==\n...old content..."
  // INSERT "...new content..."
  // EQUAL  "rest of article"
  // There will also be an edit comment with just the autogenerated section title in it.

  if ((d.operation == Operation.EQUAL) && i.hasNext()) {
     d = i.next()
     if ((d.operation == Operation.DELETE) && i.hasNext()) {
        // Is first line of delete a section header?
        Boolean sectionHeaderDeleted = d.text ==~ /(?m)^==.+?==$/
        d = i.next()
        if (d.operation == Operation.INSERT) {
          if (sectionHeaderDeleted) {
             // Check if the header just changed title.
             // This would not be so very typical of vandalism,
             // But sometimes they do mess with it, so don't quit on this check.
             sectionHeaderDeleted = !(d.text ==~ /(?m)^==.+?==$/)
          }
        }
        // This might be the last section.
        if (!i.hasNext()) return [false, false, sectionHeaderDeleted, true];
        d = i.next()
        // Is that it for changes?
        if ((d.operation == Operation.EQUAL) && !i.hasNext()) {
           // Then this is a section replacement if the rest is the beginning of another section.
           if (d.text ==~ /(?m)^==.+?==$/) return [false, false, sectionHeaderDeleted, true];
        }
     }
  }

  return [false, false, false, false]
}

// An measure of the article's complexity in terms of wikiformatting.
// Vandals (except mostly for advertisers and clever pranksters) don't use wikimarkup.
// In fact they often break the format in obvious ways, which would be another thing to measure.
def countWikiMarkup(String text)
{
   (text.split(/\{\{/).length - 1) + (text.split(/\[\[/).length - 1) + (text.split(/(?m)^==/).length - 1)    
}

String[] collectInsertedLines(List<Diff> differences)
{
   def lines = differences.collect { Diff d ->
      switch (d.operation) {
         case Operation.DELETE : null; break
         case Operation.INSERT : d.text + ' '; break
         case Operation.EQUAL  : d.text.contains('\n') ? '\n' : null; break
         default : null
      }
   }

   // Turn that list of strings into an array, omitting nulls (and empty strings for good measure).
   (lines.grep { (it && (it != '\n')) }) as String[]
}

String differencesToText(List<Diff> differences)
{
  String[] lines = collectInsertedLines(differences)

  String text = lines.join('')
  
  text = text.replaceAll(/(?s)[\n]+/, '\n')

  if (text.startsWith('\n')) { text = text.substring(1) }

  if (!text.endsWith('\n'))  { text = text + '\n' }

  text
}

assert (((['\n', 'abc', null, 'd\n', '\n'].grep { (it && (it != '\n')) }) as String[]).join('') == 'abcd\n')

def testMatching(String text, exp_all_cap_word_count, exp_word_count, exp_char_cap_count, exp_char_alpha_count)
{
  def all_cap_word_count = 0
  def word_count = 0
  def char_cap_count = 0
  def char_alpha_count = 0

//  text.eachMatch(/(?s)[\p{Lu}]{2,}(?![\p{L}])/) { all_cap_word_count += 1 }
  (text =~ /(?s)[\p{Lu}]{2,}(?![\p{L}])/).each { ++all_cap_word_count }
  (text =~ /(?s)[\p{L}]{2,}/).each { ++word_count }
  (text =~ /(?s)[\p{Lu}]/).each { ++char_cap_count }
  (text =~ /(?s)[\p{L}]/).each { ++char_alpha_count }

  assert (all_cap_word_count == exp_all_cap_word_count)
  assert (word_count == exp_word_count)
  assert (char_cap_count == exp_char_cap_count)
  assert (char_alpha_count == exp_char_alpha_count)
}

testMatching("A BAD mojo Daddy WO", 2, 4, 7, 15)
testMatching("A BAD mojo Daddy", 1, 3, 5, 13)

def ratio(Double measure, Double scale)
{
   ((scale > 0) ? (Math.log(scale) * (measure / scale)) : 0)
}

badWordsRegEx = /(?i)(\bg[ay]+\b)|(\bf[uck]{3,})|(\blol[ol]*\b)|(\bi+\s*l[ike]{3,}\b)|(butt[^oe])|(\bass+[hole]*\b)|(\bhomos\b)/ +
        /|(\bw[ei]{2}rd\b)|(\!{4,})|(\by[o]?ur\s*mo[mt])/

badWordsPattern = Pattern.compile(badWordsRegEx)

def analyzeEdit(edit)
{
  String newtext = findRevisionFile(edit.newrevisionid).text.normalize()
  String newhash = edit.articletitle + hashForText(newtext)

  String oldtext = findRevisionFile(edit.oldrevisionid).text.normalize()

  def old_size = oldtext.length()
  def new_size = newtext.length()

  def old_markup_count = countWikiMarkup(oldtext)
  def new_markup_count = countWikiMarkup(newtext)

  def codec = new WordAsCharCODEC();

  String xoldtext = codec.encodeWords(oldtext)
  String xnewtext = codec.encodeWords(newtext)

  if (codec.encoderError()) {
     println "Overflowed text encoding! (editid = ${edit.editid})"
     xoldtext = oldtext
     xnewtext = newtext
  }
  
  List<Diff> differences = differ.diff_main(xoldtext, xnewtext)
  differ.diff_cleanupSemantic(differences)

  if (!codec.encoderError()) {
    differences.each { Diff d -> d.text = codec.decodeWords(d.text) }
  }

  def (is_article_blanked, is_article_replacement, is_section_replacement, is_section_content_replacement) = checkForReplacement(differences)

  String inserted_text = differencesToText(differences)

  String oldVisibleText = WikiTextCleaner.clearWikiText(oldtext)
  String newVisibleText = WikiTextCleaner.clearWikiText(newtext)

  def plainCodec = new WordAsCharCODEC();

  String xoldvistext = plainCodec.encodeWords(oldVisibleText)
  String xnewvistext = plainCodec.encodeWords(newVisibleText)

  if (plainCodec.encoderError()) {
     println "Overflowed text encoding! (editid = ${edit.editid})"
     xoldvistext = oldVisibleText
     xnewvistext = newVisibleText
  }

  differences = differ.diff_main(xoldvistext, xnewvistext)
  differ.diff_cleanupSemantic(differences)

  if (!plainCodec.encoderError()) {
    differences.each {Diff d -> d.text = plainCodec.decodeWords(d.text) }
  }

  String visible_insertion = differencesToText(differences)

  def borked_markup = visible_insertion.length() > inserted_text.length()
  if (borked_markup) {
    // What we really want is a way to figure out that what's happened is that
    // the edit has broken the markup and still pick up the visible text.
    visible_insertion = inserted_text
  }

//  def all_cap_words = inserted_text.grep(/[A-Z]{2,}/)
//  def all_cap_words = (inserted_text =~ /[A-Z]{4,}/).collect { it }
  def all_cap_word_count = 0
  def word_count = 0
  def char_cap_count = 0
  def char_alpha_count = 0

  (visible_insertion =~ /(?s)[\p{Lu}]{2,}(?![\p{L}])/).each { ++all_cap_word_count }
  (visible_insertion =~ /(?s)[\p{L}]{2,}/).each { ++word_count }
  (visible_insertion =~ /(?s)[\p{Lu}]/).each { ++char_cap_count }
  (visible_insertion =~ /(?s)[\p{L}]/).each { ++char_alpha_count }

//  def all_words = (inserted_text =~ /\p{javaLowerCase}+/).collect { it }.sort()

  def bad_words = badWordsPattern.matcher(inserted_text).collect { it }

//  println inserted_text
//  println all_cap_word_count
//  println all_cap_words
//  println bad_words.size()

  // Cleanup comment text.
  String comment = edit.editcomment
  // Data comes in with a literal "null" string when it should be an empty string.
  if (comment == "null") {
    comment = ""
  }
  // Split off the automatically generated section comment.
  Matcher sm = comment =~ /^(\/\*[^*]+\*\/)?\s*(.*)$/
  assert sm.matches()
  String edit_section = sm.group(1)
  comment = sm.group(2).trim()

  // Blank Automatic Edit Summary comments.
  // We could use their checks here, but no time for now.
  if (comment.startsWith('[[WP:AES|')) comment = ''
  if (comment.startsWith('[[Wikipedia:AES|')) comment = ''

  Boolean revert_comment = (comment =~ /(?i)revert/).find()
  Boolean vandal_comment = (comment =~ /(?i)vandalism/).find()

  // This edit is a reversion if we've seen this hash earlier in time.
  Boolean is_a_reversion = editorsForHashes.containsKey(newhash)

  // If this edit is a revert, then mark the editor of the previous version as suspicious.
  if (revert_comment || vandal_comment || is_a_reversion) {
     def vandal = null
     Matcher vm = comment =~ /(?i)revert.*? edits? by \[\[Special:Contributions\/([^|]+)/
     if (vm.find()) vandal = vm.group(1)
     if (!vandal) {
       String oldhash = edit.articletitle + hashForText(oldtext)
       vandal = editorsForHashes[oldhash]
     }
     if ((vandal != null) && (vandal != edit.editor)) {
//        println "Reverted editor $vandal"
        revertedEditCountForEditors[vandal] = (revertedEditCountForEditors[vandal] ?: 0) + 1
     }
  } else {
     editorsForHashes[newhash] = edit.editor
  }

  // Should change this to track total edits and number of reverted edits to date.
  // And using WP API is probably better because of more info.

  def editor_edit_count = editCountForEditors[edit.editor] ?: 0
  editCountForEditors[edit.editor] = editor_edit_count + 1
  def editor_revert_count = revertedEditCountForEditors[edit.editor] ?: 0

//  Boolean reverted = false

  if (editor_revert_count > 0) println "Previously reverted editor ${edit.editor} edits again!"

  Boolean editor_anon = edit.editor ==~ /^\d+\.\d+\.\d+\.\d+$/

  return new Expando(comment:comment, revert_comment:revert_comment, vandal_comment:vandal_comment
    , is_a_reversion:is_a_reversion, edit_section:edit_section
    , editor_edit_count:editor_edit_count, editor_revert_count:editor_revert_count
    , editor_anon:editor_anon
    , old_size:old_size, new_size:new_size, old_markup_count:old_markup_count, new_markup_count:new_markup_count
    , is_article_blanked:is_article_blanked, is_article_replacement:is_article_replacement
    , is_section_replacement:is_section_replacement, is_section_content_replacement:is_section_content_replacement
    , all_cap_word_count:all_cap_word_count, word_count:word_count
    , all_cap_word_ratio:ratio(all_cap_word_count, word_count)
    , bad_word_count:bad_words.size()
    , char_cap_count:char_cap_count, char_alpha_count:char_alpha_count
    , char_cap_ratio:ratio(char_cap_count, char_alpha_count)
    , char_total:visible_insertion.length()
    , inserted_text:inserted_text, visible_insertion:visible_insertion, borked_markup:borked_markup)
}


gold = readCSVtoMap('gold-annotations.csv')

edits = readCSVtoMap('edits.csv')

//edits.values().grep { it.editid in ['20366', '31723', '373', '43633', '24828', '20860', '19616', '20366', '45222', '8473', '16989']}.each {
//  println it
//  println gold[it.editid]
//  println analyzeEdit(it)
//  println ''
//}
//
//return null

String fixQuotes(String s) { s ? s.replace('"', "'") : s }

xedits = new CSVWriter(new BufferedWriter(new FileWriter(new File(dataDir, "xedits.csv"))))
xedits.writeNext(["_editid", "_label" // , "_annotators", "_totalannotators"
    , "_oldrevisionid", "_newrevisionid", "_edittime"
    , "_editor", "_anonymous_editor", "_editor_edit_count", "_editor_revert_count"
    , "_is_a_reversion", "_revert_comment", "_vandal_comment"
    , "_editsection", "_editcomment", "_commentlength"
    , "_articletitle"
        , "_old_size", "_new_size", "_size_diff", "_size_diff_ratio"
        , "_old_markup_count", "_new_markup_count", "_markup_count_diff", "_markup_diff_ratio"
        , "_is_article_blanked", "_is_article_replacement"
        , "_is_section_replacement", "_is_section_content_replacement"
        , "_all_cap_word_count", "_word_count", "_all_cap_word_ratio"
        , "_char_cap_count", "_char_alpha_count", "_char_cap_ratio"
        , "_bad_word_count", "_borked_markup"
  ] as String[])

edits.values().sort{ it.edittime }.each { edit ->
  def result = analyzeEdit(edit)

  def annotation = gold[edit.editid]

  if (annotation.type.startsWith('!')) {
     annotation.type = (annotation.type == '!regular') ? 'vandalism' : 'regular'
  }

  edit.editor = fixQuotes(edit.editor)
  edit.articletitle = fixQuotes(edit.articletitle)
  result.edit_section = fixQuotes(result.edit_section)
  result.comment = fixQuotes(result.comment)

  xedits.writeNext (['X' + edit.editid /*+ ".txt"*/, annotation.type
//          , annotation.annotators, annotation.totalannotators
          , edit.oldrevisionid, edit.newrevisionid, edit.edittime
          , edit.editor, result.editor_anon ? "1" : "0"
          , result.editor_edit_count as String, result.editor_revert_count as String
          , result.is_a_reversion ? "1" : "0", result.revert_comment ? "1" : "0", result.vandal_comment ? "1" : "0"
          , result.edit_section ?: "", result.comment, result.comment.length() as String
          , edit.articletitle
          , result.old_size, result.new_size
          , result.new_size - result.old_size, ratio(result.new_size - result.old_size, result.old_size)
          , result.old_markup_count, result.new_markup_count
          , result.new_markup_count - result.old_markup_count, ratio(result.new_markup_count - result.old_markup_count, result.old_markup_count)
          , result.is_article_blanked ? "1" : "0", result.is_article_replacement ? "1" : "0"
          , result.is_section_replacement ? "1" : "0", result.is_section_content_replacement ? "1" : "0"
          , result.all_cap_word_count, result.word_count, result.all_cap_word_ratio
          , result.char_cap_count, result.char_alpha_count, result.char_cap_ratio
          , result.bad_word_count, result.borked_markup ? "1" : "0"
  ] as String[])

  new File(annotation.type == 'regular' ? regularDir : vandalismDir, 'X' + edit.editid).write(result.inserted_text, 'UTF-8')
  new File(annotation.type == 'regular' ? regularVisDir : vandalismVisDir, 'X' + edit.editid).write(result.visible_insertion, 'UTF-8')
}

xedits.close()
