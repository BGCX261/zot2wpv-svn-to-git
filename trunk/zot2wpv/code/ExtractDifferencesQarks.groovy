
import com.qarks.util.files.diff.Diff
import com.qarks.util.CancellableImpl
import com.qarks.util.files.diff.ParsedFile
import com.qarks.util.files.diff.FileLine
import com.qarks.util.files.diff.FileDiffResult

extDir  = new File(".")
dataDir = new File(extDir, 'data')
revisionsDir = new File(dataDir, 'article-revisions')

File findRevisionFile(String revId)
{
   revId = revId + '.txt'
   File revFile = null
   revisionsDir.eachDir { File dir ->
      if (revFile == null) {
        File f = new File(dir, revId)
        if (f.exists()) { revFile = f }
      }
   }

   revFile
}

canceller = new CancellableImpl()

def listDifferences(ParsedFile pf)
{
   pf.lines.each { FileLine line ->
      if (line.status != FileLine.UNCHANGED) println "${line.index}:${FileLine.statusToString(line.status)}: ${line}"
   }
}

def listDifferences(FileDiffResult diff)
{
   FileLine[] leftLines = diff.leftFile.lines
   FileLine[] rightLines = diff.rightFile.lines

   assert (leftLines.length == rightLines.length)

   leftLines.length.times { x ->
      FileLine left = leftLines[x]
      FileLine right = rightLines[x]

      if (left.status != FileLine.UNCHANGED) println "L${left.index}:${FileLine.statusToString(left.status)}: ${left}"
      if (right.status != FileLine.UNCHANGED) println "R${right.index}:${FileLine.statusToString(right.status)}: ${right}"
   }
}

def listDifferences(String newid, String oldid)
{
  def differences = Diff.diff(findRevisionFile(oldid).getText('UTF-8'), findRevisionFile(newid).getText('UTF-8'), canceller, false)
  differences = Diff.format(differences)
  differences = Diff.format(differences)
  listDifferences(differences)
}
// Edit :  20366    Vandalism    Editor :  205.202.138.53    newid :  329076459    oldid :  328513217

// Edit :  31723    Regular    Editor :  Wconaventura    newid :  328713139    oldid :  328713088
//listDifferences('328713139', '328713088')

// Edit :  43633    Vandalism    Editor :  67.193.144.69    newid :  328121726    oldid :  328121608
//listDifferences('328121726', '328121608')

// This should see the new line as a single insertion without anything saved from the old line.
// Or maybe not.  The new text is ALL CAPS, while the preserved words are not.
// So insertion analysis should be on the true minimal change, whereas phrasal analysis should
// see words surrounding and between the inserted words.
// This edit is also a mass delete.  Ton of text with a lot of wikimarkup replaced by much less with no markup.
// Edit :  24828    Vandalism    Editor :  70.251.133.197    newid :  328314831    oldid :  328313468
//listDifferences('328314831', '328313468')

// Edit :  20860    Vandalism    Editor :  92.3.230.178    newid :  329295100    oldid :  329056440
//listDifferences('329295100', '329056440')

// Edit :  19616    Vandalism    Editor :  74.62.72.98    newid :  329266740    oldid :  327850271
// A line splitting insertion.
listDifferences('329266740', '327850271')
