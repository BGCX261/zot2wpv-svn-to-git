import au.com.bytecode.opencsv.CSVReader;

extDir  = new File(".")
dataDir = new File(extDir, 'data')

corpusDir = new File(dataDir, 'corpus')
vandalismDir = new File(corpusDir, 'vandalism')
regularDir = new File(corpusDir, 'regular')

vandalismDir.mkdirs()
regularDir.mkdirs()

new File(dataDir, 'text.csv').withReader {
   def reader = new CSVReader(it)

   String[] row = reader.readNext()

   def currentEditId = null
   PrintWriter currentPrinter = null

   while ((row = reader.readNext()) != null) {
      def editId = row[0]

      if (editId != currentEditId) {
         currentPrinter?.close()
         def label = row[1]
         if (!((label == "vandalism") || (label == "regular"))) {
            println "Bad label (${label}) for editid ${editId}"
         }
         File editFile = new File((label == "vandalism") ? vandalismDir : regularDir, editId + ".txt")
         currentPrinter = editFile.newPrintWriter()
         currentEditId = editId
      }

      if (row[2] == "Y") {
         currentPrinter.println (row[5])
      }
   }

   currentPrinter?.close() 
}
