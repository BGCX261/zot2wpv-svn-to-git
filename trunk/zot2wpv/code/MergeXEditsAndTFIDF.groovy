import au.com.bytecode.opencsv.CSVReader
import au.com.bytecode.opencsv.CSVWriter
import java.util.regex.Matcher
import java.security.MessageDigest

extDir  = new File(".")
dataDir = new File(extDir, 'data')
workspaceDir = new File(extDir, 'workspace')

revisionsDir = new File(dataDir, 'article-revisions')

String mungeFieldName(String name) { name == 'class' ? 'type' : name }

Map readCSVtoMap(String fileName) {
  reader = new CSVReader(new FileReader(new File(dataDir, fileName)))
  String[] fieldNames = reader.readNext()
  List data = reader.readAll()
  Map indexedData = [:]
  data.each { String[] row ->
     Map fields = [:]
     fieldNames.eachWithIndex { name, x -> fields[mungeFieldName(name)] = row[x] }
     indexedData[row[0]] = new Expando(fields)
  }
  indexedData
}

Expando arrayToExpando(String[] row, String[] fieldNames)
{
  Map fields = [:]
  fieldNames.eachWithIndex { name, x -> fields[mungeFieldName(name)] = row[x].replace(/"/, /'/) }
  new Expando(fields)
}

xedits = readCSVtoMap('xedits.csv')

xedits_tfidf = new CSVWriter(new BufferedWriter(new FileWriter(new File(workspaceDir, "xedits_tfidf.csv"))))

//new File(workspaceDir, 'tfidf_corpus_short.csv').withReader {
new File(workspaceDir, 'tfidf_corpus.csv').withReader {
   def reader = new CSVReader(it)

   String[] fieldNames = reader.readNext()

   assert (fieldNames[-1] == 'id')
   assert (fieldNames[-2] == 'label')

   List allFieldNames = (fieldNames as List) + [
      "_anonymous_editor", "_editor_edit_count", "_editor_revert_count"
      , "_is_a_reversion", "_revert_comment", "_vandal_comment"
      , "_commentlength"
      , "_old_size", "_new_size", "_size_diff"
      , "_old_markup_count", "_new_markup_count", "_markup_count_diff"
      , "_is_article_blanked", "_is_article_replacement"
      , "_is_section_replacement", "_is_section_content_replacement"
      , "_all_cap_word_count", "_bad_word_count"
  ]

   xedits_tfidf.writeNext (allFieldNames as String[])

   def lineNum = 0

   while ((row = reader.readNext()) != null) {
      String id = row[-1]
      def theXEdit = xedits.remove(id)

      assert (theXEdit != null)

      if (++lineNum % 100 == 0) println "Merging $lineNum"

      List fields = (row as List) + [
              theXEdit._anonymous_editor, theXEdit._editor_edit_count, theXEdit._editor_revert_count
              , theXEdit._is_a_reversion, theXEdit._revert_comment, theXEdit._vandal_comment
              , theXEdit._commentlength
              , theXEdit._old_size, theXEdit._new_size, theXEdit._size_diff
              , theXEdit._old_markup_count, theXEdit._new_markup_count, theXEdit._markup_count_diff
              , theXEdit._is_article_blanked, theXEdit._is_article_replacement
              , theXEdit._is_section_replacement, theXEdit._is_section_content_replacement
              , theXEdit._all_cap_word_count, theXEdit._bad_word_count
      ]
      xedits_tfidf.writeNext (fields as String[])
   }

   String[] dummyRow = new String[fieldNames.length]

   (0..<(dummyRow.length)).each { dummyRow[it] = "0" }

   println "Merged $lineNum rows"
   println "Filling ${xedits.keySet().size()}"

   lineNum = 0

//   (xedits.keySet() as List)[0..20].each { String id ->
   xedits.keySet().each { String id ->
      Expando theXEdit = xedits[id]
      dummyRow[-1] = id
      dummyRow[-2] = theXEdit._label

     if (++lineNum % 100 == 0) println "Merging $lineNum"


     List fields = (dummyRow as List) + [
             theXEdit._anonymous_editor, theXEdit._editor_edit_count, theXEdit._editor_revert_count
             , theXEdit._is_a_reversion, theXEdit._revert_comment, theXEdit._vandal_comment
             , theXEdit._commentlength
             , theXEdit._old_size, theXEdit._new_size, theXEdit._size_diff
             , theXEdit._old_markup_count, theXEdit._new_markup_count, theXEdit._markup_count_diff
             , theXEdit._is_article_blanked, theXEdit._is_article_replacement
             , theXEdit._is_section_replacement, theXEdit._is_section_content_replacement
             , theXEdit._all_cap_word_count, theXEdit._bad_word_count
     ]
     xedits_tfidf.writeNext (fields as String[])
   }
}

xedits_tfidf.close()
