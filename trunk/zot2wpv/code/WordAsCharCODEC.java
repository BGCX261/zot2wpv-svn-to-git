import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordAsCharCODEC
{
    final static int initialCode = 0x8000;

    int nextCode = initialCode;

    Map<String, Character> plainToCode = new HashMap<String, Character>();
    Map<Character, String> codeToPlain = new HashMap<Character, String>();

    static Pattern wordPattern = Pattern.compile("[\\w\\u8000-\\uCFFE]+");
    static Pattern urlPattern = Pattern.compile("(?x) ^(https?):// ([^/:]+) (?::(\\\\d+))?");

    static Pattern codePattern = Pattern.compile("[\\u8000-\\uCFFE]");

    boolean encoderError() { return (nextCode > '\uCFFE'); }

    String encodeWords(String text)
    {
        StringBuffer sbuf = new StringBuffer(text.length());

        Matcher m = wordPattern.matcher(text);
        while (m.find()) {
            String word = m.group();

            Character code = plainToCode.get(word);

            if (code == null) {
                code = (char) ++nextCode;
                plainToCode.put(word, code);
                codeToPlain.put(code, word);
            }

            m.appendReplacement(sbuf, String.valueOf(code.charValue()));
        }

        m.appendTail(sbuf);

        return sbuf.toString();
    }

    String decodeWords(String text)
    {
        StringBuffer sbuf = new StringBuffer(text.length());

        Matcher m = codePattern.matcher(text);
        while (m.find()) {
            m.appendReplacement(sbuf, codeToPlain.get(m.group().charAt(0)));
        }
        m.appendTail(sbuf);

        return sbuf.toString();
    }
}
