import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import groovy.xml.MarkupBuilder
import org.cyberneko.html.parsers.SAXParser
import java.util.regex.Matcher
import groovy.io.FileType
import java.text.SimpleDateFormat

extDir  = new File(".")
dataDir = new File(extDir, 'data')
diffDir = new File(extDir, 'cache')
htmlDir = new File(extDir, 'html')

newDiffsDir = new File(extDir, 'west_cache')

revisionsDir = new File(dataDir, 'article-revisions')

File getRevisionFile(rev)
{
   rev = rev + '.txt'
   File found = null
   revisionsDir.eachFileRecurse(FileType.FILES) { File f ->
     if (f.name == rev) found = f
   }
   if (!found) {
      println "Can't find revision file ${rev}."
   }
   found
}

String relativePath(File base, File target)
{
//    URI baseURI = base.parentFile.toURI()
//    URI relURI = baseURI.relativize(target.toURI())
//    return relURI.toASCIIString()

   // No need for canonicalization because we constructed them relatively.
   base = base.parentFile
   String t = target.toURI().toASCIIString()

   String prefix = ""

   while (base != null ) {
      String p = base.toURI().toASCIIString()

      if (t.startsWith(p)) {
         return prefix + t.substring(p.length())
      }

      prefix += "../"
      base = base.parentFile
   }

   target.toURI().toASCIIString()
}

String mungeFieldName(name) { name == 'class' ? 'type' : name }

List readCSVtoList(String fileName) {
  reader = new CSVReader(new FileReader(new File(dataDir, fileName)))
  String[] fieldNames = reader.readNext()
  List data = reader.readAll()
  data.collect { String[] row ->
    Map fields = [:]
    fieldNames.eachWithIndex { name, x -> fields[mungeFieldName(name)] = row[x] }
    new Expando(fields)
  }
}

Map readCSVtoMap(String fileName) {
  reader = new CSVReader(new FileReader(new File(dataDir, fileName)))
  String[] fieldNames = reader.readNext()
  List data = reader.readAll()
  Map indexedData = [:]
  data.each { String[] row ->
     Map fields = [:]
     fieldNames.eachWithIndex { name, x -> fields[mungeFieldName(name)] = row[x] }
     indexedData[row[0]] = new Expando(fields)
  }
  indexedData
}


assert (('http://en.wikipedia.org/w/index.php?title=Hellenistic_Greece&amp;oldid=255378201' =~ /oldid=(\d+)/)[0][1] == '255378201')

editid = 100000

newEdits = new CSVWriter(new BufferedWriter(new FileWriter(new File(dataDir, "west_edits.csv"))))
newEdits.writeNext(["editid","editor","oldrevisionid","newrevisionid","diffurl","edittime","editcomment","articletitle"] as String[])

wikipediaDateFormat = new SimpleDateFormat('HH:mm, dd MMMM yyyy')
utcDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")

def getEditInfo(diffHtml, forOldVersion)
{
  def revDiv = diffHtml.'**'.findAll { it.'@id' == (forOldVersion ? 'mw-diff-otitle1' : 'mw-diff-ntitle1') }
  // First anchor has url with oldid and text with timestamp
  def revAnchor = revDiv.strong[0].a[0]
  def revId = (revAnchor.'@href' =~ /oldid=(\d+)/)[0][1]
  def revTime1 = (revAnchor.text() =~ /^Revision as of (.*)$/)[0][1]

  def revDate = wikipediaDateFormat.parse(revTime1)

  def revTime2 = utcDateFormat.format(revDate)

  def userDiv = diffHtml.'**'.findAll { it.'@id' == (forOldVersion ? 'mw-diff-otitle2' : 'mw-diff-ntitle2') }
  def userId = userDiv.a[0].text()

  def comment = ''

  def commentDiv = diffHtml.'**'.findAll { it.'@id' == (forOldVersion ? 'mw-diff-otitle3' : 'mw-diff-ntitle3') }

  if (commentDiv) {
     def commentSpan = commentDiv[0].span[0]


    if (commentSpan) {
      comment = new StringBuilder()

      commentSpan.children().each {
        if (it instanceof String) {
          comment.append(it)
        } else {
          assert (it instanceof Node)
          if ('A'.equalsIgnoreCase(it.name())) {
            comment.append('[[')

            def u = it.'@href'
            if (u.startsWith('http://en.wikipedia.org/wiki/')) {
              u = u.substring('http://en.wikipedia.org/wiki/'.length())
            }
            comment.append(u)

            def t = it.text()
            if (t) {
              comment.append('|')
              comment.append(t)
            }
            comment.append(']]')
          } else if (it.'@class' == 'autocomment') {
            comment.append('/* ')
            comment.append(it.text())
            comment.append(' */')
          } else {
            comment.append(it.text())
          }
        }
      }
      comment = comment.toString()

      if (comment.startsWith('(')) comment = comment.substring(1)
      if (comment.endsWith(')')) comment = comment.substring(0, comment.length() - 1)

      comment = comment.trim()
    }

//    println comment
  }

  [revisionid:revId, edittime:revTime2, editor:userId, editcomment:comment]
}

def makeDiffURL(oldid, newid) { "http://en.wikipedia.org/w/index.php?diff=${newid}&oldid=${oldid}" }

def processDiffFile(File diffFile)
{
  XmlParser parser = new XmlParser(false, false)
  parser.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false)
  parser.setTrimWhitespace(false)
  def diffHtml = parser.parse(diffFile.newReader())
  
  def diffTable = diffHtml.'**'.findAll { it.'@class' == 'diff' }

  if (diffTable.size() != 1) {
     println "Didn't find (exactly one) <table class='diff'> diffFile ${diffFile.path}"
     return
  }

  def title = diffHtml.'**'.find { it.'@class' == 'firstHeading' }

  title = title.text()

//  println title

  def oldInfo = getEditInfo(diffHtml, true)

//  println oldInfo

  def newInfo = getEditInfo(diffHtml, false)

//  println newInfo

  newEdits.writeNext([++editid as String, newInfo.editor, oldInfo.revisionid, newInfo.revisionid
          , makeDiffURL(oldInfo.revisionid, newInfo.revisionid)
          , newInfo.edittime, newInfo.editcomment
          , title] as String[])
}

newDiffsDir.eachFile (FileType.FILES) {
   processDiffFile(it)
}

newEdits.close()
