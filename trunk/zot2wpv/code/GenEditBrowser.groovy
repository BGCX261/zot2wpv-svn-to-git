import au.com.bytecode.opencsv.CSVReader;
import groovy.xml.MarkupBuilder

extDir  = new File(".")
dataDir = new File(extDir, 'data')
diffDir = new File(extDir, 'cache')
htmlDir = new File(extDir, 'html')

viewDir = new File(htmlDir, 'edits')
viewDir.mkdirs()

File getViewFile(edit) { new File(viewDir, edit.editid + '.html') }

String relativePath(File base, File target)
{
//    URI baseURI = base.parentFile.toURI()
//    URI relURI = baseURI.relativize(target.toURI())
//    return relURI.toASCIIString()
  
   // No need for canonicalization because we constructed them relatively.
   base = base.parentFile
   String t = target.toURI().toASCIIString()

   String prefix = ""

   while (base != null ) {
      String p = base.toURI().toASCIIString()

      if (t.startsWith(p)) {
         return prefix + t.substring(p.length())
      }

      prefix += "../"
      base = base.parentFile
   }

   target.toURI().toASCIIString()
}

assert(relativePath(new File(htmlDir, 'xxx.html'), new File(viewDir, 'yyy.html')) == "edits/yyy.html")
assert(relativePath(new File(diffDir, 'xxx.html'), new File(viewDir, 'yyy.html')) == "../html/edits/yyy.html")

String mungeFieldName(String name) { name == 'class' ? 'type' : name }

Map readCSVtoMap(String fileName) {
  reader = new CSVReader(new FileReader(new File(dataDir, fileName)))
  String[] fieldNames = reader.readNext()
  List data = reader.readAll()
  Map indexedData = [:]
  data.each { String[] row ->
     Map fields = [:]
     fieldNames.eachWithIndex { name, x -> fields[mungeFieldName(name)] = row[x] }
     indexedData[row[0]] = new Expando(fields)
  }
  indexedData
}

List readCSVtoList(String fileName) {
  reader = new CSVReader(new FileReader(new File(dataDir, fileName)))
  String[] fieldNames = reader.readNext()
  List data = reader.readAll()
  data.collect { String[] row ->
    Map fields = [:]
    fieldNames.eachWithIndex { name, x -> fields[mungeFieldName(name)] = row[x] }
    new Expando(fields)
  }
}

edits = readCSVtoMap('edits.csv')
gold = readCSVtoMap('gold-annotations.csv')
annotators = readCSVtoMap('annotators.csv')
annotations = readCSVtoList('annotations.csv')

println "${edits.size()} edits."
println "${gold.size()} gold annotations."
println "${annotators.size()} annotators."
println "${annotations.size()} annotations."

println gold["3"]
println (gold["3"].type)
println (gold["3"].totalannotators)
println (gold["3"].type == 'regular')

vandalism = edits.values().findAll{ gold[it.editid].type == 'vandalism' }

println vandalism.size()

annotations.each { annotation ->
   def annotator = annotators[annotation.annotatorid]
   if (!annotator) {
      println( "No annotator for id ${annotation.annotatorid}")
      annotator = new Expando(annotatorid:annotation.annotatorid)
      annotators[annotation.annotatorid] = annotator
   }
   annotator.decisions = (annotator.decisions ?: 0) + 1
   annotator.decisionTime = (annotator.decisionTime ?: 0) + Integer.valueOf(annotation.decisiontime)
}

annotators.values().each {
   if (it.decisions) it.decisionTime /= it.decisions
}

annotators

annotations.each { annotation ->
   def edit = edits[annotation.editid]
   if (!edit) {
      println( "No edit for id ${annotation.editid}")
   } else {
      def annotator = annotators[annotation.annotatorid]
      Float confidence = Math.exp(1-(Integer.valueOf(annotation.decisiontime)/annotator.decisionTime))
      edit.decisions = (edit.decisions ?: 0) + 1
      edit.decisionTime = (edit.decisionTime ?: 0) + Integer.valueOf(annotation.decisiontime)
      edit.confidence = (edit.confidence ?: 0) + confidence
   }
}

edits.values().each {
    if (it.decisions) {
        it.decisionTime /= it.decisions
        it.confidence /= it.decisions
    }
}

String makeIndexFileName(type, sort)
{
   "${type}-${sort}.html"
}

def genIndexFile(type, sort)
{
  File indexFile = new File(htmlDir, makeIndexFileName(type, sort))

  html = new MarkupBuilder(new FileWriter(indexFile))

  html.html {
     body {
        div { a(href:makeIndexFileName('vandalism', 'id'), target:'edits') { span ("Vandalism (by id)") } }
        div { a(href:makeIndexFileName('regular', 'id'), target:'edits') { span ("Regular (by id)") } }
        div { a(href:makeIndexFileName('vandalism', 'confidence'), target:'edits') { span ("Vandalism (by Confidence)") } }
        div { a(href:makeIndexFileName('regular', 'confidence'), target:'edits') { span ("Regular (by Confidence)") } }
        hr()
        def editList = edits.values().findAll{ gold[it.editid].type == type }
        switch (sort) {
           case 'confidence' :
             editList = editList.sort { -it.confidence }
             break
           case 'id' :
             editList = editList.sort { Integer.valueOf(it.editid) }
             break
        }
        editList.each { edit ->
           div {
              a(href:relativePath(indexFile, getViewFile(edit)), target:'editview') { span ( edit.editid ) }
           }
        }
     }
  }
}

genIndexFile('vandalism', "confidence")
genIndexFile('vandalism', "id")
genIndexFile('regular', "confidence")
genIndexFile('regular', "id")
