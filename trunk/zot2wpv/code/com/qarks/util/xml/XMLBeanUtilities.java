/**
 *  Copyright 2009 QArks.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 **/

package com.qarks.util.xml;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.*;

public class XMLBeanUtilities{

    private XMLBeanUtilities(){}

    public static Object bytesToObject(byte bytes[]) throws IOException{
        Object result = null;
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
        XMLDecoder decoder = new XMLDecoder(bais);
        result = decoder.readObject();
        return result;
    }

    public static byte[] objectToBytes(Object object) throws IOException{
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        XMLEncoder encoder = new XMLEncoder(baos);
        encoder.writeObject(object);
        encoder.flush();
        encoder.close();
        byte[] result=baos.toByteArray();
        return result;
    }
}
