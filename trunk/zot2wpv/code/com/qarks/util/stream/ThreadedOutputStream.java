/**
 *  Copyright 2009 QArks.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 **/

package com.qarks.util.stream;

import java.io.*;
import com.qarks.util.*;

public class ThreadedOutputStream extends CorePipedOutputStream implements Runnable {
    private byte mArray[];

    private Thread mThread = null;

    private OutputStream mOs;

    public ThreadedOutputStream(OutputStream source, int bufferSize) {
        super(2 * bufferSize);
        mOs = source;
        mPis = new CorePipedInputStream();
        mArray = new byte[bufferSize];
        try {
            mPis.connect(this);
            mThread = new Thread(this);
            mThread.start();
        }
        catch (IOException e) {
            Log.logError(this, "cannot connect pipes:", e);
        }
    }

    public void close() throws IOException {
        super.close();
        try {
            mThread.join();
        }
        catch (InterruptedException e) {
            Log.logError(this, "join interrupted", e);
        }
    }

    public void run() {
        int nbRead;
        try {
            while ((nbRead = mPis.read(mArray)) >= 0) {
              if (nbRead>0){
                mOs.write(mArray, 0, nbRead);
              }
              else{
                Thread.sleep(50);
              }
            }
            mOs.close();
            mPis.close();
        }
        catch (Exception e) {
            Log.logError(this, "cannot write.", e);
            e.printStackTrace();
        }
        finally {
            try {
                mOs.close();
                mPis.close();
            }
            catch (IOException e) {}
        }
    }
}
