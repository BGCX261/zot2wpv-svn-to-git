/**
 *  Copyright 2009 QArks.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 **/

package com.qarks.util.stream;

import java.io.*;
import com.qarks.util.Synchronizer;
import com.qarks.util.Log;


public class CorePipedOutputStream extends OutputStream {
    private byte mBuffer[] = null;

    private int mSize = 0;

    private int mWriteOffset = 0;

    private int mReadOffset = 0;

    private int mAvailable = 0;

    private boolean mClosed = false;

    private Object mSync = new Object();

    private static int PIPE_SIZE = 1024;

    boolean mConnected = false;

    private Synchronizer mNotificationWrite = new Synchronizer();

    private Synchronizer mNotificationRead = new Synchronizer();

    private Synchronizer mNotificationEnd = new Synchronizer();

    CorePipedInputStream mPis = null;

    public CorePipedOutputStream(int bufferSize) {
        mSize = bufferSize;
        mBuffer = new byte[bufferSize];
    }

    public CorePipedOutputStream() {
        this(PIPE_SIZE);
    }

    public int getBufferSize(){
        return mBuffer.length;
    }

    public CorePipedOutputStream(CorePipedInputStream pis, int bufferSize) throws IOException {
        this(bufferSize);
        connect(pis);
    }

    public CorePipedOutputStream(CorePipedInputStream pis) throws IOException {
        this(pis, PIPE_SIZE);
    }

    public boolean canBeReused() {
        return ((mConnected == true) &&
                (mPis.isClosed()) && (mClosed));
    }

    public void reset() {
        if (canBeReused() == false)
            Log.logWarning(this, "Reset OutputPipe with inputStream not closed");
        mReadOffset = 0;
        mWriteOffset = 0;
        mAvailable = 0;
        mClosed = false;
        mPis.open();
        mNotificationWrite.reset();
        mNotificationRead.reset();
        mNotificationEnd.reset();
    }

    public void connect(CorePipedInputStream pis) throws IOException {
        mPis = pis;
        mPis.mPos = this;
        mPis.mConnected = true;
        mConnected = true;
    }

    public void write(int b) throws IOException {
    	write(new byte[]{(byte)(b & 255)});
        //write(new byte[] {(byte)(b << 8), (byte)((b & 0xF0) << 8)}, 0, 2);
    }

    public void write(byte b[]) throws IOException {
        write(b, 0, b.length);
    }

    public synchronized void write(byte b[], int off, int len) throws IOException {
        int copied = 0;
        int firstPart = 0;
        int secondPartCopied = 0;
        int firstPartCopied = 0;

        try {
            if (mConnected == false)
                throw (new IOException("OutputPipe not connected"));

            if (mClosed == true)
                throw (new IOException("OutputPipe closed"));

            while (len > 0) {
                synchronized (mSync) {
                    if (mAvailable < mSize) {
                        // let's copy as much as we can util the end of the buffer
                        // the current offset (mOffset) should never be positionned
                        // at the end of the buffer when arriving here !!!
                        firstPart = Math.min(mSize - mWriteOffset, len);
                        firstPartCopied = Math.min(firstPart, mSize - mAvailable);
                        if (firstPartCopied > 0) {
                            System.arraycopy(b, off, mBuffer, mWriteOffset, firstPartCopied);
                            mAvailable += firstPartCopied;
                            mWriteOffset += firstPartCopied;
                            off += firstPartCopied;
                            if (mWriteOffset == mSize)
                                mWriteOffset = 0;
                            len -= firstPartCopied;
                            copied += firstPartCopied;
                            if ((len > 0) && ((mSize - mAvailable) > 0)) {
                                if (mWriteOffset != 0)
                                    throw (new IOException("offset should not be != 0"));
                                // we have reached the end of the buffer (rotating buffer) but
                                // some datas are left and some place is left at the beginning of the buffer
                                secondPartCopied = Math.min(mSize - mAvailable, len);
                                System.arraycopy(b, off, mBuffer, 0, secondPartCopied);
                                mAvailable += secondPartCopied;
                                mWriteOffset = secondPartCopied;
                                off += secondPartCopied;
                                len -= secondPartCopied;
                                copied += secondPartCopied;
                            }
                        }
                    }
                    if (mAvailable > 0)
                        mNotificationRead.notifyEvent();
                }

                // we have copied as much as possible, so now
                // if some datas are still left to copy, we have to wait until
                // some datas are read
                if (len > 0) {
                    if (mClosed == true)
                        throw (new IOException("OutputPipe closed before waiting for available space"));

                    mNotificationWrite.waitEvents();
                }
            }
        }
        catch (Throwable t) {
            Log.logError(this, "Exception while writing.", t);
            if (t instanceof IOException)
                throw (IOException)t;
        }
    }

    public void flush() throws IOException {

    }

    public boolean isClosed() {
        return mClosed;
    }

    public void close() throws IOException {
        mClosed = true;
        // if the writer was locked because no more space
        // was available, we have to unlock him
        mNotificationWrite.notifyEvent();
        if (mAvailable == 0) {
            // the reader may be locked in reading since there
            // was nothing to read, let's unlock him
            mNotificationRead.notifyEvent();
        }
    }

    int receive(byte b[], int off, int len) throws IOException {
        int result = 0;
        int firstPart = 0;
        int secondPartCopied = 0;
        int firstPartCopied = 0;

        len = Math.min(len, b.length);

        try {
            while (available() <= 0) {
                if (isClosed() == true)
                    return -1;
                else
                    mNotificationRead.waitEvents();
            }

            synchronized (mSync) {
                firstPart = Math.min(mSize - mReadOffset, mAvailable);
                firstPartCopied = Math.min(firstPart, len);
                System.arraycopy(mBuffer, mReadOffset, b, off, firstPartCopied);
                mAvailable -= firstPartCopied;
                mReadOffset += firstPartCopied;
                off += firstPartCopied;
                len -= firstPartCopied;
                result += firstPartCopied;
                if (mReadOffset == mSize)
                    mReadOffset = 0;
                if ((len > 0) && (mAvailable > 0)) {
                    // we have reached the end of the rotating buffer, but
                    // some datas are left to be read at the beginning and
                    // some place is left in the client buffer
                    secondPartCopied = Math.min(mAvailable, len);
                    System.arraycopy(mBuffer, 0, b, off, secondPartCopied);
                    mAvailable -= secondPartCopied;
                    mReadOffset = secondPartCopied;
                    off += secondPartCopied;
                    len -= secondPartCopied;
                    result += secondPartCopied;
                }
            }
            if (result > 0) {
                mNotificationWrite.notifyEvent();
            }
        }
        catch (Throwable t) {
            Log.logError(this, "Exception while receiving.", t);
            if (t instanceof IOException)
                throw (IOException)t;
        }

        if (result == 0)
            Log.logWarning(this, "receive should never return 0");

        return result;
    }

    int available() {
        int result = mAvailable;
        if ((result == 0) && (mClosed == true))
            result = -1;
        return result;
    }
}
