/**
 *  Copyright 2009 QArks.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 **/

package com.qarks.util.stream;

import java.io.*;


public class ResourceHelper {

  private static ResourceHelper instance = null;

  public static synchronized ResourceHelper getInstance(){
    if (instance==null){
      instance = new ResourceHelper();
    }
    return instance;
  }

  public void extractResourceIfNotExisting(String resource, String destinationFile){
    File file = new File(destinationFile);
    if (!file.exists() || file.length()==0){
      extractResource(resource, destinationFile);
    }
  }

  public void extractResource(String resource, String destinationFile) {
    File file = new File(destinationFile);
    try {
      InputStream is = getClass().getClassLoader().getResourceAsStream(resource);
      byte array[] = new byte[2048];
      int nbread = 0;
      FileOutputStream fos = new FileOutputStream(file);

      while ( (nbread = is.read(array)) > -1) {
        if (nbread > 0) {
          fos.write(array, 0, nbread);
        }
        else {
          Thread.sleep(50);
        }
      }
      fos.close();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}
