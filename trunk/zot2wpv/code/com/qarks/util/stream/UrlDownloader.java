/**
 *  Copyright 2009 QArks.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 **/

package com.qarks.util.stream;

import java.io.*;
import java.net.*;


public class UrlDownloader {

/*
  public static void main(String args[]){
    try{
      File file = downloadFromUrl("http://cache.yacast.fr/V4/rmc/rmc.m3u","m3u");
      System.out.println(file.getAbsolutePath());
    }
    catch(Exception ex){
      ex.printStackTrace();
    }
  }*/

  public static File downloadFromUrl(String urlString,String ext) throws Exception{
    URL url = new URL(urlString);
    InputStream is = url.openStream();
    byte bytes[] = new byte[20480];
    File result = File.createTempFile("temp_",ext);
    FileOutputStream fos = new FileOutputStream(result);
    BufferedOutputStream bos = new BufferedOutputStream(fos);
    int nbread = 0;
    int loops = 0;
    while((nbread = is.read(bytes))>-1){
      bos.write(bytes,0,nbread);
      loops++;
      System.out.println(loops);
    }
    bos.close();
    return result;
  }
}
