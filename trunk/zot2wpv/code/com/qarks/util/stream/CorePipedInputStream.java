/**
 *  Copyright 2009 QArks.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 **/

package com.qarks.util.stream;

import java.io.*;

public class CorePipedInputStream extends InputStream {
    private boolean mClosed = false;
    boolean mConnected = false;
    CorePipedOutputStream mPos = null;
    private static byte[] skipBuffer;
    private static final int SKIP_BUFFER_SIZE = 2048;
    private byte[] b = new byte[1];

    public CorePipedInputStream(CorePipedOutputStream pos) throws IOException {
        connect(pos);
    }

    public CorePipedInputStream() {
    }

    public void connect(CorePipedOutputStream pos) throws IOException {
        mPos=pos;
        mConnected=true;
        mPos.mPis=this;
        mPos.mConnected=true;
    }

    public int read() throws IOException {
        int nbread = read(b);
        if (nbread>=0){
        	return b[0];
        }
        else{
        	return -1;
        }
    }

    public int read(byte b[]) throws IOException {
        return read(b,0,b.length);
    }

    public long skip(long n) throws IOException {
        long remaining = n;
        int nr;
        if (skipBuffer == null)
            skipBuffer = new byte[SKIP_BUFFER_SIZE];

        byte[] localSkipBuffer = skipBuffer;

        if (n <= 0) {
            return 0;
        }

        while (remaining > 0) {
            nr = read(localSkipBuffer, 0,
                      (int)Math.min(SKIP_BUFFER_SIZE, remaining));
            if (nr < 0) {
                break;
            }
            remaining -= nr;
        }

        return n - remaining;
    }

    public synchronized int read(byte b[], int off, int len) throws IOException {
        if (mConnected==false)
            throw(new IOException("InputPipe not connected"));
        if (mClosed==true)
            throw(new IOException("InputPipe closed"));
        return mPos.receive(b,off,len);
    }

    public synchronized int available() throws IOException {
        if (mConnected==false)
            throw(new IOException("InputPipe not connected"));
        int available = (mPos.available());
        /*
        if (available==0){
        System.out.println(available);
        }*/
        return available;
    }

    public void close() throws IOException {
        mClosed=true;
        if (!mPos.isClosed()) {
            mPos.close();
        }
    }

    void open() {
        mClosed=false;
    }

    public boolean isClosed() {
        return mClosed;
    }
}
