/**
 *  Copyright 2009 QArks.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 **/

package com.qarks.util.stream;

import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipperInputStream extends InputStream{

	private boolean eof = false;
	private InputStream streamToZip;
	private CorePipedInputStream pipeIn;
	private CorePipedOutputStream pipeOut;
	private ZipOutputStream zos;
	private byte array[];
	
	/*
	public static void main(String args[]){
		try{
			String test = "ceci est un test.";
			StringBuffer buf = new StringBuffer();
			for(int i=0;i<100;i++){
				buf.append(test);
			}
			String toZip = buf.toString();
			ByteArrayInputStream bais = new ByteArrayInputStream(toZip.getBytes());
			InputStream zis = new ZipperInputStream(bais);
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			byte array[] = new byte[2048];
			int nbread = 0;
			while((nbread=zis.read(array))>-1){
				baos.write(array,0,nbread);
			}
			byte[] compressed = baos.toByteArray();
			bais = new ByteArrayInputStream(compressed);
			ZipInputStream zip = new ZipInputStream(bais);
			ZipEntry entry = zip.getNextEntry();

			baos = new ByteArrayOutputStream();
			while((nbread=zip.read(array))>-1){
				baos.write(array,0,nbread);
			}
			byte[] uncompressed = baos.toByteArray();
			String uncompressedString = new String(uncompressed);
			if (!uncompressedString.equals(toZip)){
				System.err.println("not equals: "+uncompressedString);
			}
		}
		catch(Throwable e){
			e.printStackTrace();
		}
	}*/
	
	public ZipperInputStream(InputStream streamToZip) throws IOException{
		this.streamToZip = streamToZip;
		pipeIn = new CorePipedInputStream();
		pipeOut = new CorePipedOutputStream(pipeIn);
		zos = new ZipOutputStream(pipeOut);
		zos.putNextEntry(new ZipEntry("content"));
		// buffer size must be smaller than pipe size, otherwise
		// writing a new chunk to the pipe to let read() return
		// some data will block, and we will have a deadlock
		array = new byte[1024];
	}
	
	
	public int read() throws IOException {
		int result;
		while (pipeIn.available()==0 && !eof){
			int nbread = streamToZip.read(array);
			if (nbread==-1){
				eof=true;
				zos.closeEntry();
			}
			else{				
				zos.write(array,0,nbread);
			}
		}		
		if (eof && pipeIn.available()==0){
			result = -1;
		}
		else{
			result = pipeIn.read();
		}
		return result;
	}
	
	

}
