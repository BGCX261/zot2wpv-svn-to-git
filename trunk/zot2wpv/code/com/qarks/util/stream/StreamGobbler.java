/**
 *  Copyright 2009 QArks.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 **/

package com.qarks.util.stream;

import java.io.InputStream;
import java.io.*;


public class StreamGobbler extends Thread{

  private InputStream is;

  public StreamGobbler(InputStream is) {
    this.is = is;
  }

  public static void startGobbling(Process proc){
    StreamGobbler out = new StreamGobbler(proc.getInputStream());
    StreamGobbler err = new StreamGobbler(proc.getErrorStream());
    out.start();
    err.start();
  }

  public static StreamGobbler startStreamGobbler(InputStream is){
    StreamGobbler result = new StreamGobbler(is);
    result.start();
    return result;
  }

  public void run(){
    byte array[] = new byte[512];
    int nbread=0;

    while(nbread>-1){
      try {
        nbread = is.read(array);
        if (nbread>0){
          System.out.println(new String(array, 0, nbread));
        }
      }
      catch (IOException ex) {
        ex.printStackTrace();
        nbread = -1;
      }
    }
  }
}
