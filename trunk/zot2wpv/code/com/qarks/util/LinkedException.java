/**
 *  Copyright 2009 QArks.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 **/

package com.qarks.util;

public class LinkedException extends Exception {

    private Exception mLinkedException = null;


    public LinkedException() {
        mLinkedException = null;
    }

    public LinkedException(Exception linkedException) {
        mLinkedException = linkedException;
    }

    public LinkedException(String message) {

        super(message);

        mLinkedException = null;
    }

    public LinkedException(String message, Exception linkedException) {

        super(message);

        mLinkedException = linkedException;
    }

    public Exception getLinkedException() {
        return mLinkedException;
    }
}
