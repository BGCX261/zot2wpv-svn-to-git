/* 
 * Distributed as part of jdiff
 * 
 * Copyright (C) 2008 QArks.com
 *
 * Author: Pierre Meyer <support@qarks.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 2.1, as 
 * published by the Free Software Foundation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software; see the file LICENSE.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package com.qarks.util.files.diff;

import java.util.ArrayList;
import java.util.List;

import com.qarks.util.files.diff.FileLine.EolType;

public class ParsedFile {

	private FileLine lines[];

	public ParsedFile(String fileContent) {
		List<FileLine> lineArray = new ArrayList<FileLine>();
		char chars[] = fileContent.toCharArray();
		StringBuffer buf = new StringBuffer();
		for(int i=0;i<chars.length;i++){
			if (chars[i]=='\r' && (i==(chars.length-1) || chars[i+1]!='\n')){
				lineArray.add(new FileLine(buf.toString(),lineArray.size(),EolType.CR));
				buf = new StringBuffer();
			}
			if (chars[i]=='\n'){
				lineArray.add(new FileLine(buf.toString(),lineArray.size(),EolType.LF));
				buf = new StringBuffer();
			}
			else if (chars[i]=='\r' && i<chars.length-1 && chars[i+1]=='\n'){
				lineArray.add(new FileLine(buf.toString(),lineArray.size(),EolType.CRLF));
				buf = new StringBuffer();
				i++;
			}
			else{
				buf.append(chars[i]);
			}
		}
		String str = buf.toString();
		if (str.length()>0){
			lineArray.add(new FileLine(str,lineArray.size(),EolType.NO_EOL));
		}
		lines = lineArray.toArray(new FileLine[0]);

		/*
		String[] fileLines = fileContent.split("\r\n|\n|\r");
		boolean endsWithNewLine = (fileContent.endsWith("\r\n") || 
				fileContent.endsWith("\n") || 
				fileContent.endsWith("\r"));
		lines = new FileLine[fileLines.length+(endsWithNewLine?1:0)];
		for(int i=0;i<fileLines.length;i++){
			lines[i] = new FileLine(fileLines[i],i);
		}
		if (endsWithNewLine){
			lines[lines.length-1] = new FileLine("",lines.length-1);
		}*/
		
	}

	public ParsedFile(ArrayList lineArray){
		lines = new FileLine[lineArray.size()];
		for(int i=0;i<lineArray.size();i++){
			lines[i] = (FileLine)lineArray.get(i);
		}
	}

	public ParsedFile(FileLine lines[]){
		this.lines = lines;
	}

	public FileLine[] getLines(){
		return lines;
	}
}
