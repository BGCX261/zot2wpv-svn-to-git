/* 
 * Distributed as part of jdiff
 * 
 * Copyright (C) 2008 QArks.com
 *
 * Author: Pierre Meyer <support@qarks.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 2.1, as 
 * published by the Free Software Foundation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software; see the file LICENSE.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package com.qarks.util.files.diff;

public class FileLine {

	public static final int UNKNOWN = 0;
	public static final int UNCHANGED = 1;
	public static final int MOVED = 2;
	public static final int MODIFIED = 3;
	public static final int INSERTED_ON_OTHER_SIDE = 4;
	public static final int DELETED_ON_OTHER_SIDE = 5;
	public static final int CONFLICT = 6;
	public static final int NO_MATCH = -1;	
	public enum EolType{IGNORE,NO_EOL,CR,LF,CRLF};

	private EolType eolType;
	private String line;
	private int indexInOtherVersion = -1;
	private boolean matchFound = false;
	private int index;
	private int status = NO_MATCH;
	private int hashcode = -1;

	public FileLine(String line, int index, EolType eolType) {
		this.line = line;
		this.index = index;
		this.eolType = eolType;
	}
	
	public static FileLine getFormattingLine(int status){
		FileLine result =  new FileLine("",-1,EolType.IGNORE);
		result.status = status;
		return result;
	}
	
	public EolType getEolType(){
		return eolType;
	}
/*
	public FileLine(String line, int index, int status) {
		this.line = line;
		this.index = index;
		this.status = status;
	}
*/
	public static String statusToString(int status){
		String result = "?";
		switch(status){
		case FileLine.CONFLICT:
			result = "conflict";
			break;
		case FileLine.DELETED_ON_OTHER_SIDE:
			result = "deleted on other side";
			break;
		case FileLine.INSERTED_ON_OTHER_SIDE:
			result = "inserted on other side";
			break;
		case FileLine.MODIFIED:
			result = "modified";
			break;
		case FileLine.MOVED:
			result = "moved";
			break;
		case FileLine.NO_MATCH:
			result = "no match";
			break;
		case FileLine.UNCHANGED:
			result = "unchanged";
			break;
		}
		return result;
	}

	public String getContent(){
		return line;
	}

	public int getIndex(){
		return index;
	}

	public boolean isMatchFound(){
		return matchFound;
	}

	public int indexInOtherVersion(){
		return indexInOtherVersion;
	}

	public int getStatus(){
		return status;
	}

	public void setIndexInOtherVersion(int indexInOtherVersion){
		matchFound = true;
		this.indexInOtherVersion = indexInOtherVersion;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public String toString(){
		// for copy paste
		return line;
	}

	public int getHashCode(){
		if (hashcode==-1){
			hashcode = line.hashCode();
		}
		return hashcode;
	}

	public boolean matches(FileLine other,boolean ignoreLeadingSpaces){
		boolean result = false;
		if (ignoreLeadingSpaces){
			String first = line;
			while(first.startsWith(" ") || first.startsWith("\t")){
				first = first.substring(1);
			}
			String second = other.getContent();
			while(second.startsWith(" ") || second.startsWith("\t")){
				second = second.substring(1);
			}
			result = (first.length()==second.length() && 
					first.hashCode()==second.hashCode() &&
					other.eolType==eolType);
		}
		else if (line.length()==other.getContent().length() && 
				getHashCode()==other.getHashCode() &&
				other.eolType==eolType){
			result = true;
		}
		return result;
	}
}
