/* 
 * Distributed as part of jdiff
 * 
 * Copyright (C) 2008 QArks.com
 *
 * Author: Pierre Meyer <support@qarks.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 2.1, as 
 * published by the Free Software Foundation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software; see the file LICENSE.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package com.qarks.util.files.diff;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DirContentStatus {
	
	public static enum Status{NEW,MODIFIED,EQUAL};
	private File file;
	private File otherFile;
	private Status status;
	private List<DirContentStatus> children;
	
	public DirContentStatus(File file, Status status){
		this(file);
		this.status = status;
	}
	
	public String toString(){
		return file.getAbsolutePath();
	}
	
	public DirContentStatus(File file){
		this.file = file;
		this.status = Status.EQUAL;
		children = new ArrayList<DirContentStatus>();
	}

	public File getFile(){
		return file;
	}
	
	public List<DirContentStatus> getChildren(){
		return children;
	}
	
	// this method only makes sense for modified files, so we
	// do not have to find in the other folder tree the corresponding
	// file to compute a file diff with both
	public File getOtherFile(){
		return otherFile;
	}
	
	public Status getStatus(){
		return status;
	}	
	
	public void setStatus(Status status){
		this.status = status;
	}
	
	public DirContentStatus addFileEqual(File file){
		DirContentStatus content = new DirContentStatus(file, Status.EQUAL);
		children.add(content);	
		return content;
	}
	
	public DirContentStatus addFileModified(File file, File otherFile){
		DirContentStatus content = new DirContentStatus(file, Status.MODIFIED);
		content.otherFile = otherFile;
		children.add(content);
		return content;
	}
	
	public DirContentStatus addFileNew(File file){
		DirContentStatus content = new DirContentStatus(file, Status.NEW);
		children.add(content);
		return content;
	}
	
	public void addAll(DirContentStatus externalContentStatus){
		children.addAll(externalContentStatus.children);		
	}
	
	public void sort(){
		FileComparator comparator = new FileComparator();
		Collections.sort(children, comparator);
	}
	
	//////////////////////////////////
	
	private class FileComparator implements Comparator{

		public int compare(Object arg0, Object arg1) {
			int result = 0;
			DirContentStatus status1 = (DirContentStatus)arg0;
			DirContentStatus status2 = (DirContentStatus)arg1;
			if (status1.file.isDirectory()!=status2.file.isDirectory()){
				if (status1.file.isDirectory()){
					result = -1;
				}
				else{
					result = 1;
				}
			}			
			else{
				result = status1.file.getName().compareTo(status2.file.getName());
			}
			return result;
		}
		
	}

}
