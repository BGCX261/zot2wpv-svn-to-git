/* 
 * Distributed as part of jdiff
 * 
 * Copyright (C) 2008 QArks.com
 *
 * Author: Pierre Meyer <support@qarks.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 2.1, as 
 * published by the Free Software Foundation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software; see the file LICENSE.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package com.qarks.util.files.diff; 

import java.util.ArrayList; 

public class MergeResult { 

   private ArrayList<MergeResultItem> mergeItems; 
   private ParsedFile leftFile = null; 
   private ParsedFile rightFile = null; 
   private String eol = System.getProperty("line.separator"); 

   public MergeResult(ArrayList<MergeResultItem> mergeItems) { 
      this.mergeItems = mergeItems; 
   } 

   public ParsedFile getLeftFile(){ 
      if (leftFile==null){ 
         ArrayList<FileLine> lines = new ArrayList<FileLine> (); 
         for (int i = 0; i < mergeItems.size(); i++) { 
            lines.addAll(mergeItems.get(i).getLeftVersion()); 
         } 
         leftFile = new ParsedFile(lines); 
      } 
      return leftFile; 
   } 

   public ParsedFile getRightFile(){ 
      if (rightFile==null){ 
         ArrayList<FileLine> lines = new ArrayList<FileLine> (); 
         for (int i = 0; i < mergeItems.size(); i++) { 
            lines.addAll(mergeItems.get(i).getRightVersion()); 
         } 
         rightFile = new ParsedFile(lines); 
      } 
      return rightFile; 
   } 

   public ArrayList<MergeResultItem> getMergeItems(){ 
      return mergeItems; 
   } 

   public String getDefaultMergedResult(){       
      StringBuffer buf = new StringBuffer(); 
      for(int i=0;i<mergeItems.size();i++){ 
         if (i>0){ 
            buf.append(eol); 
         } 
         MergeResultItem item = mergeItems.get(i);          
         MergeResultItem.Type type = item.getType(); 
         String text = ""; 
         boolean defaultLeft = item.getDefaultVersion()==MergeResultItem.DefaultVersion.LEFT; 
         String leftText = getText(item.getLeftVersion()); 
         String rightText = getText(item.getRightVersion()); 
         switch(type){ 
         case NO_CONFLICT: 
            text = defaultLeft?leftText:rightText; 
            break; 
         case CONFLICT: 
            text = "<<CONFLICT>>"; 
            break; 
         case WARNING_ORDER: 
            text = leftText + eol + rightText; 
            break; 
         case WARNING_DELETE: 
            text = defaultLeft?leftText:rightText; 
            break; 
         }        
         buf.append(text); 
      } 
      return buf.toString(); 
   } 
    
   public boolean isConflict(){ 
      boolean result = false; 
      for(int i=0;!result && i<mergeItems.size();i++){ 
         MergeResultItem item = mergeItems.get(i);          
         if (item.getType()==MergeResultItem.Type.CONFLICT){ 
            result = true; 
         } 
      } 
      return result; 
   } 
    
   public boolean isWarning(){ 
      boolean result = false; 
      for(int i=0;!result && i<mergeItems.size();i++){ 
         MergeResultItem item = mergeItems.get(i);          
         if (item.getType()==MergeResultItem.Type.WARNING_DELETE || 
            item.getType()==MergeResultItem.Type.WARNING_ORDER){ 
            result = true; 
         } 
      } 
      return result; 
   } 

   private String getText(ArrayList<FileLine> lines){ 
      StringBuffer buf = new StringBuffer(); 
      for(int i=0;i<lines.size();i++){ 
         if (i>0){ 
            buf.append(eol); 
         } 
         buf.append(lines.get(i).getContent()); 
      } 
      return buf.toString(); 
   } 
} 
