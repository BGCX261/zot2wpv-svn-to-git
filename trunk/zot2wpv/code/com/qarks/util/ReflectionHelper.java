package com.qarks.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class ReflectionHelper {

	public static List<Field> getAllFields(Class clazz){
		List<Field> result = new ArrayList<Field>();
		while(!clazz.equals(Object.class)){
			Field fields[] = clazz.getDeclaredFields();
			for(Field field:fields){
				result.add(field);
			}
			clazz = clazz.getSuperclass();
		}
		return result;
	}
	
	public static Method getMethod(Class clazz, String name, Class parameterTypes[]) throws NoSuchMethodException{
		Method result = null;
		while(result==null && !clazz.equals(Object.class)){
			try {
				result = clazz.getDeclaredMethod(name, parameterTypes);
			} 
			catch (NoSuchMethodException e) {
				clazz = clazz.getSuperclass();
			}			
		}
		if (result==null){
			throw new NoSuchMethodException("method "+name+" not found in "+clazz);
		}
		return result;
	}
}
