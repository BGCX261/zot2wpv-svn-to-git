/**
 *  Copyright 2009 QArks.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 **/

package com.qarks.util.fetch.logic;

import java.io.Serializable;

public class DynamicDataSet
    implements Serializable {

  private Object[] dataRows;
  private int offset;
  private boolean last;

  public DynamicDataSet(Object[] dataRows, int offset, boolean last) {
    this.dataRows = dataRows;
    this.offset = offset;
    this.last = last;
  }

  public boolean isLast() {
    return last;
  }

  public Object[] getDataRows() {
    return dataRows;
  }

  public int getOffset() {
    return offset;
  }

  public void setOffset(int offset) {
    this.offset = offset;
  }

}
