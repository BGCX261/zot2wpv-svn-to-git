/**
 *  Copyright 2009 QArks.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 **/

package com.qarks.util.fetch.logic;

import java.util.Vector;

import javax.swing.DefaultListModel;


public class DynamicDataFetcher{

	private DynamicTableDataProvider dataProvider;
	private FetcherWorkerThread workerThread = null;
	private Vector<DynamicDataSet> pendingDataFetched = new Vector<DynamicDataSet>();

	public DynamicDataFetcher(DynamicTableDataProvider dataProvider){
		this.dataProvider = dataProvider;
	}

	public void asynchFetchData(Object fetchCriteria, String sortCriteria, boolean ascending, int offset, int count) {
		WaitingFetch fetch = new WaitingFetch(fetchCriteria,sortCriteria,ascending,offset,count);

		synchronized(this){
			if (workerThread==null){
				workerThread = new FetcherWorkerThread();
				workerThread.start();
			}
		}
		workerThread.addWaitingFetch(fetch);
	}

	public DynamicDataSet[] consumeFetchedDataSet(){
		int size = pendingDataFetched.size();
		DynamicDataSet[] result = new DynamicDataSet[size];
		for(int i=0;i<result.length;i++){
			result[i] = pendingDataFetched.remove(0);
		}
		return result;
	}

	////////////////////////////////////////////

	private class FetcherWorkerThread extends Thread{

		private Vector<WaitingFetch> waitingFetches = new Vector<WaitingFetch>();
		private boolean stop = false;

		public void addWaitingFetch(WaitingFetch waitingFetch){
//			System.out.println("adding a fetch request");
			synchronized(waitingFetches){
				waitingFetches.removeAllElements();
				waitingFetches.add(waitingFetch);
				waitingFetches.notify();
			}
		}

		public void run(){

			while(!stop){
				WaitingFetch currentFetch = null;
				synchronized(waitingFetches){
					try{
						while(waitingFetches.isEmpty()){
							waitingFetches.wait();
						}
						if (!waitingFetches.isEmpty()){
							currentFetch = waitingFetches.remove(0);
						}
					}
					catch(InterruptedException ex){
						ex.printStackTrace();
					}
				}

				if (currentFetch!=null){
//					System.out.println("fetching: "+currentFetch.count);
					DynamicDataSet dataSet = dataProvider.fetchData(currentFetch.fetchCriteria,
							currentFetch.sortCriteria,
							currentFetch.ascending,
							currentFetch.offset,
							currentFetch.count);
					pendingDataFetched.add(dataSet);
				}
			}
			System.out.println("exiting fetch thread");
		}
	}

	////////////////////////////////////////////

	private class WaitingFetch{

		private int offset;
		private int count;
		private boolean ascending;
		private String sortCriteria;
		private Object fetchCriteria;

		public WaitingFetch(Object fetchCriteria, String sortCriteria, boolean ascending, int offset, int count){
			this.offset = offset;
			this.count = count;
			this.ascending = ascending;
			this.sortCriteria = sortCriteria;
			this.fetchCriteria = fetchCriteria;
		}
	}


}
