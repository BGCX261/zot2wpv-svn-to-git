/**
 *  Copyright 2009 QArks.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 **/

package com.qarks.util;

import java.util.Vector;

public class Synchronizer {
  private Vector mEvents = new Vector(10);

  public Synchronizer() {

  }

  public void reset() {
    mEvents.removeAllElements();
  }

  public void waitEvents() {
    synchronized (mEvents) {
      if (mEvents.isEmpty()) {
        try {
          mEvents.wait();
        }
        catch (InterruptedException e) {
          Log.logError(this, "interruption", e);
        }
      }
      mEvents.removeAllElements();
    }
  }

  public void notifyEvent() {
    synchronized (mEvents) {
      mEvents.addElement(new Object());
      mEvents.notifyAll();
    }
  }

  public boolean waitEvents(int timeout) {
    // this method return true if timeout has
    // elapsed without event notified
    boolean result = true;

    synchronized (mEvents) {
      if (mEvents.isEmpty()) {
        try {
          mEvents.wait(timeout);
        }
        catch (InterruptedException e) {
          Log.logError(this, "interruption before timeout", e);
        }
      }
      result = mEvents.isEmpty();
      mEvents.removeAllElements();
    }
    return result;
  }
}
