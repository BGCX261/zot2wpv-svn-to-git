/**
 *  Copyright 2009 QArks.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 **/

package com.qarks.util;


import java.util.*;
import java.io.Serializable;

public final class ObjectUID implements Serializable {

	private static final long serialVersionUID = -1843127436096725989L;
	private long time = -1;
	private double random = -1;

	public ObjectUID() {
	}

	public static ObjectUID getNewObjectUID(){
		ObjectUID uid = new ObjectUID();
		uid.time = new Date().getTime();
		uid.random = Math.random();
		return uid;
	}

	public Object clone(){
		ObjectUID result = new ObjectUID();
		result.time = time;
		result.random = random;
		return result;
	}

	public boolean equals(Object object) {
		boolean equals = false;
		if (object != null) {
			if (object.getClass().equals(ObjectUID.class)) {
				ObjectUID ObjectUID = (ObjectUID) object;
				equals = (ObjectUID.time == time) && (ObjectUID.random == random);
			}
		}
		return equals;
	}

	public int hashCode() {
		final long longRandom = Double.doubleToLongBits(random);
		return (int) (longRandom ^ (longRandom >> 32));
	}

	public String toString() {
		StringBuffer s = new StringBuffer();
		s.append(Long.toHexString(time));
		s.append("_");
		s.append(Long.toHexString(Double.doubleToLongBits(random)));
		return s.toString();
	}

	public static ObjectUID fromStringToObjectUID(String string) {
		ObjectUID objectUID = new ObjectUID();
		StringTokenizer s = new StringTokenizer(string, "_", false);
		objectUID.time = Long.parseLong(s.nextToken(), 16);
		objectUID.random = Double.longBitsToDouble(Long.parseLong(s.nextToken(), 16));
		return objectUID;
	}

	public static ObjectUID fromLongStringToObjectUID(String string) {
		ObjectUID objectUID = new ObjectUID();
		StringTokenizer s = new StringTokenizer(string, "_", false);
		objectUID.time = Long.parseLong(s.nextToken(), 10);
		objectUID.random = Long.parseLong(s.nextToken(), 10);
		return objectUID;
	}

	public double getRandom() {
		return random;
	}
	public long getTime() {
		return time;
	}
	public void setRandom(double random) {
		this.random = random;
	}
	public void setTime(long time) {
		this.time = time;
	}
}
