/**
 *  Copyright 2009 QArks.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 **/

package com.qarks.util;


import java.io.*;
import java.util.*;

public class SystemPropertiesHelper {

	public static final String PATH = "%PATH%";

	public static void loadProperties(File properyFile) {
		try{
			FileInputStream propertiesInputStream = new FileInputStream(properyFile);
			loadProperties(propertiesInputStream);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void loadProperties(InputStream propertiesInputStream) {
		String workingDir = System.getProperty("user.dir");
		Properties properties = new Properties();
		try {
			properties.load(propertiesInputStream);
			Enumeration enumeration = properties.keys();
			while (enumeration.hasMoreElements()) {
				String key = (String) enumeration.nextElement();
				String property = properties.getProperty(key);
				property = replacePath(property, PATH, workingDir);
				System.setProperty(key, property);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if (propertiesInputStream != null) {
				try {
					propertiesInputStream.close();
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static String replacePath(String source, String sourceToker,
			String destToken) {
		int index = -1;
		while ( (index = source.indexOf(sourceToker)) > -1) {
			String result = source.substring(0, index) + destToken;
			if (source.length() > index + sourceToker.length()) {
				result = result + source.substring(index + sourceToker.length());
			}
			source = result;
		}
		return source;
	}

}
