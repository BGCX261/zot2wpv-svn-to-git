
import name.fraser.neil.plaintext.diff_match_patch
import name.fraser.neil.plaintext.diff_match_patch.Diff
import name.fraser.neil.plaintext.diff_match_patch.Operation
import java.util.regex.Matcher

extDir  = new File(".")
dataDir = new File(extDir, 'data')
revisionsDir = new File(dataDir, 'article-revisions')

File findRevisionFile(String revId)
{
   revId = revId + '.txt'
   File revFile = null
   revisionsDir.eachDir { File dir ->
      if (revFile == null) {
        File f = new File(dir, revId)
        if (f.exists()) { revFile = f }
      }
   }

   revFile
}

def printDiffString(String prefix, Diff d) { d.text.split('\n').each { println "$prefix $it" } }

def listDifferences(List<Diff> diff)
{
//  diff.each { Diff d ->  println d }

//   diff.each { Diff d -> if (d.operation != Operation.EQUAL ) println d }
//  diff.each { Diff d -> if (d.operation == Operation.DELETE ) printDiffString ('-', d) }
//  diff.each { Diff d -> if (d.operation == Operation.INSERT ) printDiffString ('+', d) }

  diff.each { Diff d ->
    if (d.operation == Operation.DELETE ) printDiffString ('-', d)
    if (d.operation == Operation.INSERT ) printDiffString ('+', d)
  }
}

differ = new diff_match_patch()
differ.Diff_Timeout = 3

nextCode = initialCode = new Character(0xC000 as char)

wordPattern = ~/[\w\uC000-\uFFFE]+/
urlPattern = ~'(?x) ^(https?):// ([^/:]+) (?::(\\d+))?'

codePattern = ~/[\uC000-\uFFFE]/

def encodeWords(Map<String, Character> plainToCode, Map<Character, String> codeToPlain, String text)
{
  StringBuffer sbuf = new StringBuffer(text.length());

  Matcher m = wordPattern.matcher(text);
  while (m.find()) {
      String word = m.group()

      Character code = plainToCode[word]

      if (code == null) {
         nextCode = nextCode + 1
         plainToCode[word] = code = nextCode
         codeToPlain[code] = word
      }

      m.appendReplacement(sbuf, code as String);
  }
  m.appendTail(sbuf);

  sbuf.toString();
}

//def encodeURLs(Map<String, Character> plainToCode, Map<Character, String> codeToPlain, String text)
//{
//  StringBuffer sbuf = new StringBuffer(text.length());
//
//  Matcher m = urlPattern.matcher(text);
//  while (m.find()) {
//      String word = m.group()
//
//      Character code = plainToCode[word]
//
//      if (code == null) {
//         nextCode = nextCode + 1
//         plainToCode[word] = code = nextCode
//         codeToPlain[code] = word
//      }
//
//      m.appendReplacement(sbuf, code as String);
//  }
//  m.appendTail(sbuf);
//
//  sbuf.toString();
//}
//

def decodeWords(Map<Character, String> codeToPlain, String text)
{
  StringBuffer sbuf = new StringBuffer(text.length());

  Matcher m = codePattern.matcher(text);
  while (m.find()) {
      String code = m.group()

      m.appendReplacement(sbuf, codeToPlain[code.charAt(0)]);
  }
  m.appendTail(sbuf);

  sbuf.toString();
}

def listDifferences(String newid, String oldid)
{
  println "newid $newid oldid $oldid\n"

  String oldText = findRevisionFile(oldid).text
  String newText = findRevisionFile(newid).text

  nextCode = initialCode

  Map plainToCode = new HashMap<String, Character>()
  Map codeToPlain = new HashMap<Character, String>()

//  oldText = encodeURLs(plainToCode, codeToPlain, oldText)
//  newText = encodeURLs(plainToCode, codeToPlain, newText)
  oldText = encodeWords(plainToCode, codeToPlain, oldText)
  newText = encodeWords(plainToCode, codeToPlain, newText)

  List<Diff> differences = differ.diff_main(oldText, newText)
  differ.diff_cleanupSemantic(differences)

  differences.each { Diff d -> d.text = decodeWords(codeToPlain, d.text) }
  
  listDifferences(differences)

  println '\n---\n'
  
//  differences = differ.diff_main(findRevisionFile(oldid).text, findRevisionFile(newid).text)
//  differ.diff_cleanupEfficiency(differences)
//  listDifferences(differences)
//
//  println '\n---\n'
}

// Edit :  20366    Vandalism    Editor :  205.202.138.53    newid :  329076459    oldid :  328513217

// Edit :  31723    Regular    Editor :  Wconaventura    newid :  328713139    oldid :  328713088
listDifferences('328713139', '328713088')

// Edit :  373    Regular    Editor :  Hauganm    newid :  327118012    oldid :  325867244
listDifferences('327118012', '325867244')

// Edit :  43633    Vandalism    Editor :  67.193.144.69    newid :  328121726    oldid :  328121608
listDifferences('328121726', '328121608')

// This should see the new line as a single insertion without anything saved from the old line.
// Or maybe not.  The new text is ALL CAPS, while the preserved words are not.
// So insertion analysis should be on the true minimal change, whereas phrasal analysis should
// see words surrounding and between the inserted words.
// This edit is also a mass delete.  Ton of text with a lot of wikimarkup replaced by much less with no markup.
// Edit :  24828    Vandalism    Editor :  70.251.133.197    newid :  328314831    oldid :  328313468
listDifferences('328314831', '328313468')

// Edit :  20860    Vandalism    Editor :  92.3.230.178    newid :  329295100    oldid :  329056440
listDifferences('329295100', '329056440')

// Edit :  19616    Vandalism    Editor :  74.62.72.98    newid :  329266740    oldid :  327850271
// A line splitting insertion.  diff_match_patch handles this pretty well with semantic cleanup.
listDifferences('329266740', '327850271')

// Edit :  20366    Vandalism    Editor :  205.202.138.53    newid :  329076459    oldid :  328513217
listDifferences('329076459', '328513217')

// Edit :  45222    Vandalism    Editor :  74.84.99.19    newid :  327486516    oldid :  322660251
// Obvious mass delete with markup-free replacement.
listDifferences('327486516', '322660251')

// Edit :  8473    Vandalism    Editor :  Steele eleets    newid :  328440431    oldid :  326943319
listDifferences('328440431', '326943319')

//  Edit :  25584    Vandalism    Editor :  74.93.85.101    newid :  329258306    oldid :  329258136
// This currently diffs as:
//- the Plymouth
//- ment
//- it
//+ joe sheidt
//+ d
//+ he
//+ he
// I would like the diff to not splice words.  That can be done by tokenizing and substituting unique token codes.
// It may also be possible to do something to the semantic filter to avoid intraword splits.
listDifferences('329258306', '329258136')

// Edit :  16989    Vandalism    Editor :  146.186.52.112    newid :  326840218    oldid :  325913195
// Similar problem here.  We get:
//+ go
// and we want:
//- http://www.joeylogano.com
//+ http://www.gojoeylogano.com
listDifferences('326840218', '325913195')
