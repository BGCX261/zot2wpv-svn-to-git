import java.util.regex.Pattern;
import java.util.regex.Matcher;

import org.cyberneko.html.filters.DefaultFilter;
import org.apache.xerces.xni.QName;
import org.apache.xerces.xni.XMLAttributes;
import org.apache.xerces.xni.Augmentations;
import org.apache.xerces.xni.XNIException;
import org.apache.xerces.xni.XMLString;
import org.junit.Test;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertSame;
import static junit.framework.Assert.assertTrue;
import sunx.text.EntityConverter;
import sunx.text.Normalizer;


public class WikiTextCleaner
{
        static Pattern externalLinkPattern = Pattern.compile("(?s)\\[[^\\s\\]]*\\s?([^\\]]*)\\]");

        static Pattern htmlBreak = Pattern.compile("(?is)<\\s*br\\s*/?>");

        static Pattern[] patternsToStrip = {
                Pattern.compile("'''''"),
                Pattern.compile("'''"),
                Pattern.compile("''"),
                Pattern.compile("(?s)\\{\\|.*?\\|\\}"),     // Tables.
                Pattern.compile("(?s)\\{\\{.*?\\}\\}"),     // Templates.
                Pattern.compile("(?is)<math>.*?<math>"),    // Mathematical expressions.
                Pattern.compile("(?is)<ref>.*?</ref>"),     // Inline references.
                Pattern.compile("<[^>]*>")                  // HTML tags.

        };

        public static String clearWikiText(String content)
        {
            // clear the article of wikitext

            content = EntityConverter.replaceEntities(content);

            content = clearWikiTemplates(content);

            content = clearMetaDivs(content);

            content = clearWikiLinks(content);

            content = externalLinkPattern.matcher(content).replaceAll("$1");

            content = htmlBreak.matcher(content).replaceAll("\n");

            content = zapGremlins(content);

            for (Pattern p : patternsToStrip) {
                content = p.matcher(content).replaceAll("");
            }

            return content;
        }

        static Pattern wikiTextPattern = Pattern.compile("(?s)^(.*?)\\]\\]");

        public static String clearWikiLinks(String content)
        {
            int x = 0;

            while ((x = content.indexOf("[[")) >= 0) {
                content = content.substring(0, x) + textFromWikiLink(content.substring(x + 2));
            }

            return content;
        }

        static Pattern foreignWikiCategoryPattern = Pattern.compile("^[a-z][a-z]:.*");

        static Pattern altTextPattern = Pattern.compile("(?i)alt\\s*=([^|]*)");

        static Pattern imageFormatAttributePattern = Pattern.compile("\\|(?:(?:right)|(?:left)|(?:center)|(?:none)"
                + "|(?:upright(?:=[\\d\\.]+)?)|(?:baseline)|(?:middle)|(?:sub)|(?:super)|(?:text-top)|(?:text-bottom)|(?:top)|(?:bottom)"
                + "|(?:thumb)|(?:frame)|(?:frameless)|(?:link(?:=[^|]*))"
                + "|(?:[x\\d]+px))(?=\\||$)");

        static String textFromWikiLink(String content)
        {
            while (true) {
                int endText = content.indexOf("]]");

                // Badly formed wikiLink.
                if (endText < 0) return content;

                int nextLink = content.indexOf("[[");

                // We can stop recursing when there are no more wikiLink openers before the next ender.
                if ((nextLink < 0) || (nextLink > endText)) break;

                content = content.substring(0, nextLink) + textFromWikiLink(content.substring(nextLink + 2));
            }

            Matcher m = wikiTextPattern.matcher(content);
            StringBuffer sbuf = new StringBuffer(content.length());
            if (m.find()) {
                String wikitext = m.group(1);

                // If this is a category link, just clear it.
                // It won't have text that is article content, and so we don't want it appearing as such.
                if (wikitext.startsWith("Category:")) wikitext = "";
                if (wikitext.startsWith("Wikipedia:")) wikitext = "";
                if (wikitext.startsWith("Help:")) wikitext = "";
                if (wikitext.startsWith("Template:")) wikitext = "";
                if (wikitext.startsWith("Book:")) wikitext = "";
                if (wikitext.startsWith("WikiMedia:")) wikitext = "";
                if (wikitext.startsWith("Portal:")) wikitext = "";
                if (wikitext.startsWith("File:")) wikitext = "";
                if (wikitext.startsWith("User:")) wikitext = "";

                // Two letter categories are links to other language Wikipedias and contain forgeign language text.
                // We don't want them here either.
                if (foreignWikiCategoryPattern.matcher(wikitext).matches()) wikitext = "";

                if (wikitext.startsWith("Image:")) {
                    // We don't want to just strip these.  Try and get the alt text if present.
                    Matcher altm = altTextPattern.matcher(wikitext);
                    if (altm.find()) {
                        wikitext = altm.group(1);
                    } else {
                        wikitext = imageFormatAttributePattern.matcher(wikitext).replaceAll("");

                        if (!wikitext.contains("|")) wikitext = "";
                    }
                }

                if (wikitext.contains("|")) {
                    if (wikitext.endsWith("|")) {
                        // this is a "pipe trick" link, the pipe is used to hide text in
                        // parentheses or after a comma

                        // The text before the pipe at the end and after any that precede it.
                        Matcher matcher = Pattern.compile("([^|]*)\\|$").matcher(wikitext);

                        if (matcher.find()) {
                            wikitext = matcher.group(1).trim();
                        } else {
                            // This shouldn't happen.
                            wikitext = "";
                        }

                        // Remove first namespace, if any.
                        wikitext = wikitext.replaceFirst("^:?\\w*:", "");

                        if (wikitext.endsWith(")")) {
                            wikitext = wikitext.replaceAll("\\(.*?\\)$", "");
                        } else if (wikitext.contains(",")) {
                            wikitext = wikitext.substring(0, wikitext.indexOf(",") - 1);
                        }
                    } else {
                        // this is a beautified wikilink, get the displayed text
                        wikitext = wikitext.substring(wikitext.lastIndexOf("|") + 1);
                    }
                }

                m.appendReplacement(sbuf, Matcher.quoteReplacement(wikitext));
            }
            m.appendTail(sbuf);
            content = sbuf.toString();
            return content;
        }

        public static String clearWikiTemplates(String content)
        {
            int x = 0;

            while ((x = content.indexOf("{{", x)) >= 0) {
                content = content.substring(0, x) + textFromWikiTemplates(content.substring(x + 2));
            }

            return content;
        }

        static Pattern wikiTemplatePattern = Pattern.compile("(?s)^(.*?)}}");

        static String textFromWikiTemplates(String content)
        {
            while (true) {
                int endText = content.indexOf("}}");

                // Badly formed wikiLink.
                if (endText < 0) return content;

                int nextLink = content.indexOf("{{");

                // We can stop recursing when there are no more wikiLink openers before the next ender.
                if ((nextLink < 0) || (nextLink > endText)) break;

                content = content.substring(0, nextLink) + textFromWikiTemplates(content.substring(nextLink + 2));
            }

            StringBuffer sbuf = new StringBuffer(content.length());

            Matcher m = wikiTemplatePattern.matcher(content);
            if (m.find()) {
                String wikitext = ""; // m.group(1);

                m.appendReplacement(sbuf, Matcher.quoteReplacement(wikitext));
            }
            m.appendTail(sbuf);

            return sbuf.toString();
        }

        //    static Pattern divPattern = Pattern.compile("(?is)<div (?:class=((?:\"[^\"]*\")|(?:'[^']*')))>(.*?)(?!<div>)(</div>)?");
        static Pattern disambPattern = Pattern.compile("(?i)^\\W*For .*");
        static Pattern divPattern = Pattern.compile("(?is)<div\\s*((?:\\w+\\s*=\\s*(?:(?:\"[^\"]*\")|(?:'[^']*'))\\s*)*)>(.*?)</div>");
        static Pattern galleryPattern = Pattern.compile("(?is)<gallery\\s*((?:\\w+\\s*=\\s*(?:(?:\"[^\"]*\")|(?:'[^']*'))\\s*)*)>(.*?)</gallery>");

        public static String clearMetaDivs(String content)
        {
            // Remove initial line(s) if it is an old-style disambiguation message ("For ...").
            content = content.trim();

            do {
                while (content.startsWith("\n")) content = content.substring(1).trim();

                content = disambPattern.matcher(content).replaceFirst("").trim();
            } while (content.startsWith("\n"));

            // Strip image galleries.  They are like a wikiLink per line.
            // Be nice to keep the captions, but not worth the hassle here.
            content = galleryPattern.matcher(content).replaceAll("");

            StringBuffer sbuf = new StringBuffer(content.length());

            // TODO: Deal with nested DIVs.
            // How many divs deep are we?
            int depth = 0;
            // Are we inside a div that we're removing?
            boolean ignoring = false;

            Matcher m = divPattern.matcher(content);
            while (m.find()) {
                String attributes = m.group(1).toLowerCase();
                String wikitext = m.group(2);

                // Is this a div containing non-article text content?
                if ((attributes.indexOf("messagebox") >= 0) || (attributes.indexOf("cleanup") >= 0) || (attributes.indexOf("metadata") >= 0)) {
                    // Yup.  We want to remove all of it.

                    ignoring = true;

                    wikitext = "";
                }

                m.appendReplacement(sbuf, Matcher.quoteReplacement(wikitext));
            }
            m.appendTail(sbuf);

            return sbuf.toString();
        }

        // Sections we don't want to parse.  Packed and sorted for binary searching convenience.
        // Of course if this were in Groovy wouldn't need to have done the conversion manually...
        static String[] sectionsToIgnore = {"externallinks", "furtherreading", "notes", "references", "seealso"};

        static Pattern definitionListPattern = Pattern.compile("^;[\\w\\s]*:(.*)$");

//        public String stripWikiHeaders(String content)
//        {
//            final StringBuffer sb = new StringBuffer(content.length());
//
//            final BufferedReader br = new BufferedReader(new StringReader(content));
//
//            // We start by not ignoring lines.
//            boolean ignoringSection = false;
//            // We'll start as though we know that the previous sentence is closed.
//            boolean periodSeen = true;
//            // Silliness to turn lists into something sentence-like.
//            boolean colonSeen = false;
//            boolean needPeriod = false;
//
//            try {
//                while (br.ready()) {
//                    String line = br.readLine();
//
//                    if (line == null) break;
//
//                    line = line.trim();
//
//                    if (line.startsWith("=") && line.endsWith("=")) {
//                        // It's a section header.
//                        // Ditch all the non-word characters.
//                        line = line.replaceAll("\\W", "").toLowerCase().trim();
//
//                        // Is this or is this not a heading that means we ignore the section?
//                        ignoringSection = !(Arrays.binarySearch(sectionsToIgnore, line) < 0);
//
//                        if (!ignoringSection) {
//                            if (!periodSeen || needPeriod) {
//                                sb.append(".\n");
//                            }
//
//                            periodSeen = true;
//                            colonSeen = false;
//                            needPeriod = false;
//                        }
//                        // And we ignore the heading in any case.
//                        // If we wanted to parse the heading, then this is where we would
//                        // make it look like a sentence and include it in the output.
//                    } else {
//                        if (!ignoringSection) {
//                            // A line that starts with a colon is a quotation.
//                            // We could consider this a sentence split, but this is not really the right place for that.
//                            // Do this first because some people use it to indent list items (which is wrong, but whatever).
//                            if (line.startsWith(":")) line = line.replaceFirst("^:+", "");
//
//                            // A line that starts with a semicolon is a definition list,
//                            // but I leave it in for now because it may help the sentence splitter.
//
//                            // Try to get sentences to break appropriately.
//                            // This should properly be done in a markup-aware sentence splitter.
//                            if (line.startsWith("*")) {
//                                // Bullet list.
//                                // Hard to say what to do with these.
//                                // Try a stitch the together with semicolons if there aren't periods.
//                                line = line.replaceFirst("^[*#+:]+", !(colonSeen || periodSeen) ? "; " : "");
//                                needPeriod = true;
//                            } else if (line.startsWith("#")) {
//                                // Numbered list.
//                                // Same problem as with bullet lists.
//                                // Same hack.
//                                line = line.replaceFirst("^[*#+:]+", !(colonSeen || periodSeen) ? "; " : "");
//                                needPeriod = true;
//                            } else if (line.startsWith("----")) {
//                                // A horizontal rule.
//                                // A sentence shouldn't span a rule,
//                                // so we'll toss in a period if previous line doesn't have one.
//                                line = line.replaceFirst("^-+", (needPeriod || !periodSeen) ? ". " : "");
//                                needPeriod = false;
//                            } else if (needPeriod) {
//                                // This means this is a line following a list mark, but it doesn't have one.
//                                // If they didn't put a period at the end of the last item, then we'll throw one in.
//                                if (!periodSeen) sb.append(". ");
//                                periodSeen = true;
//                                colonSeen = false;
//                                needPeriod = false;
//                            }
//
//                            // If this line is a term definition, strip off the term part.
//                            Matcher m = definitionListPattern.matcher(line);
//
//                            if (m.find()) {
//                                line = m.group(1).trim();
//                            }
//
//                            // It's been through the gauntlet, so we'll allow it is a line of text now.
//                            sb.append(line);
//                            sb.append('\n');
//
//                            // We may have appended a space.
//                            line = line.trim();
//
//                            // Does this line end with a period?
//                            // But if this is a blank line, then just keep what we found on last non-blank line.
//                            if (line.length() > 0) {
//                                periodSeen = line.endsWith(".") || line.endsWith("!") || line.endsWith("?");
//                                if (periodSeen) needPeriod = false;
//                                colonSeen = line.endsWith(":") || line.endsWith(";") || line.endsWith(",");
//                            }
//                        }
//                    }
//                }
//            } catch (IOException e) {
//                logger.log("stripWikiHeaders: ", e);
//                return content;
//            }
//
//            if (needPeriod) sb.append(".\n");
//
//            return sb.toString();
//        }

        final static Pattern anyKindOfGremlin = Pattern.compile("[\u00A0\u2018\u2019\u201C\u201D\u2010-\u2015]");
        final static Pattern anyKindOfDash = Pattern.compile("[\u2010-\u2015]");
        final static Pattern anyCombiningMark = Pattern.compile("\\p{M}");

        // Reusing a Matcher would be fastest, but we don't wanna open concurrency worms.
        // final static Matcher anyKindOfDash = new Matcher('[\u2010-\u2015]', '')

        /**
         * By default, OpenOffice has various automatic "corrections" enabled.
         * "Smart" quotes and hyphens are usually very trouble when they occur
         * in code.  Also the PrepareForHTML macro replaces blanks with non-breaking
         * spaces, those are also usually a problem.
         * So Wings will zap those gremlins prior to evaluating code.
         * <p/>
         * TODO: Make this configurable and optional.
         * TODO: More intelligence so Unicode strings and comments can be literal if they like.
         */
        public static String zapGremlins(String text)
        {
            // Normalize for "compatibility".
            // That means not only seperate diacriticals but also split ligatures.
            text = Normalizer.normalize(text, Normalizer.Form.NFKD);

            if (anyKindOfGremlin.matcher(text).find()) {
                // The String.replace(char, char) method tries to be fast and doesn't
                // create a new string if there are no replacements.
                // So in this case these multiple calls are quicker than some big
                // complicated method that does it all in one pass.

                // Replace non-breaking space with regular space.
                text = text.replace('\u00A0', ' ');

                // Replace curly quotes with plain.

                text = text.replace('\u2018', '\'');
                text = text.replace('\u2019', '\'');
                text = text.replace('\u00b4', '\'');    // Grave accent to single quote.
                text = text.replace('\u201C', '"');
                text = text.replace('\u201D', '"');

                // Anything that looks like a dash...
                // text = text.replaceAll('[\u2010-\u2015]', '-')
                // text = anyKindOfDash.reset(text).replaceAll('-')
                text = anyKindOfDash.matcher(text).replaceAll("-");
            }

            // Get rid of any combining marks.
            text = anyCombiningMark.matcher(text).replaceAll("");

            return text;
        }

        static Pattern htmlTags = Pattern.compile("(?is)<\\s*(:(:br\\W)|(:img\\W)|(:p\\W)|(:[a-z]+\\s*/>))");

        static boolean hasHTML(String content)
        {
//        return htmlTags.matcher(content).find();
            return content.contains("<");
        }

//        protected String htmlToText(String content, String url, String articleId)
//        {
////        DOMFragmentParser parser = new DOMFragmentParser();
////        HTMLDocument document = new HTMLDocumentImpl();
////        DocumentFragment fragment = document.createDocumentFragment();
//            try {
//                // create element remover filter
//                ElementRemover remover = new ElementRemover();
//
//                // set which elements to accept
//                remover.acceptElement("p", null);
//                remover.acceptElement("br", null);
//
//                // completely remove script elements
//                remover.removeElement("script");
//
//                LinebreakFilter linebreaker = new LinebreakFilter();
//
//                StringWriter sw = new StringWriter();
//
//                // create writer filter
//                org.cyberneko.html.filters.Writer writer =
//                    new org.cyberneko.html.filters.Writer(sw, "UTF-8");
//
//                // setup filter chain
//                XMLDocumentFilter[] filters = {
//                    remover,
//                    linebreaker,
//                    writer,
//                };
//
//                // create HTML parser
//                XMLParserConfiguration parser = new HTMLConfiguration();
//                parser.setProperty("http://cyberneko.org/html/properties/filters", filters);
//
//                String systemId = "content:blog:" + articleId;
//
//                parser.parse(new XMLInputSource(url, systemId, systemId, new StringReader(content), "UTF-8"));
//
//                content = sw.toString();
//
//                content = sunx.text.EntityConverter.replaceEntities(content);
//
//                content = convertQuotesWithWrongEncoding(content);
//
//                content = zapGremlins(content);
//            } catch (IOException e) {
//                logger.log("Trouble in htmlToText.", e);
//            }
//
//            return content;
//        }

        static Pattern doubledUTFedSingle = Pattern.compile("(?s)\u00E2?\u0080?[\u0098\u0099]");
        static Pattern doubledUTFedDouble = Pattern.compile("(?s)\u00E2?\u0080?[\u009D\u009C]");

        static String convertQuotesWithWrongEncoding(String content)
        {
            // These are ANSI.
            content = content.replace('\u0091', '\'');
            content = content.replace('\u0092', '\'');
            content = content.replace('\u0093', '"');
            content = content.replace('\u0094', '"');

            // This is UTF-8 encoded twice.
            content = doubledUTFedSingle.matcher(content).replaceAll("'");
            content = doubledUTFedDouble.matcher(content).replaceAll("\"");

            // This is what happens to MacRoman:

            // Elipsis can wind up looking like E with accent.
            content = content.replace('\u00C9', '\u2026');

            content = content.replace('\u00d2', '"');
            content = content.replace('\u00d3', '"');
            content = content.replace('\u00d4', '\'');
            content = content.replace('\u00d5', '\'');

            return content;
        }

        class LinebreakFilter extends DefaultFilter
        {
            public void startElement(QName element, XMLAttributes attributes, Augmentations augs)
                    throws XNIException
            {

            }

            char[] newline = { '\n' };

            public void emptyElement(QName element, XMLAttributes attributes, Augmentations augs)
                    throws XNIException
            {
                super.characters(new XMLString(newline, 0, newline.length), augs);
            }

            public void endElement(QName element, Augmentations augs)
                throws XNIException
            {
                super.characters(new XMLString(newline, 0, newline.length), augs);
            }

        }


        @Test
        public static void testClearWikiText()
        {
            assertEquals("abcAaAaX\u03A6\u03C6", clearWikiText("abc&Aring;&aring;\u00C5\u00E5X\u03A6\u03D5"));
        }

        @Test
        void test_clearExternalLinks()
        {
            assertEquals("abccopyrighted 2006. Paternal leave rights compiled from International Labour Organization site at  defbarxyzhoo"
                    , clearWikiText("abc[" +
                            "http://www.ilo.org/public/english/protection/condtrav/family/reconcilwf/specialleave.htm copyrighted 2006. " +
                            "Paternal leave rights compiled from [[International Labour Organization]] site at <foo> <ref>" +
                            "]def[http:/foo.com bar]xyz[]hoo"));
        }

        @Test
        void test_clearCitations()
        {
            assertEquals("abcdefbarxyzhoo"
                    , clearWikiText("abc[2]def[1 bar]xyz[]hoo"));
        }

        @Test
        void test_clearTables()
        {
            assertEquals("abcdefxyz"
                    , clearWikiText("abc{| border=\"1\" cellspacing=\"0\" cellpadding=\"8\" style=\"font-size: small\"\n" +
                            "|-- bgcolor=\"lightgreen\"\n" +
                            "! [[Country]]\n" +
                            "| \n" +
                            "| \n" +
                            "|}def{| hey |}xyz"));
            // Don't want infoboxes or other templates either.
            assertEquals("x\n\nNicolae Jetty Carpathia"
                    , clearWikiText("x\n{{Infobox Officeholder\n" +
                            "| name            =Nicolae Carpathia\n" +
                            "| nationality     =[[Romania]]n\n" +
                            "| image           =\n" +
                            "| caption         =\n" +
                            "| order           =Supreme [[Potentate]]<br />of the [[Global Community]]\n" +
                            "| term_start      =''[[Tribulation Force]]''\n" +
                            "| term_end        =''[[Glorious Appearing]]''\n" +
                            "| vicepresident   =[[Leon Fortunato]]<br />(Supreme Commander)\n" +
                            "| deputy          =\n" +
                            "| predecessor     =Inaugural\n" +
                            "| successor       =[[Leon Fortunato]]<br />(temporarily)\n" +
                            "| birth_date      =unknown\n" +
                            "| birth_place     ={{flagicon|Romania}} [[Cluj County|Cluj]], [[Romania]]\n" +
                            "| death_date      =unspecified\n" +
                            "| death_place     ={{flagicon|Israel}} [[mountain|Har]] [[Megiddo (place)|Megiddo]], [[Israel]]\n" +
                            "| constituency    =[[Global Community]]\n" +
                            "| party           =\n" +
                            "| profession      =[[politician]], [[antichrist]]\n" +
                            "| religion        =[[Carpathianism]]\n" +
                            "| signature       =\n" +
                            "| footnotes       =\n" +
                            "| order2          =[[United Nations Secretary-General]]\n" +
                            "| term_start2     =''[[Left Behind]]''\n" +
                            "| term_end2       =''[[Tribulation Force]]''\n" +
                            "| president       =\n" +
                            "| predecessor2    =[[Mwangati Ngumo]]\n" +
                            "| successor2      =position dissolved\n" +
                            "| order3          =[[President of Romania]]\n" +
                            "| term_start3     =prior to ''Left Behind''\n" +
                            "| term_end3       =''Left Behind''\n" +
                            "| predecessor3    =\n" +
                            "| successor3      =\n" +
                            "}}\n" +
                            "'''Nicolae Jetty Carpathia''' "));
        }

        @Test
        void test_clearReferences()
        {
            assertEquals("abcde&fxyz"
                    , clearWikiText("abc<ref>\n" +
                            "Paternal leave rights compiled from [[International Labour Organization]] site at <foo> <ref>\n" +
                            "http://www.ilo.org/public/english/protection/condtrav/family/reconcilwf/specialleave.htm copyrighted 2006. \n" +
                            "| \n" +
                            "</ref>de&f<ref></ref>xyz"));
        }

        @Test
        void test_clearTemplates()
        {
            assertEquals("abcdefxyz"
                    , clearWikiText("abc{{\n" +
                            "Paternal leave rights compiled from [[International Labour Organization]] site at <foo> <ref>\n" +
                            "| \n" +
                            "}}def{{}}xyz"));
        }

        @Test
        void test_clearMetaDivs()
        {
            assertEquals(""
                    , clearWikiText("[[Wikipedia:Verifiability|verified]]<div foo='123' class=\"messagebox cleanup metadata\" >This article or section contains information that has '''not been\n" +
                            "''' and thus might not be reliable. \n" +
                            "If you are familiar with the subject matter, please check for inaccuracies and modify as needed, \n" +
                            "'''[[Wikipedia:cite sources|citing sources]]'''.</div>"));
            assertEquals("abcdefxyz"
                    , clearWikiText("abc<div class=\"messagebox cleanup metadata\">This article or section contains information that has '''not been\n" +
                            "[[Wikipedia:Verifiability|verified]]''' and thus might not be reliable. \n" +
                            "If you are familiar with the subject matter, please check for inaccuracies and modify as needed, \n" +
                            "'''[[Wikipedia:cite sources|citing sources]]'''.</div>[[Category:Wikipedia articles needing factual verification]]def{{}}xyz"));
            assertEquals("A recruitment poster is a poster used in advertisement to recruit people into an organization, and is the most popularized method of military recruitment .\n" +
                    "\n\n\n== See also =="
                    , clearWikiText("A '''recruitment poster''' is a [[poster]] used in [[advertisement]] to [[recruitment|recruit]] people into an [[organization]], and is the most popularized method of military recruitment .\n" +
                            "\n" +
                            "<gallery>\n" +
                            "Image:New Names Canadian WW1 recruiting poster.jpg|A [[Canadian]] WWI recruitment poster\n" +
                            "Image:Kitchener-Britons.jpg|A [[World War I]] recruitment poster featuring [[Horatio Kitchener, 1st Earl Kitchener|Lord Kitchener]] (British [[Secretary of State for War|Minister of War]])\n" +
                            "Image:Unclesamwantyou.jpg|J. M. Flagg's [[Uncle Sam]] recruited soldiers for [[World War I]].  Based on the [[Lord Kitchener Wants You|Kitchener poster]].\n" +
                            "Image:Australian WWI recruiting poster.jpg|An [[Australia|Australian]] WWI recruitment poster\n" +
                            "</gallery>\n" +
                            "\n" +
                            "== See also =="));
        }

//        @Test
//        void test_stripWikiHeaders()
//        {
//            assertEquals("Of the twelve S-boats that were in service in 1939, only three survived to see the end of World War II, " +
//                    "a loss rate that inspired the song \"Twelve Little S-Boats\", based on a nursery rhyme originally written by Septimus Winner in 1868. " +
//                    "The survivors were Sealion, Seawolf and Sturgeon.\n" +
//                    "\n" +
//                    "It's definition.\n" +
//                    "Twelve little S-boats \"go to it\" like Bevin,\n" +
//                    "Starfish goes a bit too far - then there were eleven.\n" +
//                    "Eleven watchful S-boats doing fine and then\n" +
//                    "Seahorse fails to answer - so there are ten.\n" +
//                    "Ten stocky S-boats in a ragged line,\n" +
//                    "Sterlet stops and drops out - leaving us nine.\n" +
//                    "Nine plucky S-boats, all pursuing fate,\n" +
//                    "Shark is overtaken - now we are eight.\n" +
//                    "Eight sturdy S-boats, men from Hants and Devon,\n"
//                    , stripWikiHeaders(clearWikiText("Of the twelve ''S''-boats that were in service in [[1939]], only three survived to see the end of [[World War II]], " +
//                            "a loss rate that inspired the song \"Twelve Little S-Boats\", based on a [[nursery rhyme]] originally written by [[Septimus Winner]] in " +
//                            "[[1868]]. The survivors were ''Sealion'', ''Seawolf'' and ''Sturgeon''.\n" +
//                            "\n" +
//                            "; A Term : It's definition. \n" +
//                            ":Twelve little ''S''-boats \"go to it\" like [[Ernest Bevin|Bevin]], \n" +
//                            "::[[HMS Starfish (19S)|''Starfish'']] goes a bit too far &mdash; then there were eleven.\n" +
//                            ":Eleven watchful ''S''-boats doing fine and then \n" +
//                            "::[[HMS Seahorse (98S)|''Seahorse'']] fails to answer &mdash; so there are ten.\n" +
//                            ":Ten stocky ''S''-boats in a ragged line, \n" +
//                            "::[[HMS Sterlet (2S)|''Sterlet'']] stops and drops out &mdash; leaving us nine.\n" +
//                            ":Nine plucky ''S''-boats, all pursuing fate, \n" +
//                            "::[[HMS Shark (54S)|''Shark'']] is overtaken &mdash; now we are eight.\n" +
//                            ":Eight sturdy ''S''-boats, men from [[Hampshire|Hants]] and [[Devon]], ")));
//        }

        @Test
        void test_clearWikiLinks()
        {
            assertEquals("abc\ndefUnited States Joint Service Color guard \non parade at Fort Myer.tuv\nxyz"
                    , clearWikiLinks("abc\ndef[[Image:Jointcolors.jpg|thumb|350px|right|[[United States Department of Defense|United States Joint Service]] Color guard \non parade at [[Fort Myer]].]]tuv\nxyz"));
            assertEquals("In the US military and other similar organizations, the '''Color guard''' carries the National Color and other flags appropriate to its position in the chain of command."
                    , clearWikiLinks("In the US [[military]] and other similar organizations, the '''Color guard''' carries the [[Colours, standards and guidons#United States|National Color]] and other [[flag]]s appropriate to its position in the [[chain of command]]."));
            assertEquals("Tour of Duty", clearWikiLinks("[[de:Tour of Duty]][[Tour of Duty]]"));
            assertEquals("*   Army Distinguished Service Medal with two oak leaf clusters"
                    , clearWikiText("* [[Image:ArmDRib.gif|60px]]&nbsp;&nbsp;[[Army Distinguished Service Medal]] with two oak leaf clusters "));
            assertEquals("* Hey There Foo!  Army Distinguished Service Medal with two oak leaf clusters"
                    , clearWikiText("* [[Image:ArmDRib.gif|alt=Hey There Foo!|60px]]&nbsp;&nbsp;[[Army Distinguished Service Medal]] with two oak leaf clusters "));
            assertEquals("* Hey There Foo!  Army Distinguished Service Medal with two oak leaf clusters"
                    , clearWikiText("* [[Image:ArmDRib.gif|Hey There Foo!|60px]]&nbsp;&nbsp;[[Army Distinguished Service Medal]] with two oak leaf clusters "));
            assertEquals("* Hey There Foo!  Army Distinguished Service Medal with two oak leaf clusters"
                    , clearWikiText("* [[Image:ArmDRib.gif|blah blah|alt=Hey There Foo!]]&nbsp;&nbsp;[[Army Distinguished Service Medal]] with two oak leaf clusters "));
            assertEquals("* Hey There Foo!  Army Distinguished Service Medal with two oak leaf clusters"
                    , clearWikiText("* [[Image:ArmDRib.gif|top|Hey There Foo!]]&nbsp;&nbsp;[[Army Distinguished Service Medal]] with two oak leaf clusters "));
            assertEquals("* Hey There Foo!  Army Distinguished Service Medal with two oak leaf clusters"
                    , clearWikiText("* [[Image:ArmDRib.gif|top|alt=Hey There Foo!|hoop]]&nbsp;&nbsp;[[Army Distinguished Service Medal]] with two oak leaf clusters "));
        }

//        @Test
//        void test_htmlToText()
//        {
//            assertEquals("\n\n" +
//                    "  dry tinder out there and I'm not sure what's going to light the spark.' Now,\n" +
//                    "  \n\n" +
//                    "  why did we say it's contained? And I think...\n" +
//                    "  \n\n" +
//                    "  \n\n" +
//                    "  KUDLOW: You said it, Mr. Bernanke said it, a lot of people said it.\n" +
//                    "  \n\n" +
//                    "  \n\n" +
//                    "  Mr. PAULSON: Yeah. And I think...\n" +
//                    "  \n\n" +
//                    "  \n\n" +
//                    "  KUDLOW: And then I said it after you said it.\n" +
//                    "  \n\n" +
//                    "  \n\n" +
//                    "  Mr. PAULSON: I know it. I know it. And you're an optimist.\n" +
//                    "  \n\n" +
//                    "  \n\n" +
//                    "  KUDLOW: I am an optimist.\n" +
//                    "  \n\n" +
//                    "  \n"
//            , htmlToText(htmlToText(" <br>\n" +
//                            "  dry tinder out there and I'm not sure what's going to light the spark.' Now,\n" +
//                            "  <br>\n" +
//                            "  why did we say it's contained? And I think...\n" +
//                            "  <br>\n" +
//                            "  <br>\n" +
//                            "  KUDLOW: You said it, Mr. Bernanke said it, a lot of people said it.\n" +
//                            "  <br>\n" +
//                            "  <br>\n" +
//                            "  Mr. PAULSON: Yeah. And I think...\n" +
//                            "  <br>\n" +
//                            "  <br>\n" +
//                            "  KUDLOW: And then I said it after you said it.\n" +
//                            "  <br>\n" +
//                            "  <br>\n" +
//                            "  Mr. PAULSON: I know it. I know it. And you're an optimist.\n" +
//                            "  <br>\n" +
//                            "  <br>\n" +
//                            "  KUDLOW: I am an optimist.\n" +
//                            "  <br>\n" +
//                            "  <br>"
//                    , "", ""), "", ""));
//            assertEquals("I read it in the course of three or four\ndays.\n\nNow, let me ask you this. You were here in this very spot in the spring of\n2007. We interviewed.\n\nMr. PAULSON: Yeah.\n\nKUDLOW: And the first stirrings of the crisis"
//            , htmlToText("I read it in the course of three or four<br />days.<br /><br />Now, let me ask you this. You were here in this very spot in the spring of<br />2007. We interviewed.<br /><br />Mr. PAULSON: Yeah.<br /><br />KUDLOW: And the first stirrings of the crisis"
//                    , "", ""));
//            String x = htmlToText(
//                    "There's just a lot of<br /><br />" +
//                    "dry tinder out there and I'm not sure what's going to light the spark.' Now,<br /><br />" +
//                    "why did we say it's contained? And I think...<br /><br />" +
//                    "KUDLOW: You said it, Mr. Bernanke said it, a lot of people said it.<br /><br />" +
//                    "Mr. PAULSON: Yeah. And I think...<br /><br />" +
//                    "KUDLOW: And then I said it after you said it.<br /><br />" +
//                    "Mr. PAULSON: I know it. I know it. And you're an optimist.<br /><br />" +
//                    "KUDLOW: I am an optimist.<br /><br />"
//                    , "", "");
//            x = htmlToText(x, "", "");
//            docSplitter = DocSplitterFactory.create();
//            String[] sentences = getSentences(x).toArray(new String[0]);
//            String[] y = new String[] { "There's just a lot of\n\ndry tinder out there and I'm not sure what's going to " +
//                    "light the spark.' Now,\n\nwhy did we say it's contained?",
//                    "And I think...\n\nKUDLOW: You said it, Mr. Bernanke said it, a lot of people said it.",
//                    "Mr. PAULSON: Yeah.",
//                    "And I think...\n\nKUDLOW: And then I said it after you said it.",
//                    "Mr. PAULSON: I know it.",
//                    "I know it.",
//                    "And you're an optimist.",
//                    "KUDLOW: I am an optimist."};
//
//            assertEquals(y.length, sentences.length);
//            for (int i = 0; i < y.length; ++i) {
//                assertEquals(y[i], sentences[i]);
//            }
//        }
//
//    }
}
