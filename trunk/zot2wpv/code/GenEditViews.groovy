import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import groovy.xml.MarkupBuilder
import org.cyberneko.html.parsers.SAXParser
import java.util.regex.Matcher
import groovy.io.FileType

extDir  = new File(".")
dataDir = new File(extDir, 'data')
diffDir = new File(extDir, 'cache')
htmlDir = new File(extDir, 'html')

viewDir = new File(htmlDir, 'edits')
viewDir.mkdirs()

File getViewFile(edit) { new File(viewDir, edit.editid + '.html') }

File getSubviewFile(edit) { new File(viewDir, "f" + edit.editid + '.html') }

File getDiffFile(edit)
{
   new File(diffDir, "index.php_diff=${edit.newrevisionid}_oldid=${edit.oldrevisionid}.html")
}

revisionsDir = new File(dataDir, 'article-revisions')

File getRevisionFile(rev)
{
   rev = rev + '.txt'
   File found = null
   revisionsDir.eachFileRecurse(FileType.FILES) { File f ->
     if (f.name == rev) found = f
   }
   if (!found) {
      println "Can't find revision file ${rev}."
   }
   found
}

String relativePath(File base, File target)
{
//    URI baseURI = base.parentFile.toURI()
//    URI relURI = baseURI.relativize(target.toURI())
//    return relURI.toASCIIString()

   // No need for canonicalization because we constructed them relatively.
   base = base.parentFile
   String t = target.toURI().toASCIIString()

   String prefix = ""

   while (base != null ) {
      String p = base.toURI().toASCIIString()

      if (t.startsWith(p)) {
         return prefix + t.substring(p.length())
      }

      prefix += "../"
      base = base.parentFile
   }

   target.toURI().toASCIIString()
}

assert(relativePath(new File(htmlDir, 'xxx.html'), new File(viewDir, 'yyy.html')) == "edits/yyy.html")
assert(relativePath(new File(diffDir, 'xxx.html'), new File(viewDir, 'yyy.html')) == "../html/edits/yyy.html")

String mungeFieldName(name) { name == 'class' ? 'type' : name }

List readCSVtoList(String fileName) {
  reader = new CSVReader(new FileReader(new File(dataDir, fileName)))
  String[] fieldNames = reader.readNext()
  List data = reader.readAll()
  data.collect { String[] row ->
    Map fields = [:]
    fieldNames.eachWithIndex { name, x -> fields[mungeFieldName(name)] = row[x] }
    new Expando(fields)
  }
}

Map readCSVtoMap(String fileName) {
  reader = new CSVReader(new FileReader(new File(dataDir, fileName)))
  String[] fieldNames = reader.readNext()
  List data = reader.readAll()
  Map indexedData = [:]
  data.each { String[] row ->
     Map fields = [:]
     fieldNames.eachWithIndex { name, x -> fields[mungeFieldName(name)] = row[x] }
     indexedData[row[0]] = new Expando(fields)
  }
  indexedData
}

edits = readCSVtoMap('edits.csv')
gold = readCSVtoMap('gold-annotations.csv')
//annotators = readCSVtoMap('annotators.csv')
//annotations = readCSVtoList('annotations.csv')

println "${edits.size()} edits."
println "${gold.size()} gold annotations."
//println "${annotators.size()} annotators."
//println "${annotations.size()} annotations."

editText = new CSVWriter(new BufferedWriter(new FileWriter(new File(dataDir, "text.csv"))))
editText.writeNext(["editid", "class", "insert", "line", "text"] as String[])

textStat = new CSVWriter(new BufferedWriter(new FileWriter(new File(dataDir, "stat.csv"))))
textStat.writeNext(["editid", "deletedChars", "insertedChars"
                   , "changedLines", "deletedLines", "insertedLines"
                   , "oldSize", "newSize"] as String[])

def genViewFile(edit)
{
  def goldInfo = gold[edit.editid]

  File diffFile = getDiffFile(edit)

  if (!diffFile.exists()) {
     println "No diff file ${diffFile.path} for edit ${edit.editid} new=${edit.newrevisionid} old=${edit.oldrevisionid}"
     return
  }

//  def diffHtml = new XmlSlurper(new SAXParser()).parse(diffFile)
  XmlParser parser = new XmlParser(false, false)
  parser.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false)
  parser.setTrimWhitespace(false)
  def diffHtml = parser.parse(diffFile.newReader())
  
  def diffTable = diffHtml.'**'.findAll { it.'@class' == 'diff' }

  if (diffTable.size() != 1) {
     println "Didn't find (exactly one) <table class='diff'> for edit ${edit.editid} diffFile ${diffFile.path}"
     diffTable = null
  }

  def subviewFile = getSubviewFile(edit)

  def html = new MarkupBuilder(new BufferedWriter(new FileWriter(subviewFile)))

  html.html {
    head {
       meta ('http-equiv':"Content-Type", content:"text/html; charset=UTF-8")
       title("Edit " + edit.editid)
       link(rel:"stylesheet", href:"../view.css", type:"text/css", media:"all")
    }
     body {
        div {
          span ('class':'label', "Edit : ")
          span (edit.editid)
          span (" ")
          if (goldInfo.type == 'vandalism') { span ('class':'vandalism', "Vandalism") } else  { span ('class':'regular', "Regular") }
          span(" ")
          span ('class':'label', "Editor : ")
          span (edit.editor)
          span (" ")
          span('class':'label', "newid : ")
          span(edit.newrevisionid)
          span(" ")
          span('class':'label', "oldid : ")
          span(edit.oldrevisionid)
        }
        div {
           span ('class':'label', "Title : ")
           span (edit.articletitle)
        }
        div {
           span('class':'label', "Comment : ")
           if (edit.editcomment == "null") {
              edit.editcomment = ""
           }
           Matcher m = edit.editcomment =~ /^(\/\*[^*]+\*\/)?\s*(.*)$/
           assert m.matches()
           if (m.group(1)) { 
              span ('class':'editcategory', m.group(1))
           }
           def comment = m.group(2).trim()
           if (comment) { span (comment) } else { span ('class':'label', 'none') }
        }
        div {
//          a(href:diffFile.toURI(), target:'diff_frame') { span ( edit.diffurl ) }

          a(href:relativePath(subviewFile, diffFile), target:'diff_frame') { span ( edit.diffurl ) }
        }
       if (diffTable != null) {
         Integer deletedLines = 0
         Integer insertedLines = 0
         Integer changedLines = 0
         Integer deletedChars = 0
         Integer insertedChars = 0

         hr()
         def rows = diffTable.tr[0]
         rows.each { tr ->
            def delLine = tr.td.find { it.'@class' == 'diff-deletedline' }
            def addLine = tr.td.find { it.'@class' == 'diff-addedline' }

            if (delLine) {
               if (addLine) {
                  // We've got a Delete and an Add, this is a Changed line.
                  changedLines += 1
                  div {
                    delLine.div.span.findAll { it.'@class' == 'diffchange'}.each { s ->
                      String t = s.text()
                      deletedChars += t.length()
                      editText.writeNext([edit.editid, goldInfo.type, "N", "N", changedLines.toString(), t] as String[])
                      span ('class':'delete-text', t)
                    }
                  }
                 div {
                   addLine.div.span.findAll { it.'@class' == 'diffchange'}.each { s ->
                     String t = s.text()
                     insertedChars += t.length()
                     editText.writeNext([edit.editid, goldInfo.type, "Y", "N", changedLines.toString(), t] as String[])
                     span ('class':'insert-text', t)
                   }
                 }
               } else {
                 delLine.div.each { s ->
                   deletedLines += 1
                   div {
                     String t = s.text()
                     deletedChars += t.length()
                     editText.writeNext([edit.editid, goldInfo.type, "N", "Y", deletedLines.toString(), t] as String[])
                     span ('class':'delete-line', t)
                   }
                 }
               }
            } else {
              if (addLine) {
                addLine.div.each { s ->
                  insertedLines += 1
                  div {
                    String t = s.text()
                    insertedChars += t.length()
                    editText.writeNext([edit.editid, goldInfo.type, "Y", "Y", insertedLines.toString(), t] as String[])
                    span ('class':'insert-line', t)
                  }
                }
              }
            }
         }

         hr()

         div {
           span('class':'numberLabel', '-chars =')
           span('class':'number', deletedChars.toString())
           span('class':'numberLabel', ',  +chars =')
           span('class':'number', insertedChars.toString())
           span('class':'numberLabel', ',  \u0394lines =')
           span('class':'number', changedLines.toString())
           span('class':'numberLabel', ',  -lines =')
           span('class':'number', deletedLines.toString())
           span('class':'numberLabel', ',  +lines =')
           span('class':'number', insertedLines.toString())
//         }
//
//         div {
           def oldSize = getRevisionFile(edit.oldrevisionid).size()
           def newSize = getRevisionFile(edit.newrevisionid).size()
           span('class':'numberLabel', ',  oldSize =')
           span('class':'number', oldSize)
           span('class':'numberLabel', ',  newSize =')
           span('class':'number', newSize)
           span('class':'numberLabel', ',  \u0394 =')
           span('class':'number', newSize-oldSize)

           textStat.writeNext([edit.editid, deletedChars.toString(), insertedChars.toString()
                              , changedLines.toString(), deletedLines.toString(), insertedLines.toString()
                              , oldSize.toString(), newSize.toString()] as String[]) 
         }

//         div {
//           def lines = diffTable.'**'.findAll { it.'@class' == 'diff-deletedline' }
//           if (lines) {
//             div { span ('class':'label', "Deleted Lines") }
//             lines.each { line ->
//                div {
//                   span (line.text())
//                }
//             }
//           } else {
//             div { span ('class':'label', "No Deleted Lines") }
//           }
//         }
//         hr()
//         div {
//           def lines = diffTable.'**'.findAll { it.'@class' == 'diff-addedline' }
//           if (lines) {
//             div { span ('class':'label', "Added Lines") }
//             lines.each { line ->
//                div {
//                   span (line.text())
//                }
//             }
//           } else {
//             div { span ('class':'label', "No Added Lines") }
//           }
//         }
       }
     }
  }

  def viewFile = getViewFile(edit)

  html = new MarkupBuilder(new BufferedWriter(new FileWriter(viewFile)))

  html.html {
      head {
         meta ('http-equiv':"Content-Type", content:"text/html; charset=UTF-8")
         title("Edit " + edit.editid)
      }
     frameset(rows:'33%,*') {
        frame(name:'editdata', src:relativePath(viewFile, subviewFile))
        frame(name:'diff_frame', src:relativePath(viewFile, diffFile))
     }
  }
}

//edits.values().findAll{ gold[it.editid].type == 'regular' }.each {
edits.values().sort { Integer.valueOf(it.editid) }.each {
   genViewFile(it)
   editText.pw.flush()
   textStat.pw.flush()
}

editText.close()
textStat.close()
