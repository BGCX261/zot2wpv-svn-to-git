\documentclass{llncs}
\usepackage[american]{babel}
\usepackage[T1]{fontenc}
\usepackage{times}
\usepackage{graphicx}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

\title{ZOT! to Wikipedia Vandalism}
%%% Please leave the subtitle unchanged.
\subtitle{Lab Report for PAN at CLEF 2010}

\author{James White \and Rebecca Maessen}
\institute{School of Information and Computer Science \\
University of California, Irvine\\
<jpwhite, rmaessen>@uci.edu}


\maketitle

\begin{abstract}
Please give a brief overview of how your vandalism detector works, and mention the evaluation results.
\end{abstract}

\section{Introduction}
Wikipedia has become a major source of the world's consensus knowledge by virtue of its broadly inclusive and open nature.  That openness also leads to problems that are endemic to other Internet media such email and blog comments that have very permissive access methods, namely \textit{spam}, \textit{abuse}, and \textit{vandalism}.  If active measures are not taken to limit and repair the damage of such harmful activity then the value of these common goods will fall, and given that their value is derived from networks effects (``Metcalf's Law") such degradation could even lead to them becoming effectively useless.  Although there are similarities in the problems across these public media commons, there are also differences that call for specifically tailored solutions.  In the realm of email and blog comments the term ``spam" is most frequently used, while with Wikipedia the term ``vandalism" is becoming more common.  Wikipedia vandalism includes the kind of commercial speech that is typical of spam (although perhaps less of the scam aspect) but also often means purposefully destructive changes that are more peculiar to Wikipedia.   

The scale of Wikipedia and the desirability of reducing the amount of manual labor needed to maintain it has led to the development of a variety of automated tools to assist in the detection and repair of vandalism, with a few such tools (``bots") actually making repairs (``reversions") entirely automatically.  The most successful of the automated Wikipedia vandalism detection tools that have been deployed so far (ClueBot, abusefilter, AVB, etc) largely depend on manually constructed and tuned rules. Recent research has focused machine learning methods on the Wikipedia vandalism problem as exemplified by the International Workshop on Uncovering Plagiarism, Authorship, and Social Software Misuse (PAN) which will be held in conjunction with the Conference on Multilingual and Multimodal Information Access Evaluation (CLEF) this year (http://pan.webis.de/).


\subsection{Related Work}

\section{Data Sources and Training Edits}
The training set provided for the PAN @ CLEF vandalism detection task is a corpus of 15,000 training cases. 944 of these cases are classified as vandalism, which comprises 6\% of the total trainingset. This is comparable to the real world ratio of 'good' edits and 'bad' edits on Wikipedia. The small number of vandalism edits in this unbalanced dataset provides little information to generalize over the complete data set. In order to find more patterns specific for vandalism we decided to include additional vandalism edits into our data set. The West et al data set (http://www.cis.upenn.edu/~westand/) contains 5.7million automatically determined offending edits along with 5,286 manually confirmed vandalism edits. We used these 5,286 manually classified vandalism edits to balance out our data set. After expanding, our training dataset holds 14,076 regular edits and 6,207 vandalism edits. Compared to 6\% ill-intended edits in the provided PAN @ CLEF data set, this expanded data set contains 31\% vandalism edits (close to the ratio Potthast et al used in their work).

\section{Data Preparation}
The data set as provided has few nominal or numerical features directly suitable for the regular vs. vandalism classification task.  We have focused on deriving features using text analysis methods.

\subsection{Text Differencing}

The primary text for each edit consists of the entire article text (in WikiMedia ``wikimarkup" format) from before the edit and after the edit.  While we do use some features based on those whole text versions of the article (such as length and markup counts), much of our classification approach is based on trying to characterize just the new text added by the editor in this edit.  The reason for trying to just deal with the edit's new text is that most (but not all) vandalism consists of inserting vulgar or nonsense words and phrases into articles.

We searched for text differencing algorithms (with implementations) and tried several different methods in order to find one well suited to our task.  A common problem with line-oriented text differencing algorithms is that they do not handle lines that get split into two (or more) lines very well.  Usually in that case at least one of the ``new" lines will be flagged as the insertion of text.  But we want to recognize that all of the text in both lines is from the original, otherwise the text actually inserted by the vandal will be ``diluted" by the (presumably) ``regular" text from the old version of the article.  
The implementation we are currently using is ``google-diff-match-patch" by Neil Fraser (http://code.google.com/p/google-diff-match-patch/).  It does a character-oriented diff with some line-oriented optimizations.  There are also a couple of ``clean up" methods and we are using the ``semantic clean up" which employs useful heuristics.  The results are not quite ideal for this application though, because (unlike sentences) we would like to get complete words when any part of a word is changed so that our word-oriented features will match properly. We made some changes to the differencing algorithm to incorporate this nuance. 

\subsection{Word Vectors}

One set of features is derived from the text inserted by the edit (as determined by the text differencing routine described above) by using the Word Vector Tool for RapidMiner (http://wvtool.sf.net/). In order to put the text in the form required by the tool, the inserted text for each edit is put into a separate file (using the editid as the file name) and with the ``vandalism" edits in one directory and the ``regular" edits in another.  The current settings are TFIDF (term frequency/inverse document frequency), prune below 3 (terms must appear in at least three documents), string tokenization, Porter stemming, and N-grams up to length 2.

The Word Vecor tool resulted in a word vector of 14,956 attributes. We were able to run logistic regression on this data in a reasonable time. However, when we added the metadata features to the word vector our classifications took too long to run, eventhough this was an addition of only 20 attributes. To make our data more manageable, we used the RemoveUselessAttributes tool in RapidMiner to select the words from the word vector with the most significance to our classification. We set the tool to remove all the attributes with a standard deviation of 0.035 or less. We figured out that this would give us the largest word vector that could be used to classify in a reasonable time. We narrowed our word vector down to 34 attributes. Testing shows that this reduction does not come with a significant information loss for our classification task.

\subsection{Historical Information}

Some useful metadata is time sensitive, such as the length of time an editor has been using Wikipedia, the number of edits they've made, and whether they've vandalized Wikipedia before.  In order to ensure that we don't use ``future" information in our classification, calculation of those features is performed in order based on the 'edittime' feature.  Due to the rather small size of the training set, it turns we do not actually gain much information using these computed features.

\subsection{Edit Features}

Unchanged \verb'edits.csv' metadata features (which are all nominal or text and thus not used directly by the classifiers): \verb'editid', \verb'label', \verb'oldrevisionid', \verb'newrevisionid', \verb'edittime', and \verb'editor'.
The raw metadata features and article text files were processed to generate the features decribed in Table 1.


\begin{table}[ht]
\label{edit-fatures-table}
\begin{center}
\begin{tabular}{l p{8cm} p{8cm}}
\multicolumn{1}{c}{\bf Feature}  &\multicolumn{1}{c}{\bf Description} 
\\ \hline\hline \\

Anonymous editor &1 if the \verb'editor' feature matches the pattern for an IP address, 0 otherwise.\\
Editor edit count &The number of edits the editor has made prior to this one.\\
Editor revert count &The number of edits the editor has made that have been reverted prior to this one.\\
Revert comment &1 if  \verb'editcomment' feature contains a pattern matching ``revert" (ignoring case), 0 otherwise.\\
Vandal comment &1 if  \verb'editcomment' feature contains a pattern matching ``vandal" (ignoring case), 0 otherwise.\\
Is a reversion &1 if this edit is a reversion to a prior version of the article, 0 otherwise.  An edit is considered to be a reversion if the 'Revert comment' or 'Vandal comment' features are true, or if the MD5 hash of the new version of the article text matches that seen for a prior version of the article.\\
Edit section &The text of the raw 'editcomment' matching the automatically generated section comment (``/* SECTION */"), if any.\\
Edit comment &The text of the raw 'editcomment' with the section comment, if any removed.\\
Comment length &The length of the 'Edit comment' (after section removal).  This is a valuable feature because many vandals leave no comment about their edit.\\
Old article size &Number of characters in the article text prior to the edit.\\
New article size &Number of characters in the article text after the edit.\\
Size diff &Difference in the number of characters in the article text (new - old).\\
Size diff ratio &Difference in the number of characters as a ratio (if old != 0) (new - old)/old.\\
Old markup count &A measure of the amount of wikimarkup in the article text prior to the edit.  \\
New markup count &A measure of the amount of wikimarkup in the article text prior to the edit.\\
Markup count diff &Difference in the amount of wikimarkup in the article text (new – old)/old.\\
Markup count diff ratio &Difference in the amount of wikimarkup as a ratio (if old != 0) (new – old)/old.\\
Is article blanked &1 if the new article text is blank.\\
Is article replacement &1 if the new text is effectively a complete replacement for the old text, 0 otherwise.  Note that the determination of what constitutes a ``complete replacement" is based on the text differencing logic which tries to distinguish between coincidental
 similarity and actual changes.\\
Is section replacement &1 if the edit completely replaced an existing section with new text (including removing the previous section header), 0 otherwise.
This is characteristic of many vandalizing edits because of the tendency for vandals to click the ``edit" button for a section and then replace the entire section.\\

Is section content replacement & section header is preserved in the new text. Another common pattern for vandals.\\

All CAP word count &The number of ``words" (currently runs of at least 4 alphabetic characters) that are all capitals.\\Bad word count &The number of matches to a regular expression for various commonly used vulgar words.\\
\hline
\end{tabular}
\end{center}
\caption{Processed Edit Features}
\end{table}



\section{Classification}
We used a decision tree stucture to distinguish vandalism edits from regular edits. The datamining tool Rapidminer functioned as a framework for our algorithm. We used its Wj48-graft decision tree learner to generate a C4. 5 decision tree. Parameters that affect the results of the classification are the level of pruning and the minimum number of instances per leaf in the tree. To optimize our results we used the WJ48-graft learner inside of a ensemble method, namely bagging. The number of iterations and the fraction of examples used for training are parameters for bagging.

Decision trees are interesting classifiers because they have the potential for being implemented in such a way that expensive-to-calculate features are only evaluated when they would be useful, which would be a consideration for a production implementation of the classifier for Wikipedia. 

\section{Evaluation}
\subsection{Method}
\subsection{Results}
\section{Conclusion}


\begin{thebibliography}{4}


\bibitem{proceeding1}Potthast, M., Stein, B., and Gerling, R.: Automatic Vandalism Detection in Wikipedia.  In: Proceedings of the 30th European Conference on IR Research (ECIR), pp. 663--668. Berlin Heidelberg New York, 2008.

\bibitem{proceeding2} Smets, K., Goethals, B., and Verdonk,B.: Automatic vandalism detection in Wikipedia: Towards a machine learning approach. In WikiAI '08: 
Proceedings of the AAAI Work-shop on Wikipedia and Artificial Intelligence, 2008.

\bibitem{proceeding2} West, A.,Lee, I., and Kannan, S.: Detecting Wikipedia Vandalism viaSpatio-Temporal Analysis of Revision Metadata. In EUROSEC '10: Proceedings of the Third European Workshop on System Security, 2010.



\end{thebibliography}


\end{document}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

