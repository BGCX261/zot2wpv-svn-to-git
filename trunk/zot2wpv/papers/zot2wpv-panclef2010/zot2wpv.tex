\documentclass{llncs}
\usepackage[american]{babel}
\usepackage[T1]{fontenc}
\usepackage{times}
\usepackage{graphicx}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

\title{ZOT! to Wikipedia Vandalism}
%%% Please leave the subtitle unchanged.
\subtitle{Lab Report for PAN at CLEF 2010}

\author{James White \and Rebecca Maessen}
\institute{School of Information and Computer Science \\
University of California, Irvine\\
<jpwhite, rmaessen>@uci.edu}

\maketitle

\begin{abstract}
Our vandalism detector uses features primarily derived from a word-preserving differencing of the text for each Wikipedia article from before and after the edit, along with a few metadata features and statistics on the beore- and after- article text.  Features computed from the text difference are then a combination of statistics such as length, markup count, and blanking along with a selected number of TFIDF values for words and bigrams.  Our training set was expanded from that supplied for the shared task to include the 5K vandalism edit corpus from West et al.  Classification was performed using bagging of the Weka J48graft (C4.5) decision tree \cite{Webb1999} which resulted in an evaluation score of 0.84340 AUC.  It is unclear whether the expanded vandalism data improved or degraded performance because that changed the ratio of regular to vandlism edits in the training set and we did not make any adjustment for that when training the classifier.
\end{abstract}

\section{Introduction}
Wikipedia has become a major source of the world's consensus knowledge by virtue of its broadly inclusive and open nature.  That openness also leads to problems that are endemic to other Internet media such email and blog comments that have very permissive access methods, namely \textit{spam}, \textit{abuse}, and \textit{vandalism}.  If active measures are not taken to limit and repair the damage of such harmful activity then the value of these common goods will fall, and given that their value is derived from networks effects (``Metcalf's Law") such degradation could even lead to them becoming effectively useless.  Although there are similarities in the problems across these public media commons, there are also differences that call for specifically tailored solutions.  In the realm of email and blog comments the term ``spam" is most frequently used, while with Wikipedia the term ``vandalism" is becoming more common.  Wikipedia vandalism includes the kind of commercial speech that is typical of spam (although perhaps less of the scam aspect) but also often means purposefully destructive changes that are more peculiar to Wikipedia.   

The scale of Wikipedia and the desirability of reducing the amount of manual labor needed to maintain it has led to the development of a variety of automated tools to assist in the detection and repair of vandalism, with a few such tools (``bots") actually making repairs (``reversions") entirely automatically.  The most successful of the automated Wikipedia vandalism detection tools that have been deployed so far (ClueBot, abusefilter, AVB, etc) largely depend on manually constructed and tuned rules. Recent research has focused machine learning methods on the Wikipedia vandalism problem as exemplified by the International Workshop on Uncovering Plagiarism, Authorship, and Social Software Misuse (PAN) which will be held in conjunction with the Conference on Multilingual and Multimodal Information Access Evaluation (CLEF) this year (http://pan.webis.de/).


\section{Related Work}

\subsection{Detecting Wikipedia Vandalism via Spatio-Temporal Analysis of Revision Metadata, by Andrew G. West, Sampath Kannan, and Insup Lee}

This classifier \cite{West2010} for detecting Wikipedia vandalism defines an \textit{offending edit} (OE) as one that resulted in a rollback by a privileged Wikipedia editor (about 4,700 of them at the time of the work).  The presumption is that such edits constitute vandalism because they are effectively unproductive.  The remainder are considered to be \textit{random}, by which they mean that whether the edit is vandalism or not is unknown.  They confine their analysis to what Wikipedia calls name-space zero (NS0) which contains the ``content" articles of the encyclopedia, in contrast to the housekeeping, discussion, user, and other ``non-content" pages.

The distinguishing characteristic of the method used for this classifier is that it relies entirely on revision metadata and does not extract features from the text in the article or the edit itself.  By not expending computational resources on text analysis they ensure relatively high performance in terms of speed and they leave open the possibility of improvement by combining the metadata analysis with text analysis.  High speed is important for a practical Wikipedia vandalism detector because of the high volume of edits and the need to find the OEs quickly.

The features they use are of two types: (1) \textit{simple features} that encode information pertaining to a particular edit, and (2) \textit{aggregate features} that are derived from analysis of the large OE history that they extracted from a year's worth of Wikipedia metadata.  The half dozen simple features are several time stamps, the length of the revision comment, and whether the user is registered.  They also use  country of origin as determined by the IP address.  The aggregate features are ``reputation" values for article, user, category, and country.

The classification algorithm used is SVM, which they point out required ``...carefully tuning the cost parameters to compensate for the `non-homogeneous' nature of the `random' label''.  By ``non-homogenous" they are referring to the fact that edits which were not rolled back (and thus labeled as OE) might or might not be offending edits as well. 

They claim classification performance as 85\% ``adjusted'' accuracy at 50\% recall.  The unlabeled nature of the``random'' edits causes some problems in understanding these figures.  The adjustments are made by manually inspecting a sample of the ``random'' edits classified as offending to determine whether they are actually offending or false positives.  Using that adjustment they assert an improvement in accuracy from 80\%.  For precision, the raw number is 27\% at 50\% recall (an F-measure of .35) and with the ``adjustment'' the precision becomes 49\%, which with 50\% recall (which would be unchanged if precision were 50\%) means an F-measure of .495.  

\subsection{Automatic Vandalism Detection in Wikipedia, by Martin Potthast, Benno Stein, and Robert Gerling}
Potthast et al \cite{Potthast2008} distinguish three types of Wikipedia vandalism: insertion, replacement, and deletion. In their research they analyzed 301 cases of vandalism to identify a set of attributes, which they believe show the biggest difference between Wikipedia edits intended to improve the content of a given page, and edits that show misuse. The traits recognized include features from the inserted text, such as term frequency and upper case ratio, as well as features from the metadata information, such as the anonymity and total number of edits of the author. With these attributes, the researchers performed logistic regression on a dataset of Wikipedia revisions as a machine learning tool to detect vandalism. 

In order to train and test their logistic regression classifier, Potthast et al compiled a vandalism corpus (WEBIS-VC07-11), which contains 940 human-classified edits. Out of these edits, 301 are categorized as vandalism.  By using their classifier with a ten-fold cross-validation on this dataset, an 83\% precision was achieved at 77\% recall (an F-measure of .80). Compared to the currently used rule-based bots, AVB and ClueBot (100\% precision at 33\% recall, an F-measure of .66)), their model shows considerably higher performance on precision, recall, and throughput.  This is an indicator that a language processing tool based on human-recognized features could improve the efficiency of Wikipedia's current defense against vandals. 

The results also show how important the individual features are for identifying the three types of Wikipedia vandalism. There exists a wide variation between the three types; what is a big indicator for one type might not play a role in pinpointing another.  A surprising result is that their feature effectiveness breakdown shows zero for recall (and thus precision too) for both anonymity and comment length, whereas West et al show those features as being very useful.

Potthast et al point out that research in analyzing vandalism characteristics could improve the quality of their model. The balance of the WEBIS-VC07-11 does not relate to the real world, where abusive edits count for approximately 5\% of all edits.  The very small size of the data set used in this work (940 edits) makes direct comparison with other results on much larger data sets uncertain.

\subsection{Vandalism Detection in Wikipedia: a Bag-of-Words Classifier Approach by Amit Belani}
Amit Belani \cite{Belani2010} reports on using logistic regression to train a classifier to predict whether an edit will be reverted (like the detector of West et al) using the counts for addition and subtraction of all words that appear in the training corpus as features, along with a handful of typical metadata features.  Evaluated on 2,000,000 edits over 7 years of Wikipedia, the classifier utilized over 6,300,000 features as words were only lowercased and not stemmed.  This bag-of-words counting method was deemed superior for their method because it maintained a high level of sparsity, which would have been reduced if using typical term frequency features (as in our work).  They show an ROC AUC of 84.72\%, but the results raise some questions because they also report that the corpus consisted of 43\% positive (reverted) and 57\% negative cases.

\section{Data Sources and Training Edits}
The training set provided for the PAN @ CLEF vandalism detection task is a corpus of 15,000 training cases. 944 of these cases are classified as vandalism, which comprises 6\% of the total training set. This is comparable to the real world ratio of 'good' edits and 'bad' edits on Wikipedia. The small number of vandalism edits in this unbalanced dataset provides little information to generalize over the complete data set. In order to find more patterns specific for vandalism we decided to include additional vandalism edits into our data set. The West et al data set (\verb'http://www.cis.upenn.edu/~westand/') contains 5.7million automatically determined offending edits along with 5,286 manually confirmed vandalism edits. We used these 5,286 manually classified vandalism edits to expand our training set. After expansion, our training dataset holds 14,076 regular edits and 6,207 vandalism edits. Compared to 6\% ill-intended edits in the provided PAN @ CLEF data set, this expanded data set contains 31\% vandalism edits (close to the ratio Potthast et al used in their work).

\section{Data Preparation}
The data set as provided has few nominal or numerical features directly suitable for the regular vs. vandalism classification task.  We have focused on deriving features using text analysis methods.

\subsection{Text Differencing}

The primary text for each edit consists of the entire article text (in WikiMedia ``wikimarkup" format) from before the edit and after the edit.  While we do use some features based on those whole text versions of the article (such as length and markup counts), much of our classification approach is based on trying to characterize just the new text added by the editor in this edit.  The reason for trying to just deal with the edit's new text is that most (but not all) vandalism consists of inserting vulgar or nonsense words and phrases into articles.

We searched for text differencing algorithms (with implementations) and tried several different methods in order to find one well suited to our task.  A common problem with line-oriented text differencing algorithms is that they do not handle lines that get split into two (or more) lines very well.  Usually in that case at least one of the ``new" lines will be flagged as the insertion of text.  But we want to recognize that all of the text in both lines is from the original, otherwise the text actually inserted by the vandal will be dilute by the (presumably) ``regular" text from the old version of the article.  
The implementation we used is ``google-diff-match-patch" by Neil Fraser (\verb'http://code.google.com/p/google-diff-match-patch/').  It does a character-oriented diff with some line-oriented optimizations.  There are also a couple of ``clean up" methods and we are using the ``semantic clean up" which employs useful heuristics.  The results are not quite ideal for this application though, because (unlike sentences) we would like to get complete words when any part of a word is changed so that our word-oriented features will match properly. We implemented a character-for-word substitution codec in order to achieve that behavior.
\subsection{Word Vectors}

One set of features is derived from the text inserted by the edit (as determined by the text differencing routine described above) by using the Word Vector Tool for RapidMiner (\verb'http://wvtool.sf.net/'). In order to put the text in the form required by the tool, the inserted text for each edit is put into a separate file (using the editid as the file name) and with the ``vandalism" edits in one directory and the ``regular" edits in another.  The current settings are TFIDF (term frequency/inverse document frequency), prune below 3 (terms must appear in at least three documents), string tokenization, Snowball stemming, and N-grams up to length 2.

The Word Vecor tool resulted in a word vector of 29,106 attributes. We were able to run logistic regression on this data in a reasonable time. However, when we added the metadata features to the word vector our classifications took too long to run, even though this was an addition of only 20 attributes. To make our data more manageable, we used \verb'CorpusBasedWeighting' and \verb'AttributeWeightSelection' tools in RapidMiner to select the words from the word vector with the most significance to our classification by choosing 300 features from each of the two classes.

\subsection{Historical Information}

Some useful metadata is time sensitive, such as the length of time an editor has been using Wikipedia, the number of edits they've made, and whether they've vandalized Wikipedia before.  In order to ensure that we don't use ``future" information in our classification, calculation of those features is performed in order based on the 'edittime' feature.  Due to the rather small size of the training set, it turns we do not actually gain much information using these computed features.

\subsection{Edit Features}

The raw metadata features and article text files were processed to generate the following features:

\subsubsection{Anonymous editor}
A registered Wikipedia user is less likely to vandalize. If the \verb'editor' displays an IP address the edit is made by an unregistered user. The value of this feature is 1 if the \verb'editor' feature matches the pattern for an IP address, 0 otherwise.

\subsubsection{Editor edit count}
The number of edits the editor has made prior to this one.

\subsubsection{Editor revert count}
The number of edits the editor has made that have been reverted prior to this one.

\subsubsection{Revert comment}
1 if  \verb'editcomment' feature contains a pattern matching ``revert" (ignoring case), 0 otherwise.

\subsubsection{Vandal comment}
1 if  \verb'editcomment' feature contains a pattern matching ``vandal" (ignoring case), 0 otherwise.

\subsubsection{Is a reversion}
1 if this edit is a reversion to a prior version of the article, 0 otherwise.  An edit is considered to be a reversion if the 'Revert comment' or 'Vandal comment' features are true, or if the MD5 hash of the new version of the article text matches that seen for a prior version of the article.

\subsubsection{Comment length}
The length of the 'Edit comment' after removal of the automatically generated section label (``/* SECTION */"), if any.. This is a valuable feature because many vandals leave no comment about their edit.

\subsubsection{Old article size}
Number of characters in the article text prior to the edit.

\subsubsection{New article size}
Number of characters in the article text after the edit.

\subsubsection{Size difference}
Difference in the number of characters in the article text (new -- old).  Vandalism tends to be short, but even very lengthy vandalism is rarely more than 1000 characters.

\subsubsection{Size difference ratio}
Difference in the number of characters as a ratio (if old != 0) (new -- old)/old.

\subsubsection{Old markup count}
A measure of the amount of wikimarkup in the article text prior to the edit.  

\subsubsection{New markup count}
A measure of the amount of wikimarkup in the article text after the edit.

\subsubsection{Markup count difference}
Difference in the amount of wikimarkup in the article text (new -- old).  Vandals usually do not use wikimarkup in their additions.

\subsubsection{Markup count difference ratio}
Difference in the amount of wikimarkup as a ratio (if old != 0) (new -- old)/old.

\subsubsection{Is article blanked}
1 if the new article text is blank.  Blanking an entire article is very often vandalism.

\subsubsection{Is article replacement}
1 if the new text is effectively a complete replacement for the old text, 0 otherwise.  Note that the determination of what constitutes a ``complete replacement" is based on the text differencing logic which tries to distinguish between coincidental
 similarity and actual changes.

\subsubsection{Is section replacement}
1 if the edit completely replaced an existing section with new text (including removing the previous section header), 0 otherwise.
This is characteristic of many vandalizing edits because of the tendency for vandals to click the ``edit" button for a section and then replace the entire section.

\subsubsection{Is section content replacement}
Section header is preserved in the new text. Another common pattern for vandals.

\subsubsection{All CAP word count}
The number of ``words" (currently runs of at least 4 alphabetic characters) that are all capitals.  Regular edits have few words longer than 3 letters that are all capitals.

\subsubsection{Bad word count}
The number of matches to a regular expression for various commonly used vulgar words.


\section{Classification}
We used a decision tree stucture to distinguish vandalism edits from regular edits. The datamining tool RapidMiner v4.6 functioned as a framework for our algorithm. We used the Weka J48-graft classifier to generate a C4. 5 decision tree  \cite{Webb1999}. Parameters that affect the results of the classification are the level of pruning and the minimum number of instances per leaf in the tree. The defaults were used for all settings (C=0.25, M=2, all option flags set to false). To optimize our results we used the WJ48-graft learner inside of a ensemble method, namely bagging. The parameters are number of iterations (11) and the fraction of examples (0.9) selected for each iteration.

Decision trees are interesting classifiers because they have the potential for being implemented in such a way that expensive-to-calculate features are only evaluated when they would be useful, which would be a consideration for a production implementation of the classifier for Wikipedia. 

\section{Evaluation}

\subsection{Method}
We determined the strength of our classifier on its f-measure and the receiver operating characteristics (ROC) curve. The f-measure, which is based on precision and recall, was useful to us in the process of comparing different types of learners, because all learners support this evaluation measure. The area under the ROC curve (AUC) is used by PAN@CLEF to assess performances of participants of the Wikipedia detection task.

We evaluated our classifier by running a ten-fold cross validation on our training set, using the W-J48graft learner within a bagging ensemble method.

\subsection{Results}
The 10-fold cross validation on our training data using the W-J48graft learner and bagging resulted in an f-measure of 82.03 and an AUC of 0.960. Using the trained learner on the test data gave an AUC of 0.84340.

\section{Source Code}
The source code for our implementation is a combination of Groovy and RapidMiner scripts and is available via Subversion under the GPL v2 license.  Go to \\
 \verb'http://code.google.com/p/ifcx/source/checkout' for access details.

\section{Conclusion}
The research that we performed shows that decision trees are a good approach to find ill-intended Wikipedia edits. Additionally, decision trees train a lot faster than other classifiers that we looked at, such as logistic regression.  Efficient performance is an important consideration for a production implementation of the classifier for Wikipedia.

Our findings confirm that top down feature analysis on text difference as well as statistical information from the word vector is relevant to classifying vandalism edits. We were able to demonstrate this by classifying using only the metadata features or the word vector features first, and subsequently run the same classifiers on the features combined. The fact that neither attribute set is as predictive as the attribute sets together indicates that both type of features are significant for predicting vandalism. 

\section{Acknowledgements}
Most of this work was performed in the course of CS175 at UC Irvine taught by Arthur Asuncion during Spring 2010.  We also thank Professor Bill Tomlinson for his support. 

\bibliographystyle{splncs03}
\begin{raggedright}
\bibliography{zot2wpv}
\end{raggedright}


\end{document}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

